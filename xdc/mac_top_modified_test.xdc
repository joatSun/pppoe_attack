##############################################
##     clock and reset
##############################################
set_property PACKAGE_PIN Y9 [get_ports Clk_in]
set_property IOSTANDARD LVCMOS33 [get_ports Clk_in]

set_property PACKAGE_PIN P16 [get_ports Reset]
set_property IOSTANDARD LVCMOS25 [get_ports Reset]

set_property CLOCK_DEDICATED_ROUTE FALSE [get_nets Reset_IBUF]

set_property PACKAGE_PIN B17 [get_ports phyrst_n]
set_property IOSTANDARD LVCMOS25 [get_ports phyrst_n]


# set_property PACKAGE_PIN T22 [get_ports led_out_flag]
# set_property IOSTANDARD LVCMOS33 [get_ports {led_out_flag} ]
##############################################
##############################################
##
set_property PACKAGE_PIN H17 [get_ports pppoe_padt]
set_property IOSTANDARD LVCMOS25 [get_ports pppoe_padt]
##
set_property PACKAGE_PIN H18 [get_ports pppoe_lcp]
set_property IOSTANDARD LVCMOS25 [get_ports pppoe_lcp]
##
set_property PACKAGE_PIN M15 [get_ports approach_switch]
set_property IOSTANDARD LVCMOS25 [get_ports approach_switch]
##
set_property PACKAGE_PIN H19 [get_ports pppoe_padi]
set_property IOSTANDARD LVCMOS25 [get_ports pppoe_padi]
##
set_property PACKAGE_PIN F21 [get_ports pppoe_ddos]
set_property IOSTANDARD LVCMOS25 [get_ports pppoe_ddos]

##############################################
##     PORT 0 physical constraint
##############################################
set_property PACKAGE_PIN J17 [get_ports rgmii_tx_clk_00]
set_property IOSTANDARD LVCMOS25 [get_ports rgmii_tx_clk_00]

set_property PACKAGE_PIN M19 [get_ports rgmii_rx_clk_00]
set_property IOSTANDARD LVCMOS25 [get_ports rgmii_rx_clk_00]

set_property PACKAGE_PIN J18 [get_ports {rgmii_rxd_00[0]}]
set_property IOSTANDARD LVCMOS25 [get_ports {rgmii_rxd_00[0]}]

set_property PACKAGE_PIN N18 [get_ports {rgmii_rxd_00[1]}]
set_property IOSTANDARD LVCMOS25 [get_ports {rgmii_rxd_00[1]}]

set_property PACKAGE_PIN P21 [get_ports {rgmii_rxd_00[2]}]
set_property IOSTANDARD LVCMOS25 [get_ports {rgmii_rxd_00[2]}]

set_property PACKAGE_PIN P20 [get_ports {rgmii_rxd_00[3]}]
set_property IOSTANDARD LVCMOS25 [get_ports {rgmii_rxd_00[3]}]

set_property PACKAGE_PIN K18 [get_ports rgmii_rx_ctl_00]
set_property IOSTANDARD LVCMOS25 [get_ports rgmii_rx_ctl_00]

set_property PACKAGE_PIN J22 [get_ports {rgmii_txd_00[0]}]
set_property IOSTANDARD LVCMOS25 [get_ports {rgmii_txd_00[0]}]

set_property PACKAGE_PIN T17 [get_ports {rgmii_txd_00[1]}]
set_property IOSTANDARD LVCMOS25 [get_ports {rgmii_txd_00[1]}]

set_property PACKAGE_PIN N17 [get_ports {rgmii_txd_00[2]}]
set_property IOSTANDARD LVCMOS25 [get_ports {rgmii_txd_00[2]}]

set_property PACKAGE_PIN L21 [get_ports {rgmii_txd_00[3]}]
set_property IOSTANDARD LVCMOS25 [get_ports {rgmii_txd_00[3]}]

set_property PACKAGE_PIN J16 [get_ports rgmii_tx_ctl_00]
set_property IOSTANDARD LVCMOS25 [get_ports rgmii_tx_ctl_00]

##############################################
##     PORT 1 physical constraint
##############################################
set_property PACKAGE_PIN R21 [get_ports rgmii_tx_clk_01]
set_property IOSTANDARD LVCMOS25 [get_ports rgmii_tx_clk_01]

set_property PACKAGE_PIN N19 [get_ports rgmii_rx_clk_01]
set_property IOSTANDARD LVCMOS25 [get_ports rgmii_rx_clk_01]

set_property PACKAGE_PIN T16 [get_ports {rgmii_rxd_01[0]}]
set_property IOSTANDARD LVCMOS25 [get_ports {rgmii_rxd_01[0]}]

set_property PACKAGE_PIN M22 [get_ports {rgmii_rxd_01[1]}]
set_property IOSTANDARD LVCMOS25 [get_ports {rgmii_rxd_01[1]}]

set_property PACKAGE_PIN P17 [get_ports {rgmii_rxd_01[2]}]
set_property IOSTANDARD LVCMOS25 [get_ports {rgmii_rxd_01[2]}]

set_property PACKAGE_PIN L17 [get_ports {rgmii_rxd_01[3]}]
set_property IOSTANDARD LVCMOS25 [get_ports {rgmii_rxd_01[3]}]

set_property PACKAGE_PIN R20 [get_ports rgmii_rx_ctl_01]
set_property IOSTANDARD LVCMOS25 [get_ports rgmii_rx_ctl_01]

set_property PACKAGE_PIN N22 [get_ports {rgmii_txd_01[0]}]
set_property IOSTANDARD LVCMOS25 [get_ports {rgmii_txd_01[0]}]

set_property PACKAGE_PIN P18 [get_ports {rgmii_txd_01[1]}]
set_property IOSTANDARD LVCMOS25 [get_ports {rgmii_txd_01[1]}]

set_property PACKAGE_PIN M20 [get_ports {rgmii_txd_01[2]}]
set_property IOSTANDARD LVCMOS25 [get_ports {rgmii_txd_01[2]}]

set_property PACKAGE_PIN T19 [get_ports {rgmii_txd_01[3]}]
set_property IOSTANDARD LVCMOS25 [get_ports {rgmii_txd_01[3]}]

set_property PACKAGE_PIN R19 [get_ports rgmii_tx_ctl_01]
set_property IOSTANDARD LVCMOS25 [get_ports rgmii_tx_ctl_01]
################################################
####     PORT 2 physical constraint
################################################
##set_property PACKAGE_PIN C22 [get_ports rgmii_tx_clk_10]
##set_property IOSTANDARD LVCMOS25 [get_ports rgmii_tx_clk_10]
##
##set_property PACKAGE_PIN B19 [get_ports rgmii_rx_clk_10]
##set_property IOSTANDARD LVCMOS25 [get_ports rgmii_rx_clk_10]
##
##set_property PACKAGE_PIN E15 [get_ports {rgmii_rxd_10[0]}]
##set_property IOSTANDARD LVCMOS25 [get_ports {rgmii_rxd_10[0]}]
##
##set_property PACKAGE_PIN C20 [get_ports {rgmii_rxd_10[1]}]
##set_property IOSTANDARD LVCMOS25 [get_ports {rgmii_rxd_10[1]}]
##
##set_property PACKAGE_PIN G15 [get_ports {rgmii_rxd_10[2]}]
##set_property IOSTANDARD LVCMOS25 [get_ports {rgmii_rxd_10[2]}]
##
##set_property PACKAGE_PIN E21 [get_ports {rgmii_rxd_10[3]}]
##set_property IOSTANDARD LVCMOS25 [get_ports {rgmii_rxd_10[3]}]
##
##set_property PACKAGE_PIN F18 [get_ports rgmii_rx_ctl_10]
##set_property IOSTANDARD LVCMOS25 [get_ports rgmii_rx_ctl_10]
##
##set_property PACKAGE_PIN G21 [get_ports {rgmii_txd_10[0]}]
##set_property IOSTANDARD LVCMOS25 [get_ports {rgmii_txd_10[0]}]
##
##set_property PACKAGE_PIN A17 [get_ports {rgmii_txd_10[1]}]
##set_property IOSTANDARD LVCMOS25 [get_ports {rgmii_txd_10[1]}]
##
##set_property PACKAGE_PIN A18 [get_ports {rgmii_txd_10[2]}]
##set_property IOSTANDARD LVCMOS25 [get_ports {rgmii_txd_10[2]}]
##
##set_property PACKAGE_PIN D21 [get_ports {rgmii_txd_10[3]}]
##set_property IOSTANDARD LVCMOS25 [get_ports {rgmii_txd_10[3]}]
##
##set_property PACKAGE_PIN E18 [get_ports rgmii_tx_ctl_10]
##set_property IOSTANDARD LVCMOS25 [get_ports rgmii_tx_ctl_10]
##############################################
##   period constraints with global clock
##############################################
create_clock -period 10.000 -name clkin -waveform {0.000 5.000} -add [get_ports Clk_in]

##############################################
##     period constraints with rx_clk
##############################################
create_clock -period 8.000 -name clk_rx_00 -waveform {0.000 4.000} [get_ports rgmii_rx_clk_00]
create_clock -period 8.000 -name clk_rx_01 -waveform {0.000 4.000} [get_ports rgmii_rx_clk_01]
##create_clock -period 8.000 -name clk_rx_10 -waveform {0.000 4.000} [get_ports rgmii_rx_clk_10]

##############################################
##     SLEW
##############################################
set_property SLEW FAST [get_ports {rgmii_txd_00[0]}]
set_property SLEW FAST [get_ports {rgmii_txd_00[1]}]
set_property SLEW FAST [get_ports {rgmii_txd_00[2]}]
set_property SLEW FAST [get_ports {rgmii_txd_00[3]}]

set_property SLEW FAST [get_ports {rgmii_txd_01[0]}]
set_property SLEW FAST [get_ports {rgmii_txd_01[1]}]
set_property SLEW FAST [get_ports {rgmii_txd_01[2]}]
set_property SLEW FAST [get_ports {rgmii_txd_01[3]}]

##set_property SLEW FAST [get_ports {rgmii_txd_10[0]}]
##set_property SLEW FAST [get_ports {rgmii_txd_10[1]}]
##set_property SLEW FAST [get_ports {rgmii_txd_10[2]}]
##set_property SLEW FAST [get_ports {rgmii_txd_10[3]}]



##############################################
##     RGMII: IODELAY Constraints
##############################################
set_property IDELAY_VALUE 0 [get_cells {U_MAC1/rgmii_if/rxdata_bus[3].delay_rgmii_rxd}]
set_property IDELAY_VALUE 0 [get_cells {U_MAC1/rgmii_if/rxdata_bus[2].delay_rgmii_rxd}]
set_property IDELAY_VALUE 0 [get_cells {U_MAC1/rgmii_if/rxdata_bus[1].delay_rgmii_rxd}]
set_property IDELAY_VALUE 0 [get_cells {U_MAC1/rgmii_if/rxdata_bus[0].delay_rgmii_rxd}]
set_property IDELAY_VALUE 0 [get_cells {U_MAC2/rgmii_if/rxdata_bus[3].delay_rgmii_rxd}]
set_property IDELAY_VALUE 0 [get_cells {U_MAC2/rgmii_if/rxdata_bus[2].delay_rgmii_rxd}]
set_property IDELAY_VALUE 0 [get_cells {U_MAC2/rgmii_if/rxdata_bus[1].delay_rgmii_rxd}]
set_property IDELAY_VALUE 0 [get_cells {U_MAC2/rgmii_if/rxdata_bus[0].delay_rgmii_rxd}]
##set_property IDELAY_VALUE 0 [get_cells {U_MAC_client/rgmii_if/rxdata_bus[3].delay_rgmii_rxd}]
##set_property IDELAY_VALUE 0 [get_cells {U_MAC_client/rgmii_if/rxdata_bus[2].delay_rgmii_rxd}]
##set_property IDELAY_VALUE 0 [get_cells {U_MAC_client/rgmii_if/rxdata_bus[1].delay_rgmii_rxd}]
##set_property IDELAY_VALUE 0 [get_cells {U_MAC_client/rgmii_if/rxdata_bus[0].delay_rgmii_rxd}]


# Group IODELAY and IDELAYCTRL components to aid placement
set_property IODELAY_GROUP grp1 [get_cells U_MAC1/rgmii_if/delay_rgmii_rx_ctl]
set_property IODELAY_GROUP grp1 [get_cells {U_MAC1/rgmii_if/rxdata_bus[0].delay_rgmii_rxd}]
set_property IODELAY_GROUP grp1 [get_cells {U_MAC1/rgmii_if/rxdata_bus[1].delay_rgmii_rxd}]
set_property IODELAY_GROUP grp1 [get_cells {U_MAC1/rgmii_if/rxdata_bus[2].delay_rgmii_rxd}]
set_property IODELAY_GROUP grp1 [get_cells {U_MAC1/rgmii_if/rxdata_bus[3].delay_rgmii_rxd}]
set_property IODELAY_GROUP grp1 [get_cells U_MAC2/rgmii_if/delay_rgmii_rx_ctl]
set_property IODELAY_GROUP grp1 [get_cells {U_MAC2/rgmii_if/rxdata_bus[0].delay_rgmii_rxd}]
set_property IODELAY_GROUP grp1 [get_cells {U_MAC2/rgmii_if/rxdata_bus[1].delay_rgmii_rxd}]
set_property IODELAY_GROUP grp1 [get_cells {U_MAC2/rgmii_if/rxdata_bus[2].delay_rgmii_rxd}]
set_property IODELAY_GROUP grp1 [get_cells {U_MAC2/rgmii_if/rxdata_bus[3].delay_rgmii_rxd}]
##set_property IODELAY_GROUP grp1 [get_cells U_MAC_client/rgmii_if/delay_rgmii_rx_ctl]
##set_property IODELAY_GROUP grp1 [get_cells {U_MAC_client/rgmii_if/rxdata_bus[0].delay_rgmii_rxd}]
##set_property IODELAY_GROUP grp1 [get_cells {U_MAC_client/rgmii_if/rxdata_bus[1].delay_rgmii_rxd}]
##set_property IODELAY_GROUP grp1 [get_cells {U_MAC_client/rgmii_if/rxdata_bus[2].delay_rgmii_rxd}]
##set_property IODELAY_GROUP grp1 [get_cells {U_MAC_client/rgmii_if/rxdata_bus[3].delay_rgmii_rxd}]



set_property IODELAY_GROUP grp1 [get_cells dlyctrl]

##############################################
##   Setup and Hold time on RGMII inputs
##############################################


set_clock_groups -physically_exclusive -group clkout1 -group clk_rx_00
set_clock_groups -physically_exclusive -group clkout1 -group clk_rx_01
#set_clock_groups -physically_exclusive -group clkout1 -group clk_rx_10

set_clock_groups -physically_exclusive -group clkout0 -group clk_rx_00
set_clock_groups -physically_exclusive -group clkout0 -group clk_rx_01
#set_clock_groups -physically_exclusive -group clkout0 -group clk_rx_10

set_clock_groups -logically_exclusive -group clkout1 -group clkout0
set_clock_groups -logically_exclusive -group clkout1 -group clkout3




