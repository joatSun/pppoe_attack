// **************************************************************        
// COPYRIGHT(c)2021, Xidian University
// All rights reserved.
//
// IP LIB INDEX  : 
// IP Name       : 
//                
// File name     :  tb_toupper.v
// Module name   :  TB_TOUPPER
// Full name     : 
//
// Author        :  shawnli
// Email         :  xwli_ff@126.com
// Data          :   
// Version       :  V 1.0 
// 
// Abstract      :  用于仿真part,to_upper顶层
// Called by     :  Father Module
// 
// This File is  Created on 2021/1/29
// Modification history
// ---------------------------------------------------------------------------------
`timescale 1ns/1ps

`define CLK_PERIOD 5

module TB_TOUPPER;

reg clk;
reg rst;

reg	[31:0]	 	rx_ff_data 		;
reg	[1:0]	 	rx_ff_mod		;
reg				rx_ff_dsav		;
reg				rx_ff_dval		;
reg				rx_ff_sop		;
reg				rx_ff_eop		;

reg 			ff_tx_rdy 		;

wire [31:0]		frame_acquiry 	;
wire 			ff_acq_en 		;
wire [7 :0] 		len 		;

always #`CLK_PERIOD clk = ~clk;

initial begin
	
	clk = 1'b0 ;
	rst = 1'b1;
	rx_ff_sop = 1'b0;
	rx_ff_eop = 1'b0;
	rx_ff_dval = 1'b0;
	rx_ff_dsav = 1'b0;
	rx_ff_data = 32'b0;

	ff_tx_rdy = 1'b1;
	//pppoe_padt = 1'b0;

	#200 
	rst = 1'b0;//复位释放
    
    pap();//模拟padi帧的发送
end
//模拟客户机发来的lcp-terminate-reques数据帧
task pap;
begin
	@(posedge clk)//0
	rx_ff_sop <= 1'b1;
	rx_ff_dval <= 1'b1;
	rx_ff_data <= 32'hffffffff;//fpga伪装的服务器的mac地址（目的mac地址）

	@(posedge clk)//1
	rx_ff_sop <= 1'b0;
	rx_ff_data <= 32'hffff1111;//目的mac以及源mac(用全1表示)

	@(posedge clk)//2
	rx_ff_data <= 32'h11111111;//源mac

	@(posedge clk)//3
	rx_ff_data <= 32'h88641100;//会话阶段帧类型0x8864，版本号0x1，类型域0x1，代码域:0x00

	@(posedge clk)//4
	rx_ff_data <= 32'h00011111;//会话ID：0x0001，长度域：随意吧

	@(posedge clk)//5
	rx_ff_data <= 32'hC023_01_11;//静载荷(ppp数据帧)：Protocol域:C023,code,id，
	//*************************test_example***************************//
	//***0601****
	// @(posedge clk)//6
	// rx_ff_data <= 32'h001c_06_11;//length,id_len,
	// @(posedge clk)//7
	// rx_ff_data <= 32'h22_33_44_55;
	// @(posedge clk)//8
	// rx_ff_data <= 32'h66_01_88_99;
	//***0606****
	// @(posedge clk)//6
	// rx_ff_data <= 32'h001c_06_11;//length,id_len,
	// @(posedge clk)//7
	// rx_ff_data <= 32'h22_33_44_55;
	// @(posedge clk)//8
	// rx_ff_data <= 32'h66_06_88_99;
	// ***061a***
	// @(posedge clk)//6
	// rx_ff_data <= 32'h001c_06_11;//length,id_len,
	// @(posedge clk)//7
	// rx_ff_data <= 32'h22_33_44_55;
	// @(posedge clk)//8
	// rx_ff_data <= 32'h66_1a_88_99;
	//***041c***
	// @(posedge clk)//6
	// rx_ff_data <= 32'h001c_04_11;//length,id_len,
	// @(posedge clk)//7
	// rx_ff_data <= 32'h22_33_44_1c;
	// @(posedge clk)//8
	// rx_ff_data <= 32'h66_77_88_99;
	//***011f***
	@(posedge clk)//6
	rx_ff_data <= 32'h001c_01_11;//length,id_len,
	@(posedge clk)//7
	rx_ff_data <= 32'h1f_33_44_55;
	@(posedge clk)//8
	rx_ff_data <= 32'h66_77_88_99;
	//***0101***
	// @(posedge clk)//6
	// rx_ff_data <= 32'h001c_01_11;//length,id_len,
	// @(posedge clk)//7
	// rx_ff_data <= 32'h01_33_44_55;
	// @(posedge clk)//8
	// rx_ff_data <= 32'h66_77_88_99;
	@(posedge clk)//9
	rx_ff_data <= 32'haa_bb_cc_dd;
	@(posedge clk)//10
	rx_ff_data <= 32'hee_ff_11_22;
	@(posedge clk)//11
	rx_ff_data <= 32'h33_44_55_66;
	@(posedge clk)//12
	rx_ff_data <= 32'h77_88_99_aa;
	@(posedge clk)//13
	rx_ff_data <= 32'hbb_cc_dd_ee;
	
	@(posedge clk)//14
	rx_ff_data <= 32'hff_11_22_33;
	rx_ff_eop <= 1'b1;
	@(posedge clk)
	rx_ff_dval <= 1'b0;
	rx_ff_eop <= 1'b0;
end
endtask

task wait_for_frame;

repeat(100) begin
	@(posedge clk);	
end

endtask

ACQUIRY U_ACQUIRY(
	.clk 		(clk 		)	,
	.rst 		(rst 		)	,
    .ff_rx_data (rx_ff_data )	,
    .ff_rx_mod	(rx_ff_mod	)	,
    .ff_rx_dsav	(rx_ff_dsav	)	,
    .ff_rx_dval	(rx_ff_dval	)	,
    .ff_rx_sop	(rx_ff_sop	)	,
    .ff_rx_eop	(rx_ff_eop	)	,

    .frame_acquiry 	(frame_acquiry 	),
    .ff_acq_en 		(ff_acq_en 		),
    .len 			(len 			) 
);
TO_UPPER U_TO_UPPER(
    .clk(clk)            ,
    .rst(rst)            ,
    //fifo
    .data_acquiry    (frame_acquiry    	),
    .data_acquiry_en (ff_acq_en 		),
    .len             (len             	),
    //mac_upper
    .ff_tx_rdy       (ff_tx_rdy       	),
    .ff_tx_data      (ff_tx_data      	),
    .ff_tx_mod       (ff_tx_mod       	),
    .ff_tx_dval      (ff_tx_dval      	),
    .ff_tx_sop       (ff_tx_sop       	),
    .ff_tx_eop       (ff_tx_eop       	)
);
endmodule