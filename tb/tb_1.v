module TB_1;

`define CLK_PERIOD 5

reg clk;
reg rst;
reg rx_ff_sop;
reg rx_ff_eop;
reg rx_ff_dval;
reg rx_ff_dsav;
reg [31:0] rx_ff_data;
wire [511:0] frame_genarate;
wire frame_valid;

reg pppoe_padt;



always #`CLK_PERIOD clk = ~clk;


initial begin
	
	clk = 1'b0 ;
	rst = 1'b1;
	rx_ff_sop = 1'b0;
	rx_ff_eop = 1'b0;
	rx_ff_dval = 1'b0;
	rx_ff_dsav = 1'b0;
	rx_ff_data = 32'b0;
	pppoe_padt = 1'b0;

	#200 
	rst = 1'b0;//复位释放

    // @(posedge clk)
    
    padi();//模拟padi帧的发送
    wait_for_frame();//模拟收发之间的间隙
    pppoe_padt = 1'b1;
    lcp2();


    /*wait_for_frame();//模拟收发之间的间隙
    padr();//模拟padr帧的发送
    wait_for_frame();//模拟收发之间的间隙
    lcp1();//模拟lcp-configure-ack数据帧*/

end



//模拟收发之间的间隙
task wait_for_frame;

repeat(100) begin
	@(posedge clk);	
end

endtask


//在发现阶段，模拟客户机发来的PADI数据帧
task padi;
begin
	@(posedge clk)
	rx_ff_sop = 1'b1;
	rx_ff_dval = 1'b1;
	rx_ff_data <= 32'hffffffff;//目的mac(广播)

	@(posedge clk)
	rx_ff_sop = 1'b0;
	rx_ff_data <= 32'hffff1111;//目的mac以及源mac(用全1表示)

	@(posedge clk)
	rx_ff_data <= 32'h11111111;//源mac

	@(posedge clk)
	rx_ff_data <= 32'h88631109;//发现阶段帧类型0x8863，版本号0x1，类型域0x1，代码域发现阶段PADI:0x09

	@(posedge clk)
	rx_ff_data <= 32'h00011111;//会话ID：0x0001，长度域：随意吧

	@(posedge clk)
	rx_ff_data <= 32'h11111111;//随意

	@(posedge clk)
	rx_ff_data <= 32'h77777777;//随意
	@(posedge clk)
	rx_ff_data <= 32'h88888888;//随意
	@(posedge clk)
	rx_ff_data <= 32'h99999999;//随意
	@(posedge clk)
	@(posedge clk)
	@(posedge clk)
	@(posedge clk)
	@(posedge clk)
	@(posedge clk)
	@(posedge clk)
	rx_ff_dval = 1'b0;


end
endtask



//在发现阶段，模拟客户机发来的PADR数据帧
task padr;
begin
	@(posedge clk)
	rx_ff_sop = 1'b1;
	rx_ff_dval = 1'b1;
	rx_ff_data <= 32'h0016fcc7;//fpga伪装的服务器的mac地址（目的mac地址）

	@(posedge clk)
	rx_ff_sop = 1'b0;
	rx_ff_data <= 32'ha6171111;//目的mac以及源mac(用全1表示)

	@(posedge clk)
	rx_ff_data <= 32'h11111111;//源mac

	@(posedge clk)
	rx_ff_data <= 32'h88631119;//发现阶段帧类型0x8863，版本号0x1，类型域0x1，代码域发现阶段PADR:0x19

	@(posedge clk)
	rx_ff_data <= 32'h00011111;//会话ID：0x0001，长度域：随意吧

	@(posedge clk)
	rx_ff_dval = 1'b0;
	rx_ff_data <= 32'h11111111;//随意
end
endtask



//模拟客户机发来的lcp-configure-ack数据帧
task lcp1;
begin
	@(posedge clk)
	rx_ff_sop = 1'b1;
	rx_ff_dval = 1'b1;
	rx_ff_data <= 32'h0016fcc7;//fpga伪装的服务器的mac地址（目的mac地址）

	@(posedge clk)
	rx_ff_sop = 1'b0;
	rx_ff_data <= 32'ha6171111;//目的mac以及源mac(用全1表示)

	@(posedge clk)
	rx_ff_data <= 32'h11111111;//源mac

	@(posedge clk)
	rx_ff_data <= 32'h88641109;//会话阶段帧类型0x8864，版本号0x1，类型域0x1，代码域：0x09

	@(posedge clk)
	rx_ff_data <= 32'h00011111;//会话ID：0x0001，长度域：随意吧

	@(posedge clk)
	rx_ff_data <= 32'h7EFF03C0;//静载荷(ppp数据帧)：FLAG:0x7E,Address域:0xFF,Control域:0x03,Protocol域:C023

	@(posedge clk)
	rx_ff_data <= 32'h21020000;//Protocol域:C223,ppp内部代码域(code)：8bit，0x06，Indentifier标识域：8bit

	@(posedge clk)
	@(posedge clk)
	@(posedge clk)
	@(posedge clk)
	@(posedge clk)
	rx_ff_dval = 1'b0;
end
endtask


//模拟客户机发来的lcp-terminate-reques数据帧
task lcp2;
begin
	@(posedge clk)
	rx_ff_sop = 1'b1;
	rx_ff_dval = 1'b1;
	rx_ff_data <= 32'h0016fcc7;//fpga伪装的服务器的mac地址（目的mac地址）

	@(posedge clk)
	rx_ff_sop = 1'b0;
	rx_ff_data <= 32'ha6171111;//目的mac以及源mac(用全1表示)

	@(posedge clk)
	rx_ff_data <= 32'h11111111;//源mac

	@(posedge clk)
	rx_ff_data <= 32'h88641100;//会话阶段帧类型0x8864，版本号0x1，类型域0x1，代码域:0x00

	@(posedge clk)
	rx_ff_data <= 32'h00011111;//会话ID：0x0001，长度域：随意吧

	@(posedge clk)
	rx_ff_data <= 32'hC0210101;//静载荷(ppp数据帧)：FLAG:0x7E,Address域:0xFF,Control域:0x03,Protocol域:C023

	@(posedge clk)
	rx_ff_data <= 32'h00010104;//Protocol域:C223,ppp内部代码域(code)：8bit，0x06，Indentifier标识域：8bit
	@(posedge clk)
	rx_ff_data <= 32'h05c80506;
	@(posedge clk)
	rx_ff_data <= 32'h00000000;
	@(posedge clk)
	@(posedge clk)
	@(posedge clk)
	@(posedge clk)
	rx_ff_dval = 1'b0;
end
endtask




 FP_GEN  U_FP_GEN(
	.clk 			(clk)     ,
	.rst 			(rst)     ,
    .ff_rx_data     (rx_ff_data)     ,
    .ff_rx_mod		()     ,
    .ff_rx_dsav		()     ,
    .ff_rx_dval		(rx_ff_dval)     ,
    .ff_rx_sop		(rx_ff_sop)     ,
    .ff_rx_eop		()     ,
    .pppoe_padt     (pppoe_padt)     ,//use for cut off net access
    .pppoe_lcp 		(1'b0)     ,
    .frame_genarate (frame_genarate)     ,
    .frame_valid    (frame_valid)
);



  FRAME_SEND 	U_FRAME_SEND(
  .clk           (clk)  ,
  .rst           (rst)  ,
  .frame_genarate(frame_genarate)  ,
  .frame_valid   (frame_valid)  ,
  .ff_tx_rdy     (1'b1)  ,
  .ff_tx_data    ()  ,
  .ff_tx_mod     ()  ,
  .ff_tx_dval    ()  ,
  .ff_tx_sop     ()  ,
  .ff_tx_eop     ()  
);


endmodule
