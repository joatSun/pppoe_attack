module TB;

`define CLK_PERIOD 5

 reg  [31:0]    rx_ff_data	;
 reg  [1:0]     rx_ff_mod 	;
 reg            rx_ff_dsav	;
 reg            rx_ff_dval	;
 reg            rx_ff_sop	;
 reg            rx_ff_eop 	;


 wire [31:0] tx_ff_data_mac_00;
 wire [1:0]  tx_ff_mod_mac_00 ;
 wire        tx_ff_dval_mac_00;
 wire        tx_ff_sop_mac_00 ;
 wire        tx_ff_eop_mac_00 ;
 wire        ff_tx_rdy_mac_00 ;

 

(*mark_debug = "true"*) wire [31:0] tx_ff_data_mac_01;
 wire [1:0]  tx_ff_mod_mac_01 ;
 wire        tx_ff_dval_mac_01;
 wire        tx_ff_sop_mac_01 ;
 wire        tx_ff_eop_mac_01 ;
 wire        ff_tx_rdy_mac_01 ;



wire  [31:0]  tx_ff_data1    ;
wire  [1:0]   tx_ff_mod1     ;
wire          tx_ff_dval1    ;
wire          tx_ff_sop1     ;
wire          tx_ff_eop1     ;

wire  [31:0]  tx_ff_data2    ;
wire  [1:0]   tx_ff_mod2     ;
wire          tx_ff_dval2    ;
wire          tx_ff_sop2     ;
wire          tx_ff_eop2     ;


reg    clk;
reg    rst;

wire    [31:0]     frame_acquiry   ;
wire                ff_acq_en      ; 
wire    [7:0]       len            ;



wire	[31:0]  ff_tx_data ;
wire	[1:0]   ff_tx_mod  ;
wire		    ff_tx_dsav ;
wire		    ff_tx_dval ;
wire		    ff_tx_sop  ;
wire		    ff_tx_eop  ;

reg approach_switch;

reg pppoe_padt;
reg pppoe_lcp ;	

reg pppoe_padi;
reg pppoe_ddos;


always #`CLK_PERIOD clk = ~clk;


initial begin
	
	clk = 1'b0 ;
	rst = 1'b1;
	rx_ff_sop = 1'b0;
	rx_ff_eop = 1'b0;
	rx_ff_dval = 1'b0;
	rx_ff_dsav = 1'b0;
	rx_ff_data = 32'b0;
	pppoe_padt = 1'b0;
	approach_switch = 1'b1; //先模拟第一种攻击方式
	pppoe_padt       = 1'b0;
	pppoe_lcp        = 1'b0;
	pppoe_padi      = 1'b0;
	pppoe_ddos      = 1'b0;

	#200 
	rst = 1'b0;//复位释放

    
    padi();//模拟padi帧的发送
    wait_for_frame();//模拟收发之间的间隙
    padr();//模拟padr帧的发送
    wait_for_frame();//模拟收发之间的间隙
    lcp_ack();
    wait_for_frame();//模拟收发之间的间隙
    lcp_req();
    wait_for_frame();//模拟收发之间的间隙
    get_account_code();//模拟账号密码获取


    wait_for_frame();//模拟收发之间的间隙
    pppoe_padt  = 1'b1;//发送padt帧

    wait_for_frame();//模拟收发之间的间隙
    pppoe_lcp  =  1'b1;//发送lcp帧


    #1000
    approach_switch = 1'b0; //模拟第二种攻击方式 
    pppoe_padi      = 1'b1; //令ddos攻击模块发送padi帧


    wait_for_frame();//模拟收发之间的间隙
    pado();//模拟服务器发回的pado帧
    wait_for_frame();//模拟收发之间的间隙
    pppoe_ddos = 1'b1;//模拟ddos攻击
    wait_for_frame();//模拟收发之间的间隙
    pppoe_ddos = 1'b1;//模拟ddos攻击


end



//模拟收发之间的间隙
task wait_for_frame;

repeat(100) begin
	@(posedge clk);	
end

endtask


//在发现阶段，模拟客户机发来的PADI数据帧
task padi;
begin
	@(posedge clk)
	rx_ff_sop = 1'b1;
	rx_ff_dval = 1'b1;
	rx_ff_data <= 32'hffffffff;//目的mac(广播)

	@(posedge clk)
	rx_ff_sop = 1'b0;
	rx_ff_data <= 32'hfffffcaa;//目的mac以及源mac(用全1表示)

	@(posedge clk)
	rx_ff_data <= 32'h14e7da9a;//源mac

	@(posedge clk)
	rx_ff_data <= 32'h88631109;//发现阶段帧类型0x8863，版本号0x1，类型域0x1，代码域发现阶段PADI:0x09

	@(posedge clk)
	rx_ff_data <= 32'h00000014;//会话ID：0x0001，长度域：随意吧

	@(posedge clk)
	rx_ff_data <= 32'h01010000;//随意

	@(posedge clk)
	rx_ff_data <= 32'h77777777;//随意
	@(posedge clk)
	rx_ff_data <= 32'h88888888;//随意
	@(posedge clk)
	rx_ff_data <= 32'h99999999;//随意
	@(posedge clk)
	@(posedge clk)
	@(posedge clk)
	@(posedge clk)
	@(posedge clk)
	@(posedge clk)
	@(posedge clk)
	rx_ff_dval = 1'b0;


end
endtask



//在发现阶段，模拟客户机发来的PADR数据帧
task padr;
begin
	@(posedge clk)
	rx_ff_sop = 1'b1;
	rx_ff_dval = 1'b1;
	rx_ff_data <= 32'h0016fcc7;//fpga伪装的服务器的mac地址（目的mac地址）

	@(posedge clk)
	rx_ff_sop = 1'b0;
	rx_ff_data <= 32'ha617fcaa;//目的mac以及源mac(用全1表示)

	@(posedge clk)
	rx_ff_data <= 32'h14e7da9a;//源mac

	@(posedge clk)
	rx_ff_data <= 32'h88631119;//发现阶段帧类型0x8863，版本号0x1，类型域0x1，代码域发现阶段PADR:0x19

	@(posedge clk)
	rx_ff_data <= 32'h00000014;//会话ID：0x0001，长度域：随意吧

	@(posedge clk)
	@(posedge clk)
	@(posedge clk)
	@(posedge clk)
	@(posedge clk)
	@(posedge clk)
	@(posedge clk)
	@(posedge clk)
	@(posedge clk)
	@(posedge clk)

	@(posedge clk)
	rx_ff_dval = 1'b0;
	rx_ff_data <= 32'h11111111;//随意
end
endtask



//模拟客户机发来的lcp-configure-ack数据帧
task lcp_ack;
begin
	@(posedge clk)
	rx_ff_sop = 1'b1;
	rx_ff_dval = 1'b1;
	rx_ff_data <= 32'h0016fcc7;//fpga伪装的服务器的mac地址（目的mac地址）

	@(posedge clk)
	rx_ff_sop = 1'b0;
	rx_ff_data <= 32'ha617fcaa;//目的mac以及源mac(用全1表示)

	@(posedge clk)
	rx_ff_data <= 32'h14e7da9a;//源mac

	@(posedge clk)
	rx_ff_data <= 32'h88641100;//会话阶段帧类型0x8864，版本号0x1，类型域0x1，代码域：0x09

	@(posedge clk)
	rx_ff_data <= 32'h00a20014;//会话ID：0x0001，长度域：随意吧

	@(posedge clk)
	rx_ff_data <= 32'hc0210201;//静载荷(ppp数据帧)：FLAG:0x7E,Address域:0xFF,Control域:0x03,Protocol域:C023

	@(posedge clk)
	rx_ff_data <= 32'h00120104;//Protocol域:C223,ppp内部代码域(code)：8bit，0x06，Indentifier标识域：8bit

	@(posedge clk)
	rx_ff_data <= 32'h05d40304;
	@(posedge clk)
	rx_ff_data <= 32'hc0230506;
	@(posedge clk)
	rx_ff_data <= 32'h0a1b2c3d;
	@(posedge clk)
	@(posedge clk)
	@(posedge clk)
	@(posedge clk)
	rx_ff_dval = 1'b0;
end
endtask


//模拟客户机发来的lcp-terminate-reques数据帧
task lcp_req;
begin
	@(posedge clk)
	rx_ff_sop = 1'b1;
	rx_ff_dval = 1'b1;
	rx_ff_data <= 32'h0016fcc7;//fpga伪装的服务器的mac地址（目的mac地址）

	@(posedge clk)
	rx_ff_sop = 1'b0;
	rx_ff_data <= 32'ha617fcaa;//目的mac以及源mac(用全1表示)

	@(posedge clk)
	rx_ff_data <= 32'h14e7da9a;//源mac

	@(posedge clk)
	rx_ff_data <= 32'h88641100;//会话阶段帧类型0x8864，版本号0x1，类型域0x1，代码域:0x00

	@(posedge clk)
	rx_ff_data <= 32'h00a20017;//会话ID：0x00a2，长度域：随意吧

	@(posedge clk)
	rx_ff_data <= 32'hC0210100;//静载荷(ppp数据帧)：FLAG:0x7E,Address域:0xFF,Control域:0x03,Protocol域:C023

	@(posedge clk)
	rx_ff_data <= 32'h00150104;//Protocol域:C223,ppp内部代码域(code)：8bit，0x06，Indentifier标识域：8bit
	@(posedge clk)
	rx_ff_data <= 32'h05c80506;
	@(posedge clk)
	rx_ff_data <= 32'h1fca0bb0;
	@(posedge clk)
	rx_ff_data <= 32'h07020802;
	@(posedge clk)
	rx_ff_data <= 32'h0d030600;
	@(posedge clk)
	@(posedge clk)
	@(posedge clk)
	@(posedge clk)
	@(posedge clk)
	@(posedge clk)
	rx_ff_dval = 1'b0;
end
endtask


//模拟客户机传给fpga明文的账号密码
task get_account_code;
begin
	@(posedge clk)
	rx_ff_sop = 1'b1;
	rx_ff_dval = 1'b1;
	rx_ff_data <= 32'h0016fcc7;
	@(posedge clk)
	rx_ff_sop = 1'b0;
	rx_ff_data <= 32'ha617fcaa;
	@(posedge clk)
	rx_ff_data <= 32'h14e7da9a;

	@(posedge clk)
	rx_ff_data <= 32'h88641100;
	@(posedge clk)
	rx_ff_data <= 32'h00a2001e;

	@(posedge clk)
	rx_ff_data <= 32'hC0230105;

	@(posedge clk)
	rx_ff_data <= 32'h001c0b01;//0b为账号长度
	@(posedge clk)
	rx_ff_data <= 32'h06000100;
	@(posedge clk)
	rx_ff_data <= 32'h01010000;
	@(posedge clk)
	rx_ff_data <= 32'h02040b01;
	@(posedge clk)
	rx_ff_data <= 32'h03060506;
	@(posedge clk)
	rx_ff_data <= 32'h05060907;
	@(posedge clk)
	rx_ff_data <= 32'h07020907;
	@(posedge clk)
	rx_ff_data <= 32'h07020907;
	rx_ff_dval = 1'b0;
end
endtask


//在ddos攻击方式中，模拟服务器发来的pado
task pado;
begin
	@(posedge clk)
	rx_ff_sop = 1'b1;
	rx_ff_dval = 1'b1;
	rx_ff_data <= 32'hfcaa14e7;//目的mac(广播)

	@(posedge clk)
	rx_ff_sop = 1'b0;
	rx_ff_data <= 32'hda9a0016;//目的mac以及源mac(用全1表示)

	@(posedge clk)
	rx_ff_data <= 32'hfcc7a617;//源mac

	@(posedge clk)
	rx_ff_data <= 32'h88631107;//发现阶段帧类型0x8863，版本号0x1，类型域0x1，代码域发现阶段PADI:0x09

	@(posedge clk)
	rx_ff_data <= 32'h00000025;//会话ID：0x0001，长度域：随意吧

	@(posedge clk)
	rx_ff_data <= 32'h01010000;//随意

	@(posedge clk)
	rx_ff_data <= 32'h0103000c;//随意
	@(posedge clk)
	rx_ff_data <= 32'h26000000;//随意
	@(posedge clk)
	rx_ff_data <= 32'h00000000;//随意
	@(posedge clk)
	rx_ff_data <= 32'h43000000;//随意
	@(posedge clk)
	rx_ff_data <= 32'h0102000d;//随意
	@(posedge clk)
	rx_ff_data <= 32'h58444258;//随意
	@(posedge clk)
	rx_ff_data <= 32'h515f4d45;//随意
	@(posedge clk)
	rx_ff_data <= 32'h36302d58;//随意
	@(posedge clk)
	rx_ff_data <= 32'h38000000;//随意
	@(posedge clk)
	rx_ff_dval = 1'b0;


end
endtask


//第一种攻击方式：steal the account and code,contact with the client
FP_GEN U_FP_GEN(
	.clk 			(clk          		),
	.rst 			(rst 	             	),
	.switch         (approach_switch        ),
    .ff_rx_data 	(rx_ff_data		),
    .ff_rx_mod		(rx_ff_mod 		),
    .ff_rx_dsav		(rx_ff_dsav		),
    .ff_rx_dval		(rx_ff_dval		),
    .ff_rx_sop		(rx_ff_sop		),
    .ff_rx_eop		(rx_ff_eop 		),
    .ff_tx_rdy      (1'b1 		),
    .pppoe_padt 	(pppoe_padt		 		),//use for cut off net access
    .pppoe_lcp 		(pppoe_lcp				),
    .ff_tx_data     (tx_ff_data1            ), 
    .ff_tx_mod      (tx_ff_mod1             ), 
    .ff_tx_dval     (tx_ff_dval1            ), 
    .ff_tx_sop      (tx_ff_sop1             ), 
    .ff_tx_eop      (tx_ff_eop1             ), 
    .frame_acquiry  (frame_acquiry          ),
    .ff_acq_en      (ff_acq_en              ),
    .len            (len) 
);



/*pppoeattack_authen_forward  U_pppoeattack_authen_forward(

    .clk             (Clk_user),
    .rst             (rst),
    .frame_acquiry   (frame_acquiry),
    .ff_acq_en       (ff_acq_en),
    .frame_data_out      (frame_data_out),
    .frame_data_en_out   (frame_data_en_out)

    );*/

//第二种攻击方式：ddos attack
DDOS_TOP U_DDOS_TOP(
    .clk         (clk), 
    .rst         (rst), 
    .rx_data     (rx_ff_data		), 
    .rx_mod      (rx_ff_mod 		), 
    .rx_dsav     (rx_ff_dsav		), 
    .rx_dval     (rx_ff_dval		), 
    .rx_sop      (rx_ff_sop		), 
    .rx_eop      (rx_ff_eop 		), 
    .pppoe_padi  (pppoe_padi), 
    .pppoe_ddos  (pppoe_ddos), 
    .tx_rdy      (1'b1),
    .tx_data     (tx_ff_data2),
    .tx_mod      (tx_ff_mod2 ), 
    .tx_dval     (tx_ff_dval2), 
    .tx_sop      (tx_ff_sop2 ), 
    .tx_eop      (tx_ff_eop2 )

    );

//根据approach_switch信号决定哪种攻击方式的输出与1口相连
CHOOSE U_CHOOSE(
        .clk                     (clk),
        .rst                     (rst),
        .tx_data1                (tx_ff_data1),
        .tx_mod1                 (tx_ff_mod1),
        .tx_dval1                (tx_ff_dval1),
        .tx_sop1                 (tx_ff_sop1),
        .tx_eop1                 (tx_ff_eop1),
        .tx_data2                (tx_ff_data2),
        .tx_mod2                 (tx_ff_mod2),
        .tx_dval2                (tx_ff_dval2),
        .tx_sop2                 (tx_ff_sop2),
        .tx_eop2                 (tx_ff_eop2),
        .switch                  (approach_switch),
        .tx_ff_data_mac_00       (tx_ff_data_mac_00),
        .tx_ff_mod_mac_00        (tx_ff_mod_mac_00),
        .tx_ff_dval_mac_00       (tx_ff_dval_mac_00),
        .tx_ff_sop_mac_00        (tx_ff_sop_mac_00),
        .tx_ff_eop_mac_00        (tx_ff_eop_mac_00)


    );

//实现账号密码的提取组帧，与2口相连
TO_UPPER U_TO_UPPER(
        .clk               (clk),
        .rst               (rst),
        .data_acquiry      (frame_acquiry),
        .data_acquiry_en   ( ff_acq_en   ),
        .len               ( len         ),
        .ff_tx_rdy         (1'b1),
        .ff_tx_data        (tx_ff_data_mac_01),
        .ff_tx_mod         (tx_ff_mod_mac_01),
        .ff_tx_dval        (tx_ff_dval_mac_01),
        .ff_tx_sop         (tx_ff_sop_mac_01),
        .ff_tx_eop         (tx_ff_eop_mac_01)
    );



endmodule