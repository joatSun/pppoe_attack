`timescale 1ns / 1ps
//////////////////////////////////////////////////////////////////////////////////
// Company: 
// Engineer: 
// 
// Create Date: 2021/01/24 15:10:55
// Design Name: 
// Module Name: DDOS_TB
// Project Name: 
// Target Devices: 
// Tool Versions: 
// Description: 
// 
// Dependencies: 
// 
// Revision:
// Revision 0.01 - File Created
// Additional Comments:
// 
//////////////////////////////////////////////////////////////////////////////////
`define CLK_PERIOD 5

module DDOS_TB;


reg                   clk                      ; 
reg                   rst                      ; 
reg                   rx_ff_sop                ; 
reg                   rx_ff_eop                ; 
reg                   rx_ff_dval               ; 
//reg                   rx_ff_dsav               ; 
reg     [  31:   0]   rx_ff_data               ; 
reg                   pppoe_padi               ;
reg                   pppoe_ddos               ;
always #`CLK_PERIOD clk = ~clk;


initial begin
	
	clk = 1'b0 ;
	rst = 1'b1;
	rx_ff_sop = 1'b0;
	rx_ff_eop = 1'b0;
	rx_ff_dval = 1'b0;
	rx_ff_data = 32'b0;
	pppoe_padi =1'b0;
	pppoe_ddos = 1'b0;

	#200 
	rst = 1'b0;//复位释放
    wait_for_frame();
    pppoe_padi = 1'b1;
    wait_for_frame();
    pppoe_padi = 1'b0;
    pado();//模拟padi帧的发送
    wait_for_frame;
    pppoe_ddos = 1'b1;
    wait_for_frame;
    pppoe_ddos = 1'b0;
    #1000
    $stop;

end



//模拟收发之间的间隙
task wait_for_frame;

repeat(32) begin
	@(posedge clk);	
end

endtask


//在发现阶段，模拟客户机发来的PADI数据帧
task pado;
begin
	@(posedge clk)
	rx_ff_sop = 1'b1;
	rx_ff_dval = 1'b1;
	rx_ff_data <= 32'h244bfe78;
	@(posedge clk)
	rx_ff_sop = 1'b0;
	rx_ff_data <= 32'h544d94db;
	@(posedge clk)
	rx_ff_data <= 32'hda3e8fcf;
	@(posedge clk)
	rx_ff_data <= 32'h88631107;
	@(posedge clk)
	rx_ff_data <= 32'h0000_0025;
	@(posedge clk)
	rx_ff_data <= 32'h0101_0000;
	@(posedge clk)
	rx_ff_data <= 32'h01030_00c;
	@(posedge clk)
	rx_ff_data <= 32'h0100_0000;
	@(posedge clk)
	rx_ff_data <= 32'h0000_0000;
	@(posedge clk)
	rx_ff_data <= 32'h0100_0000;
	@(posedge clk)
	rx_ff_data <= 32'h0102_000d;
	@(posedge clk)
	rx_ff_data <= 32'h5844_4258;
	@(posedge clk)
	rx_ff_data <= 32'h515f_4d45;
	@(posedge clk)
	rx_ff_data <= 32'h3630_2d58;
	@(posedge clk)
	rx_ff_data <= 32'h3800_0000;
	rx_ff_eop  <= 1'b1;
	@(posedge clk)
	rx_ff_data <= 32'h0000_0000;
	rx_ff_eop  <= 1'b0;
	rx_ff_dval <= 1'b0;
end
endtask
/*********instance module***************/
DDOS_TOP U_DOSS_TOP(
    .clk                  (clk                    ),
    .rst                  (rst                    ),
    .rx_data              (rx_ff_data             ),
    .rx_mod               (                       ),
    .rx_dsav              (                       ),
    .rx_dval              (rx_ff_dval             ),
    .rx_sop               (rx_ff_sop              ),
    .rx_eop               (rx_ff_eop              ),
    .pppoe_padi           (pppoe_padi             ),
    .pppoe_ddos           (pppoe_ddos             ),
    .tx_rdy               (1'b1                   ),
    .tx_data              (                       ),
    .tx_mod               (                       ),
    .tx_dval              (                       ),
    .tx_sop               (                       ),
    .tx_eop               (                       )
);

endmodule
