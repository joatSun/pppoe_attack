// **************************************************************        
// COPYRIGHT(c)2021, Xidian University
// All rights reserved.
//
// IP LIB INDEX  : 
// IP Name       : 
//                
// File name     :  frame_send.v
// Module name   :  FRAME_SEND
// Full name     : 
//
// Author        :  shawnli
// Email         :  xwli_ff@126.com
// Data          :   
// Version       :  V 1.0 
// 
// Abstract      :  
// Called by     :  Father Module
// 
// This File is  Created on 2021/1/22
// Modification history
// ---------------------------------------------------------------------------------
`timescale 1ns/1ps
module FRAME_SEND(
    input               clk             ,
    input               rst             ,
    input   [479:0]     frame_genarate  ,
    input               frame_valid     ,
  (*mark_debug = "true"*)  input               ff_tx_rdy       ,
  (*mark_debug = "true"*)  output reg [31:0]   ff_tx_data      ,
    output reg [1:0]    ff_tx_mod       ,
    output reg          ff_tx_dval      ,
    output reg          ff_tx_sop       ,
    output reg          ff_tx_eop       
);


reg                 send_en         ;
reg     [479:0]     frame_reg       ;
reg     [3:0]       send_cnt        ;
always @(posedge clk or posedge rst) begin
    if (rst) begin
        send_en <= 1'b0;
    end
    else if (frame_valid == 1'b1) begin
        send_en <= 1'b1;
    end
    else if (send_cnt == 4'b1110) begin
        send_en <= 1'b0;
    end
    else begin
        send_en <= send_en;
    end
end

always @(posedge clk or posedge rst) begin
    if (rst) begin
        frame_reg <= 480'b0;
    end
    else if (frame_valid == 1'b1) begin
        frame_reg <= frame_genarate;
    end
    else begin
        frame_reg <= frame_reg ; 
    end
end

always @(posedge clk or posedge rst) begin
    if (rst) begin
        send_cnt <= 4'b0;
    end
    else if (send_en == 1'b1) begin
        send_cnt <= send_cnt + 4'b1;
    end
    else begin
        send_cnt <= 4'b0;
    end
end

always @(posedge clk or posedge rst) begin
    if (rst) begin
        ff_tx_data      <= 'b0;
        ff_tx_mod       <= 'b0;
        ff_tx_dval      <= 'b0;
        ff_tx_sop       <= 'b0;
        ff_tx_eop       <= 'b0;
    end
    else if ((send_en == 1'b1)&(ff_tx_rdy == 1'b1)) begin
        case(send_cnt)
            4'b0000: begin
                ff_tx_data      <= frame_reg[479:448];
                ff_tx_mod       <= 'b0;
                ff_tx_dval      <= 'b1;
                ff_tx_sop       <= 'b1;
                ff_tx_eop       <= 'b0;
            end
            4'b0001: begin
                ff_tx_data      <= frame_reg[447:416];
                ff_tx_mod       <= 'b0;
                ff_tx_dval      <= 'b1;
                ff_tx_sop       <= 'b0;
                ff_tx_eop       <= 'b0;
            end
            4'b0010: begin
                ff_tx_data      <= frame_reg[415:384];
                ff_tx_mod       <= 'b0;
                ff_tx_dval      <= 'b1;
                ff_tx_sop       <= 'b0;
                ff_tx_eop       <= 'b0;
            end
            4'b0011: begin
                ff_tx_data      <= frame_reg[383:352];
                ff_tx_mod       <= 'b0;
                ff_tx_dval      <= 'b1;
                ff_tx_sop       <= 'b0;
                ff_tx_eop       <= 'b0;
            end
            4'b0100: begin
                ff_tx_data      <= frame_reg[351:320];
                ff_tx_mod       <= 'b0;
                ff_tx_dval      <= 'b1;
                ff_tx_sop       <= 'b0;
                ff_tx_eop       <= 'b0;
            end
            4'b0101: begin
                ff_tx_data      <= frame_reg[319:288];
                ff_tx_mod       <= 'b0;
                ff_tx_dval      <= 'b1;
                ff_tx_sop       <= 'b0;
                ff_tx_eop       <= 'b0;
            end 
            4'b0110: begin
                ff_tx_data      <= frame_reg[287:256];
                ff_tx_mod       <= 'b0;
                ff_tx_dval      <= 'b1;
                ff_tx_sop       <= 'b0;
                ff_tx_eop       <= 'b0;
            end 
            4'b0111: begin
                ff_tx_data      <= frame_reg[255:224];
                ff_tx_mod       <= 'b0;
                ff_tx_dval      <= 'b1;
                ff_tx_sop       <= 'b0;
                ff_tx_eop       <= 'b0;
            end 
            4'b1000: begin
                ff_tx_data      <= frame_reg[223:192];
                ff_tx_mod       <= 'b0;
                ff_tx_dval      <= 'b1;
                ff_tx_sop       <= 'b0;
                ff_tx_eop       <= 'b0;
            end 
            4'b1001: begin
                ff_tx_data      <= frame_reg[191:160];
                ff_tx_mod       <= 'b0;
                ff_tx_dval      <= 'b1;
                ff_tx_sop       <= 'b0;
                ff_tx_eop       <= 'b0;
            end
            4'b1010: begin
                ff_tx_data      <= frame_reg[159:128];
                ff_tx_mod       <= 'b0;
                ff_tx_dval      <= 'b1;
                ff_tx_sop       <= 'b0;
                ff_tx_eop       <= 'b0;
            end
            4'b1011: begin
                ff_tx_data      <= frame_reg[127:96];
                ff_tx_mod       <= 'b0;
                ff_tx_dval      <= 'b1;
                ff_tx_sop       <= 'b0;
                ff_tx_eop       <= 'b0;
            end
            4'b1100: begin
                ff_tx_data      <= frame_reg[95:64];
                ff_tx_mod       <= 'b0;
                ff_tx_dval      <= 'b1;
                ff_tx_sop       <= 'b0;
                ff_tx_eop       <= 'b0;
            end
            4'b1101: begin
                ff_tx_data      <= frame_reg[63:32];
                ff_tx_mod       <= 'b0;
                ff_tx_dval      <= 'b1;
                ff_tx_sop       <= 'b0;
                ff_tx_eop       <= 'b0;
            end 
            4'b1110: begin
                ff_tx_data      <= frame_reg[31:0];
                ff_tx_mod       <= 'b0;
                ff_tx_dval      <= 'b1;
                ff_tx_sop       <= 'b0;
                ff_tx_eop       <= 'b1;
            end 
            default: begin
                ff_tx_data      <= 32'b0;
                ff_tx_mod       <= 'b0;
                ff_tx_dval      <= 'b0;
                ff_tx_sop       <= 'b0;
                ff_tx_eop       <= 'b0;                
            end              
        endcase
    end
    else begin
    	ff_tx_data      <= 'b0;
        ff_tx_mod       <= 'b0;
        ff_tx_dval      <= 'b0;
        ff_tx_sop       <= 'b0;
        ff_tx_eop       <= 'b0;
    end
end
endmodule