// **************************************************************        
// COPYRIGHT(c)2021, Xidian University
// All rights reserved.
//
// IP LIB INDEX  : 
// IP Name       : 
//                
// File name     :  to_upper.v
// Module name   :  TO_UPPER
// Full name     : 
//
// Author        :  shawnli
// Email         :  xwli_ff@126.com
// Data          :   
// Version       :  V 1.0 
// 
// Abstract      :  完成特殊格式UDP后部分组帧，并发往上位机MAC
// Called by     :  Father Module
// 
// This File is  Created on 2021/1/29
// Modification history
// ---------------------------------------------------------------------------------
`timescale 1ns/1ps

`define DEST_ADDR_1     32'hffff_ffff
`define DEST_ADDR_2     16'hffff
`define SOUR_ADDR_1     16'h1111
`define SOUR_ADDR_2     32'h1111_1111
module TO_UPPER(
    input                   clk             ,
    input                   rst             ,
    //fifo
    input   [31:0]          data_acquiry    ,
    input                   data_acquiry_en ,
    input   [7:0]           len             ,
    //mac_upper
    input                   ff_tx_rdy       ,
    output  reg     [31:0]  ff_tx_data      ,
    output  reg     [1:0]   ff_tx_mod       ,
    output  reg             ff_tx_dval      ,
    output  reg             ff_tx_sop       ,
    output  reg             ff_tx_eop       
);
// wire                data_acquiry_en     ;
reg     [3:0]       send_cnt                ;

reg     [31:0]      data_acquiry_f          ;
reg     [31:0]      data_acquiry_ff         ;
reg     [31:0]      data_acquiry_fff        ;
reg     [31:0]      data_acquiry_4f         ;
reg     [31:0]      data_acquiry_5f         ;
//reg     [31:0]      data_acquiry_6f         ;

always @(posedge clk or posedge rst) begin
    if (rst) begin
        data_acquiry_f      <= 32'b0;
        data_acquiry_ff     <= 32'b0;
        data_acquiry_fff    <= 32'b0;
        data_acquiry_4f     <= 32'b0;
        data_acquiry_5f     <= 32'b0;
        //data_acquiry_6f     <= 32'b0;
    end
    else begin
        data_acquiry_f      <= data_acquiry;
        data_acquiry_ff     <= data_acquiry_f;
        data_acquiry_fff    <= data_acquiry_ff;
        data_acquiry_4f     <= data_acquiry_fff;
        data_acquiry_5f     <= data_acquiry_4f;
        //data_acquiry_6f     <= data_acquiry_5f;
    end
end
reg send_en;
always @(posedge clk or posedge rst) begin
    if (rst) begin
        send_cnt <= 4'b0;
    end
    else if (send_en == 1'b1) begin
        send_cnt <= send_cnt + 4'b1;
    end
    else begin
        send_cnt <= 4'b0;
    end
end
always @(posedge clk or posedge rst) begin
    if (rst) begin
        send_en <= 1'b0;
    end
    else if (data_acquiry_en == 1'b1) begin
        send_en <= 1'b1;
    end
    else if (send_cnt == 4'd14) begin
        send_en <= 1'b0;
    end
    else begin
        send_en <= send_en;
    end
end
//len cnt
wire [5:0] len_cnt;
wire [1:0] len_mod;
assign len_cnt = (len[1:0] == 2'b0)?len[7:2] - 6'd1:len[7:2];
assign len_mod = len[1:0];

always @(posedge clk or posedge rst) begin
    if (rst) begin
        ff_tx_data      <= 'b0;
        ff_tx_mod       <= 'b0;
        ff_tx_dval      <= 'b0;
        ff_tx_sop       <= 'b0;
        ff_tx_eop       <= 'b0;
    end
    else if ((send_en == 1'b1)&(ff_tx_rdy == 1'b1)&(send_cnt < 4'd4)) begin
        case(send_cnt)
            4'd0 : begin
                ff_tx_data      <= `DEST_ADDR_1;
                ff_tx_mod       <= 'b0;
                ff_tx_dval      <= 'b1;
                ff_tx_sop       <= 'b1;
                ff_tx_eop       <= 'b0;
            end
            4'b1 : begin
                ff_tx_data      <= {`DEST_ADDR_2,`SOUR_ADDR_1};
                ff_tx_mod       <= 'b0;
                ff_tx_dval      <= 'b1;
                ff_tx_sop       <= 'b0;
                ff_tx_eop       <= 'b0;
            end
            4'd2 : begin
                ff_tx_data      <= `SOUR_ADDR_2;
                ff_tx_mod       <= 'b0;
                ff_tx_dval      <= 'b1;
                ff_tx_sop       <= 'b0;
                ff_tx_eop       <= 'b0;
            end
            4'd3 : begin        // 帧类型0x8817|负载起始符x02|数据起始符x23
                ff_tx_data      <= 32'h0x8817_02_23;
                ff_tx_mod       <= 'b0;
                ff_tx_dval      <= 'b1;
                ff_tx_sop       <= 'b0;
                ff_tx_eop       <= 'b0;
            end
            default: begin
                ff_tx_data      <= 32'b0;
                ff_tx_mod       <= 'b0;
                ff_tx_dval      <= 'b0;
                ff_tx_sop       <= 'b0;
                ff_tx_eop       <= 'b0;                
            end
        endcase
    end
    else if ((send_en == 1'b1)&(ff_tx_rdy == 1'b1)&(send_cnt < 4'd14)) begin
        if (send_cnt == len_cnt) begin
            case(len_mod)
               2'b01: begin
                    ff_tx_data      <= {data_acquiry_5f[31:24],8'h24,16'b0}; 
                    ff_tx_mod       <= 'b0;
                    ff_tx_dval      <= 'b1;
                    ff_tx_sop       <= 'b0;
                    ff_tx_eop       <= 'b0; 
               end
               2'b10: begin
                    ff_tx_data      <= {data_acquiry_5f[31:16],8'h24,8'b0}; 
                    ff_tx_mod       <= 'b0;
                    ff_tx_dval      <= 'b1;
                    ff_tx_sop       <= 'b0;
                    ff_tx_eop       <= 'b0;   
               end
               2'b11: begin
                    ff_tx_data      <= {data_acquiry_5f[31:8],8'h24}; 
                    ff_tx_mod       <= 'b0;
                    ff_tx_dval      <= 'b1;
                    ff_tx_sop       <= 'b0;
                    ff_tx_eop       <= 'b0; 
               end
               default:begin
                    ff_tx_data      <= data_acquiry_5f[31:0]; 
                    ff_tx_mod       <= 'b0;
                    ff_tx_dval      <= 'b1;
                    ff_tx_sop       <= 'b0;
                    ff_tx_eop       <= 'b0; 
               end
            endcase            
        end
        else if ((send_cnt == len_cnt + 1)&(len_mod == 2'b00)) begin
            ff_tx_data      <= {8'h24,24'b0};
            ff_tx_mod       <= 'b0;
            ff_tx_dval      <= 'b1;
            ff_tx_sop       <= 'b0;
            ff_tx_eop       <= 'b0; 
        end
        else if (send_cnt < len_cnt) begin
            ff_tx_data      <= data_acquiry_5f[31:0];
            ff_tx_mod       <= 'b0;
            ff_tx_dval      <= 'b1;
            ff_tx_sop       <= 'b0;
            ff_tx_eop       <= 'b0; 
        end
        else begin
            ff_tx_data      <= 32'b0;
            ff_tx_mod       <= 'b0;
            ff_tx_dval      <= 'b1;
            ff_tx_sop       <= 'b0;
            ff_tx_eop       <= 'b0;             
        end
    end
    else if ((send_cnt == 4'd14)&(len_cnt == 4'd14)) begin
        case(len_mod)
           2'b01: begin
                ff_tx_data      <= {data_acquiry_5f[31:24],8'h24,16'b0}; 
                ff_tx_mod       <= 'b0;
                ff_tx_dval      <= 'b1;
                ff_tx_sop       <= 'b0;
                ff_tx_eop       <= 'b1; 
           end
           2'b10: begin
                ff_tx_data      <= {data_acquiry_5f[31:16],8'h24,8'b0}; 
                ff_tx_mod       <= 'b0;
                ff_tx_dval      <= 'b1;
                ff_tx_sop       <= 'b0;
                ff_tx_eop       <= 'b1;   
           end
           2'b11: begin
                ff_tx_data      <= {data_acquiry_5f[31:8],8'h24}; 
                ff_tx_mod       <= 'b0;
                ff_tx_dval      <= 'b1;
                ff_tx_sop       <= 'b0;
                ff_tx_eop       <= 'b1; 
           end
           default:begin
                ff_tx_data      <= 32'b0; 
                ff_tx_mod       <= 'b0;
                ff_tx_dval      <= 'b1;
                ff_tx_sop       <= 'b0;
                ff_tx_eop       <= 'b1; 
           end
        endcase            
    end
    else if ((send_en == 1'b1)&(ff_tx_rdy == 1'b1)) begin
        ff_tx_data      <= 32'b0; 
        ff_tx_mod       <= 'b0;
        ff_tx_dval      <= 'b1;
        ff_tx_sop       <= 'b0;
        ff_tx_eop       <= 'b1;                     
    end
    else begin
        ff_tx_data      <= 32'b0; 
        ff_tx_mod       <= 'b0;
        ff_tx_dval      <= 'b0;
        ff_tx_sop       <= 'b0;
        ff_tx_eop       <= 'b0;                   
    end            
end
endmodule