// **************************************************************        
// COPYRIGHT(c)2021, Xidian University
// All rights reserved.
//
// IP LIB INDEX  : 
// IP Name       : 
//                
// File name     :  fp_gen.v
// Module name   :  FP_GEN
// Full name     : 
//
// Author        :  shawnli
// Email         :  xwli_ff@126.com
// Data          :   
// Version       :  V 1.0  
// 
// Abstract      :	modified by ck 1.28 ,avoid using the reg with the width of 480 bits 
// Called by     :  Father Module
// 
// This File is  Created on 2021/1/22
// Modification history
// ---------------------------------------------------------------------------------
`timescale 1ns/1ps
`define WIDTH_CNT 			11
//PPPoE frame head
`define SERVER_ADDR 		48'h0016fcc7a617//group 48'h01005e7ffffa //01005e000016 01005e0000fb 01-00-5e-00-00-fc 
`define FRAME_TYPE_DISC 	16'h8863
`define FRAME_TYPE_SESS 	16'h8864
`define VERSION 			4'h1
`define TYPE 				4'h1
`define CODE_PADI 			8'h09
`define CODE_PADO 			8'h07
`define CODE_PADR 			8'h19
`define CODE_PADS 			8'h65
`define CODE_PADT 			8'ha7
`define CODE_SESSION 		8'h00 
`define SESSION_ID_PADO 	16'h0000
`define SESSION_ID_AC 		16'h00a2 	//Access concentrator generate only PPPoE flag
`define LENGTH_PADO 		16'h0025 	//????
`define LENGTH_PADS 		16'h0014 	//????
`define LENGTH_PADT			16'h0000
//
`define TAG_PADO 			64'h010100000103_000c
`define TAG_AC 				32'h0102000d
`define AC_NAME  			104'h58444258515f4d4536302d5838			
`define LENGTH_LCP 			16'h0014
`define LENGTH_PAP 			16'h0008
//PPP frame head
`define PPP_FLAG 			8'h7e
`define PPP_ADDR 			8'hff
`define PPP_CTRL 			8'h03
`define PPP_LCP 			16'hc021
`define PPP_PAP 			16'hc023
module FP_GEN(
	input 				clk 			,
	input 				rst 			,
    input               switch          ,

    input 	[31:0]	 	ff_rx_data 		,
    input 	[1:0]	 	ff_rx_mod		,
    input 				ff_rx_dsav		,
    input 				ff_rx_dval		,
    input 				ff_rx_sop		,
    input 				ff_rx_eop		,
    input               ff_tx_rdy       ,
    input 				pppoe_padt 		,//use for cut off net access
    input 				pppoe_lcp 		,

    output reg 	[31:0]  ff_tx_data      ,
    output reg 	[1:0]   ff_tx_mod       ,
    output reg 		    ff_tx_dval      ,
    output reg 		    ff_tx_sop       ,
    output reg 		    ff_tx_eop       ,
    /*output 	reg [479:0]	frame_genarate 	,
    output 	reg			frame_valid 	,*/
    output 	reg [31:0]	frame_acquiry 	,
    output 	reg 		ff_acq_en       ,
    output  reg	[7 :0] 	len 
);

reg  	[31:0]	 	ff_rx_data_f 		;
reg  	[1:0]	 	ff_rx_mod_f			;
reg  				ff_rx_dsav_f		;
reg  				ff_rx_dval_f		;
reg  				ff_rx_sop_f			;
reg  				ff_rx_eop_f			;

reg  	[31:0]	 	ff_rx_data_ff 		;
reg  	[1:0]	 	ff_rx_mod_ff     	;
reg  				ff_rx_dsav_ff		;
reg  				ff_rx_dval_ff		;
reg  				ff_rx_sop_ff		;
reg  				ff_rx_eop_ff		;

reg 	[`WIDTH_CNT:0] cnt_parse 		;

reg 	[47:0] 		src_addr 			;
reg 	[15:0] 		frame_type 			;
reg 	[7:0] 		code_type 			;
reg 	[23:0] 		ppp_head 			;
reg 	[15:0] 		ppp_protocol 		;
reg 	[7:0] 		inner_code_type 	;

reg 	[479:0] 	frame_pado 			; 
reg 	[479:0] 	frame_pads 			; 
reg 	[479:0] 	frame_lcp 			;
reg 	[479:0] 	frame_pap 			;
reg 	[479:0] 	frame_padt			;

reg 				frame_pado_en 		;
reg 				frame_pads_en 		;
wire 				frame_lcp_en 		;
reg 				frame_pap_en 		;
wire 				frame_padt_en 		;
wire 	[5:0] 		frame_en 			;

reg 				pppoe_padt_f 		;
reg 				pppoe_lcp_f 		;


//对输入的数据打一拍、两拍
always @(posedge clk or posedge rst) begin
	if (rst) begin
		ff_rx_data_f 		<= 'd0;
		ff_rx_mod_f			<= 'd0;
		ff_rx_dsav_f		<= 'd0;
		ff_rx_dval_f		<= 'd0;
		ff_rx_sop_f			<= 'd0;
		ff_rx_eop_f			<= 'd0;
		ff_rx_data_ff 		<= 'd0;
		ff_rx_mod_ff		<= 'd0;
		ff_rx_dsav_ff		<= 'd0;
		ff_rx_dval_ff		<= 'd0;
		ff_rx_sop_ff		<= 'd0;
		ff_rx_eop_ff		<= 'd0;
	end
	else begin
		ff_rx_data_f 		<= ff_rx_data 		;
		ff_rx_mod_f			<= ff_rx_mod		;
		ff_rx_dsav_f		<= ff_rx_dsav		;
		ff_rx_dval_f		<= ff_rx_dval		;
		ff_rx_sop_f			<= ff_rx_sop		;
		ff_rx_eop_f			<= ff_rx_eop		;
		ff_rx_data_ff 		<= ff_rx_data_f 	;
		ff_rx_mod_ff		<= ff_rx_mod_f		;
		ff_rx_dsav_ff		<= ff_rx_dsav_f		;
		ff_rx_dval_ff		<= ff_rx_dval_f		;
		ff_rx_sop_ff		<= ff_rx_sop_f		;
		ff_rx_eop_ff		<= ff_rx_eop_f		;
	end
end


//数据技术器
always @(posedge clk or posedge rst) begin
	if (rst) begin
		cnt_parse <= 'd0;
	end
	else if (ff_rx_dval_f == 1'b1) begin
		cnt_parse <= cnt_parse + 'b1;
	end
	else begin
		cnt_parse <= 'd0;
	end
end
//read source address
always @(posedge clk or posedge rst) begin
	if (rst) begin
		src_addr <= 48'd0;
	end
	else if (cnt_parse == 'b1) begin
		src_addr[47:32] <= ff_rx_data_f[15:0];
	end
	else if (cnt_parse == 'd2) begin
		src_addr[31:0] <= ff_rx_data_f;
	end
	else begin
		src_addr <= src_addr;
	end
end
//得到帧类型域的数值
always @(posedge clk or posedge rst) begin
	if (rst) begin
		frame_type <= 16'b0;
	end
	else if (cnt_parse == 16'd3) begin
		frame_type <= ff_rx_data_f[31:16];
	end
	else begin
		frame_type <= frame_type;
	end
end
//read code_type
always @(posedge clk or posedge rst) begin
	if (rst) begin
		code_type <= 8'h00;
	end
	else if ((frame_type == `FRAME_TYPE_DISC)&(cnt_parse == 'd4)) begin
		code_type <= ff_rx_data_ff[7:0];
	end
	else if (cnt_parse == 'd12) begin
		code_type <= 8'h00;
	end
	else begin
		code_type <= code_type;
	end
end
//read data field length
// reg [15:0] data_field_len;
// always @(posedge clk or posedge rst) begin
// 	if (rst) begin
// 		data_field_len <= 16'b0;
// 	end
// 	else if ((frame_type == `FRAME_TYPE_DISC)&(cnt_parse == 'd4)) begin
// 		data_field_len <= ff_rx_data_f[15:0];
// 	end
// 	else begin
// 		data_field_len <= data_field_len;
// 	end
// end
//read tag
reg [95:0] host_uniq;
always @(posedge clk or posedge rst) begin
	if (rst) begin
		host_uniq <= 96'b0;
	end
	else if (frame_type == `FRAME_TYPE_DISC) begin
		case(cnt_parse)
			'd7: host_uniq[95:64] <= ff_rx_data_f;
			'd8: host_uniq[63:32] <= ff_rx_data_f;
			'd9: host_uniq[31:0]  <= ff_rx_data_f;
		endcase
	end
end
//read PPP head
// always @(posedge clk or posedge rst) begin
// 	if (rst) begin
// 		ppp_head <= 24'b0;
// 	end
// 	else if ((frame_type == `FRAME_TYPE_SESS)&(cnt_parse == 'd5)) begin
// 		ppp_head <= ff_rx_data_f[31:8];
// 	end
// 	else begin
// 		ppp_head <= ppp_head;
// 	end
// end
//read PPP protocol
always @(posedge clk or posedge rst) begin
	if (rst) begin
		ppp_protocol <= 16'b0;
	end
	else if ((frame_type == `FRAME_TYPE_SESS)&(cnt_parse == 'd5)) begin
		ppp_protocol <= ff_rx_data_f[31:16];
	end
	else begin
		ppp_protocol <= ppp_protocol;
	end
end
//read inner_code_type
always @(posedge clk or posedge rst) begin
	if (rst) begin
		inner_code_type <= 8'b0;
	end
	else if ((ppp_protocol == 16'hc021)&(cnt_parse == 'd6)) begin
		inner_code_type <= ff_rx_data_ff[15:8];
	end
	else if (cnt_parse == 'd15) begin
		inner_code_type <= 8'b0;
	end
	else begin
		inner_code_type <= inner_code_type;
	end
end
//read inner_id
reg [7:0] inner_id;
always @(posedge clk or posedge rst) begin
	if (rst) begin
		inner_id <= 8'b0;
	end
	else if ((inner_code_type == 8'h01)&(cnt_parse == 'd6)) begin
		inner_id <= ff_rx_data_ff[7:0];
	end
	else begin
		inner_id <= inner_id;
	end
end
//read maxuni
reg [15:0] maxuni;
always @(posedge clk or posedge rst) begin
	if (rst) begin
		maxuni <= 16'b0;
	end
	else if ((inner_code_type == 8'h01)&(cnt_parse == 'd7)) begin
		maxuni <= ff_rx_data_f[31:16];
	end
	else begin
		maxuni <= maxuni;
	end
end
//read magic
reg [31:0] magic;
always @(posedge clk or posedge rst) begin
	if (rst) begin
		magic <= 16'b0;
	end
	else if ((inner_code_type == 8'h01)&(cnt_parse == 'd8)) begin
		magic <= ff_rx_data_f;
	end
	else begin
		magic <= magic;
	end
end
//read frame_acquiry
/*always @(posedge clk or posedge rst) begin
	if (rst) begin
		frame_acquiry <= 272'b0;
		ff_acq_en <= 1'b0;
	end
	else if (ppp_protocol == 16'hc023) begin
		case(cnt_parse)
			'd6 : begin 
					frame_acquiry[271:256] <= ff_rx_data_f[15:0]; 
				 	ff_acq_en <= 1'b0;
				end
			'd7 : begin 
					frame_acquiry[255:224] <= ff_rx_data_f; 
				 	ff_acq_en <= 1'b0;
				end
			'd8 : begin 
					frame_acquiry[223:192] <= ff_rx_data_f; 
				 	ff_acq_en <= 1'b0;
				end
			'd9 : begin 
					frame_acquiry[191:160] <= ff_rx_data_f; 
				 	ff_acq_en <= 1'b0;
				end
			'd10: begin 
					frame_acquiry[159:128] <= ff_rx_data_f; 
				 	ff_acq_en <= 1'b0;
				end
			'd11: begin 
					frame_acquiry[127:96 ] <= ff_rx_data_f; 
				 	ff_acq_en <= 1'b0;
				end
			'd12: begin 
					frame_acquiry[95 :64 ] <= ff_rx_data_f; 
				 	ff_acq_en <= 1'b0;
				end
			'd13: begin 
					frame_acquiry[63 :32 ] <= ff_rx_data_f; 
				 	ff_acq_en <= 1'b0;
				end
			'd14: begin 
					frame_acquiry[31 :0  ] <= ff_rx_data_f; 
				 	ff_acq_en <= 1'b1;
				end
			default:  begin 
					frame_acquiry <= 272'b0;
					ff_acq_en <= 1'b0;
				end
		endcase
	end
	else begin
		frame_acquiry <= 272'b0;
		ff_acq_en <= 1'b0;
	end
end*/
//*************Discovery stage************//
// Master			AC 
//	PADI 	>> broadcast
//			<< PADO
//	PADR 	>>
//			<< PADS
//****************************************//
//generate ack frame//在fpga中尽量少用大位宽的寄存器，因此改成32bit连续发送,modified by ck
/*always @(posedge clk or posedge rst) begin
	if (rst) begin
		frame_pado <= 480'b0;
		frame_pads <= 480'b0;
	end
	else if (cnt_parse == 'd10) begin
		frame_pado <= {src_addr,`SERVER_ADDR,`FRAME_TYPE_DISC,`VERSION,`TYPE,`CODE_PADO,`SESSION_ID_PADO,`LENGTH_PADO,`TAG_PADO,host_uniq,`TAG_AC,`AC_NAME,24'b0};
		frame_pads <= {src_addr,`SERVER_ADDR,`FRAME_TYPE_DISC,`VERSION,`TYPE,`CODE_PADS,`SESSION_ID_AC 	,`LENGTH_PADS,`TAG_PADO,host_uniq,160'b0};
	end
	else begin
		frame_pado <= frame_pado;
		frame_pads <= frame_pads;
	end
end
//PADO frame send enable*/
always @(posedge clk or posedge rst) begin
	if (rst) begin
		frame_pado_en <= 1'b0;
	end
	else if ((code_type == `CODE_PADI)&(cnt_parse == 'd11)) begin
		frame_pado_en <= 1'b1;
	end
	else begin 
		frame_pado_en <= 1'b0;
	end
end
//PADS frame send enable
always @(posedge clk or posedge rst) begin
	if (rst) begin
		frame_pads_en <= 1'b0;
	end
	else if ((code_type == `CODE_PADR)&(cnt_parse == 'd10)) begin
		frame_pads_en <= 1'b1;
	end
	else begin
		frame_pads_en <= 1'b0;
	end
end
//**************Session stage*************//
// Master			AC
//			<< PADT cut off net access
//***************************************//
// Master			AC
// 			<< LCP req
// 	LCP ack >>
// 			<< PAP
//generate ack frame
// reg [479:0] frame_ack;//在fpga中尽量少用大位宽的寄存器，因此改成32bit连续发送,modified by ck
// always @(posedge clk or posedge rst) begin
// 	if (rst) begin
// 		frame_lcp <= 480'b0;
// 		frame_pap <= 480'b0;
// 		frame_padt <= 480'b0;
// 	end
// 	/*`PPP_FLAG,`PPP_ADDR,`PPP_CTRL,*/
// 	else if (cnt_parse == 'd3) begin
// 		frame_lcp  <= {src_addr,`SERVER_ADDR,`FRAME_TYPE_SESS,`VERSION,`TYPE,`CODE_SESSION,`SESSION_ID_AC,`LENGTH_LCP ,`PPP_LCP,8'h01,8'h01,16'h0013,/*req,id,len*/8'h01,8'h04,16'h05d4,/*type,len,maxuni*/8'h03,8'h04,`PPP_PAP,/*type,len,prtc*/8'h05,8'h06,32'h0a1b2c3d,/*type,len,magic num*/160'b0};
// 		frame_pap  <= {src_addr,`SERVER_ADDR,`FRAME_TYPE_SESS,`VERSION,`TYPE,`CODE_SESSION,`SESSION_ID_AC,`LENGTH_PAP ,`PPP_PAP,8'h01,8'h01,16'h0004,/*req,id,len*/272'b0};
// 		frame_padt <= {src_addr,`SERVER_ADDR,`FRAME_TYPE_DISC,`VERSION,`TYPE,`CODE_PADT   ,`SESSION_ID_AC,`LENGTH_PADT,320'b0};
// 	end
// 	else begin
// 		frame_lcp  <= frame_lcp;
// 		frame_pap  <= frame_pap;
// 		frame_padt <= frame_padt;
// 	end
// end
// always @(posedge clk or posedge rst) begin
// 	if (rst) begin
// 		frame_ack <= 480'b0;
// 	end
// 	else if ((inner_code_type == 8'h01)&(cnt_parse == 'd10)) begin
// 		frame_ack <= {src_addr,`SERVER_ADDR,`FRAME_TYPE_SESS,`VERSION,`TYPE,`CODE_SESSION,`SESSION_ID_AC,`LENGTH_LCP ,`PPP_LCP,8'h02,inner_id,16'h0015,/*req,id,len*/8'h01,8'h04,maxuni,/*type,len,maxuni*/8'h05,8'h06,magic,56'h070208020d0306,136'b0};
// 	end
// 	else begin
// 		frame_ack <= frame_ack;		
// 	end
// end
//PAP frame send enable
always @(posedge clk or posedge rst) begin
	if (rst) begin
		frame_pap_en <= 1'b0;
	end
	else if ((inner_code_type == 8'h02)&(cnt_parse == 'd7)) begin
		frame_pap_en <= 1'b1;
	end
	else begin 
		frame_pap_en <= 1'b0;
	end
end
//LCP ACK frame send enable
reg frame_ack_en;
always @(posedge clk or posedge rst) begin
	if (rst) begin
		frame_ack_en <= 1'b0;
	end
	else if ((inner_code_type == 8'h01)&(cnt_parse == 'd11)) begin
		frame_ack_en <= 1'b1;
	end
	else begin 
		frame_ack_en <= 1'b0;
	end
end
//PADT frame send enable
always @(posedge clk or posedge rst) begin
	if (rst) begin
		pppoe_padt_f <= 1'b0;
	end
	else begin
		pppoe_padt_f <= pppoe_padt;
	end
end
assign frame_padt_en = pppoe_padt&(~pppoe_padt_f);
//LCP frame send enable
always @(posedge clk or posedge rst) begin
	if (rst) begin
		pppoe_lcp_f <= 1'b0;
	end
	else begin
		pppoe_lcp_f <= pppoe_lcp;
	end
end
assign frame_lcp_en = pppoe_lcp&(~pppoe_lcp_f);

assign frame_en = {frame_ack_en,frame_pado_en,frame_pads_en,frame_lcp_en,frame_pap_en,frame_padt_en} ;

/*always @(posedge clk or posedge rst) begin//在fpga中尽量少用大位宽的寄存器，因此改成32bit连续发送,modified by ck
	if (rst) begin
		frame_genarate <= 480'b0;
		frame_valid <= 1'b0;
	end
	else begin
		case(frame_en)
			6'b000001: begin
				frame_genarate <= frame_padt ;
				frame_valid <= 1'b1;
			end
			6'b000010: begin
				frame_genarate <= frame_pap  ;
				frame_valid <= 1'b1;
			end 
			6'b000100: begin
				frame_genarate <= frame_lcp  ;
				frame_valid <= 1'b1;
			end 
			6'b001000: begin
				frame_genarate <= frame_pads ;
				frame_valid <= 1'b1;
			end 
			6'b010000: begin
				frame_genarate <= frame_pado ;
				frame_valid <= 1'b1;
			end
			6'b100000: begin
				frame_genarate <= frame_ack ;
				frame_valid <= 1'b1;
			end
			default:  begin
				frame_genarate <= 480'b0;
				frame_valid <= 1'b0;
			end 
		endcase
	end
end*/

reg [5:0] frame_en_reg; 
reg [3:0] send_cnt;

always @(posedge clk or posedge rst) begin
	if (rst) begin
		frame_en_reg <= 6'b0;
	end
	else if (frame_en != 6'b0) begin
		frame_en_reg <= frame_en;
	end
	else if(send_cnt == 4'd15) begin
		frame_en_reg <= 6'b0;
	end
	else begin
		frame_en_reg <= frame_en_reg;
	end
end

always @(posedge clk or posedge rst) begin
	if (rst) begin
		send_cnt <= 4'b0;
	end
	else if (frame_en_reg != 6'b0 && ff_tx_rdy == 1'b1) begin
		send_cnt <= send_cnt + 1'b1;
	end
	else begin
		send_cnt <= 4'b0;
	end
end

always @(posedge clk or posedge rst) begin//发送数据
	if (rst) begin
		ff_tx_data  <= 32'b0;
        ff_tx_mod   <= 2'b0;
        ff_tx_dval  <= 1'b0;
        ff_tx_sop   <= 1'b0;
        ff_tx_eop   <= 1'b0;
	end  
	else if ( frame_en_reg[0] == 1'b1 && ff_tx_rdy == 1'b1 ) begin//发送padt帧frame_padt <= {src_addr,`SERVER_ADDR,`FRAME_TYPE_DISC,`VERSION,`TYPE,`CODE_PADT   ,`SESSION_ID_AC,`LENGTH_PADT,320'b0};
		case( send_cnt )
			4'b0000:
				begin
					ff_tx_data  <= src_addr[47:16];
        			ff_tx_mod   <= 2'b0;
        			ff_tx_dval  <= 1'b1;
        			ff_tx_sop   <= 1'b1;
        			ff_tx_eop   <= 1'b0;
				end
			4'b0001:
				begin
					ff_tx_data  <= {src_addr[15:0],16'h0016};
        			ff_tx_mod   <= 2'b0;
        			ff_tx_dval  <= 1'b1;
        			ff_tx_sop   <= 1'b0;
        			ff_tx_eop   <= 1'b0;
				end
			4'b0010:
				begin
					ff_tx_data  <= 32'hfcc7a617;
        			ff_tx_mod   <= 2'b0;
        			ff_tx_dval  <= 1'b1;
        			ff_tx_sop   <= 1'b0;
        			ff_tx_eop   <= 1'b0;
				end
			4'b0011:
				begin
					ff_tx_data  <= {`FRAME_TYPE_DISC,`VERSION,`TYPE,`CODE_PADT};
        			ff_tx_mod   <= 2'b0;
        			ff_tx_dval  <= 1'b1;
        			ff_tx_sop   <= 1'b0;
        			ff_tx_eop   <= 1'b0;
				end
			4'b0100:
				begin
					ff_tx_data  <= {`SESSION_ID_AC,`LENGTH_PADT};
        			ff_tx_mod   <= 2'b0;
        			ff_tx_dval  <= 1'b1;
        			ff_tx_sop   <= 1'b0;
        			ff_tx_eop   <= 1'b0;
				end
			4'b0101,4'b0110,4'b0111,4'b1000,4'b1001,4'b1010,4'b1011,4'b1100,4'b1101:
				begin
					ff_tx_data  <= 32'b0;
        			ff_tx_mod   <= 2'b0;
        			ff_tx_dval  <= 1'b1;
        			ff_tx_sop   <= 1'b0;
        			ff_tx_eop   <= 1'b0;
				end
			4'b1110:
				begin
					ff_tx_data  <= 32'b0;
        			ff_tx_mod   <= 2'b0;
        			ff_tx_dval  <= 1'b1;
        			ff_tx_sop   <= 1'b0;
        			ff_tx_eop   <= 1'b1;
				end
			default:
				begin
					ff_tx_data  <= 32'b0;
        			ff_tx_mod   <= 2'b0;
        			ff_tx_dval  <= 1'b0;
        			ff_tx_sop   <= 1'b0;
        			ff_tx_eop   <= 1'b0;
				end
		endcase
	end
	else if( frame_en_reg[1] == 1'b1 && ff_tx_rdy == 1'b1 )  begin//发送pap帧，frame_pap  <= {src_addr,`SERVER_ADDR,`FRAME_TYPE_SESS,`VERSION,`TYPE,`CODE_SESSION,`SESSION_ID_AC,`LENGTH_PAP ,`PPP_PAP,8'h01,8'h01,16'h0004,/*req,id,len*/272'b0};
		case( send_cnt )
			4'b0000:
				begin
					ff_tx_data  <= src_addr[47:16];
        			ff_tx_mod   <= 2'b0;
        			ff_tx_dval  <= 1'b1;
        			ff_tx_sop   <= 1'b1;
        			ff_tx_eop   <= 1'b0;
				end
			4'b0001:
				begin
					ff_tx_data  <= {src_addr[15:0],16'h0016};
        			ff_tx_mod   <= 2'b0;
        			ff_tx_dval  <= 1'b1;
        			ff_tx_sop   <= 1'b0;
        			ff_tx_eop   <= 1'b0;
				end
			4'b0010:
				begin
					ff_tx_data  <= 32'hfcc7a617;
        			ff_tx_mod   <= 2'b0;
        			ff_tx_dval  <= 1'b1;
        			ff_tx_sop   <= 1'b0;
        			ff_tx_eop   <= 1'b0;
				end
			4'b0011:
				begin
					ff_tx_data  <= {`FRAME_TYPE_SESS,`VERSION,`TYPE,`CODE_SESSION};
        			ff_tx_mod   <= 2'b0;
        			ff_tx_dval  <= 1'b1;
        			ff_tx_sop   <= 1'b0;
        			ff_tx_eop   <= 1'b0;
				end
			4'b0100:
				begin
					ff_tx_data  <= {`SESSION_ID_AC,`LENGTH_PAP};
        			ff_tx_mod   <= 2'b0;
        			ff_tx_dval  <= 1'b1;
        			ff_tx_sop   <= 1'b0;
        			ff_tx_eop   <= 1'b0;
				end
			4'b0101:
				begin
					ff_tx_data  <= {`PPP_PAP,8'h01,8'h01};
        			ff_tx_mod   <= 2'b0;
        			ff_tx_dval  <= 1'b1;
        			ff_tx_sop   <= 1'b0;
        			ff_tx_eop   <= 1'b0;
				end
			4'b0110:
				begin
					ff_tx_data  <= {16'h0004,16'h0000};
        			ff_tx_mod   <= 2'b0;
        			ff_tx_dval  <= 1'b1;
        			ff_tx_sop   <= 1'b0;
        			ff_tx_eop   <= 1'b0;
				end
			4'b0111,4'b1000,4'b1001,4'b1010,4'b1011,4'b1100,4'b1101:
				begin
					ff_tx_data  <= 32'b0;
        			ff_tx_mod   <= 2'b0;
        			ff_tx_dval  <= 1'b1;
        			ff_tx_sop   <= 1'b0;
        			ff_tx_eop   <= 1'b0;
				end
			4'b1110:
				begin
					ff_tx_data  <= 32'b0;
        			ff_tx_mod   <= 2'b0;
        			ff_tx_dval  <= 1'b1;
        			ff_tx_sop   <= 1'b0;
        			ff_tx_eop   <= 1'b1;
				end
			default:
				begin
					ff_tx_data  <= 32'b0;
        			ff_tx_mod   <= 2'b0;
        			ff_tx_dval  <= 1'b0;
        			ff_tx_sop   <= 1'b0;
        			ff_tx_eop   <= 1'b0;
				end
		endcase
	end
	else if( frame_en_reg[2] == 1'b1 && ff_tx_rdy == 1'b1 )  begin//发送lcp帧，frame_lcp  <= {src_addr,`SERVER_ADDR,`FRAME_TYPE_SESS,`VERSION,`TYPE,`CODE_SESSION,`SESSION_ID_AC,`LENGTH_LCP ,`PPP_LCP,8'h01,8'h01,16'h0013,/*req,id,len*/8'h01,8'h04,16'h05d4,/*type,len,maxuni*/8'h03,8'h04,`PPP_PAP,/*type,len,prtc*/8'h05,8'h06,32'h0a1b2c3d,/*type,len,magic num*/160'b0};
	case( send_cnt )
			4'b0000:
				begin
					ff_tx_data  <= src_addr[47:16];
        			ff_tx_mod   <= 2'b0;
        			ff_tx_dval  <= 1'b1;
        			ff_tx_sop   <= 1'b1;
        			ff_tx_eop   <= 1'b0;
				end
			4'b0001:
				begin
					ff_tx_data  <= {src_addr[15:0],16'h0016};
        			ff_tx_mod   <= 2'b0;
        			ff_tx_dval  <= 1'b1;
        			ff_tx_sop   <= 1'b0;
        			ff_tx_eop   <= 1'b0;
				end
			4'b0010:
				begin
					ff_tx_data  <= 32'hfcc7a617;
        			ff_tx_mod   <= 2'b0;
        			ff_tx_dval  <= 1'b1;
        			ff_tx_sop   <= 1'b0;
        			ff_tx_eop   <= 1'b0;
				end
			4'b0011:
				begin
					ff_tx_data  <= {`FRAME_TYPE_SESS,`VERSION,`TYPE,`CODE_SESSION};
        			ff_tx_mod   <= 2'b0;
        			ff_tx_dval  <= 1'b1;
        			ff_tx_sop   <= 1'b0;
        			ff_tx_eop   <= 1'b0;
				end
			4'b0100:
				begin
					ff_tx_data  <= {`SESSION_ID_AC,`LENGTH_LCP};
        			ff_tx_mod   <= 2'b0;
        			ff_tx_dval  <= 1'b1;
        			ff_tx_sop   <= 1'b0;
        			ff_tx_eop   <= 1'b0;
				end
			4'b0101:
				begin
					ff_tx_data  <= {`PPP_LCP,8'h01,8'h01};
        			ff_tx_mod   <= 2'b0;
        			ff_tx_dval  <= 1'b1;
        			ff_tx_sop   <= 1'b0;
        			ff_tx_eop   <= 1'b0;
				end
			4'b0110:
				begin
					ff_tx_data  <= {16'h0013,/*req,id,len*/8'h01,8'h04};
        			ff_tx_mod   <= 2'b0;
        			ff_tx_dval  <= 1'b1;
        			ff_tx_sop   <= 1'b0;
        			ff_tx_eop   <= 1'b0;
				end
			4'b0111:
				begin
					ff_tx_data  <= {16'h05d4,/*type,len,maxuni*/8'h03,8'h04};
        			ff_tx_mod   <= 2'b0;
        			ff_tx_dval  <= 1'b1;
        			ff_tx_sop   <= 1'b0;
        			ff_tx_eop   <= 1'b0;
				end
			4'b1000:
				begin
					ff_tx_data  <= {`PPP_PAP,/*type,len,prtc*/8'h05,8'h06};
        			ff_tx_mod   <= 2'b0;
        			ff_tx_dval  <= 1'b1;
        			ff_tx_sop   <= 1'b0;
        			ff_tx_eop   <= 1'b0;
				end
			4'b1001:
				begin
					ff_tx_data  <= 32'h0a1b2c3d;
        			ff_tx_mod   <= 2'b0;
        			ff_tx_dval  <= 1'b1;
        			ff_tx_sop   <= 1'b0;
        			ff_tx_eop   <= 1'b0;
				end
			4'b1010,4'b1011,4'b1100,4'b1101:
				begin
					ff_tx_data  <= 32'b0;
        			ff_tx_mod   <= 2'b0;
        			ff_tx_dval  <= 1'b1;
        			ff_tx_sop   <= 1'b0;
        			ff_tx_eop   <= 1'b0;
				end
			4'b1110:
				begin
					ff_tx_data  <= 32'b0;
        			ff_tx_mod   <= 2'b0;
        			ff_tx_dval  <= 1'b1;
        			ff_tx_sop   <= 1'b0;
        			ff_tx_eop   <= 1'b1;
				end
			default:
				begin
					ff_tx_data  <= 32'b0;
        			ff_tx_mod   <= 2'b0;
        			ff_tx_dval  <= 1'b0;
        			ff_tx_sop   <= 1'b0;
        			ff_tx_eop   <= 1'b0;
				end
		endcase
	end
	else if( frame_en_reg[3] == 1'b1 && ff_tx_rdy == 1'b1 )  begin//发送pads帧，frame_pads <= {src_addr,`SERVER_ADDR,`FRAME_TYPE_DISC,`VERSION,`TYPE,`CODE_PADS,`SESSION_ID_AC 	,`LENGTH_PADS,`TAG_PADO,host_uniq,160'b0};
	case( send_cnt )
			4'b0000:
				begin
					ff_tx_data  <= src_addr[47:16];
        			ff_tx_mod   <= 2'b0;
        			ff_tx_dval  <= 1'b1;
        			ff_tx_sop   <= 1'b1;
        			ff_tx_eop   <= 1'b0;
				end
			4'b0001:
				begin
					ff_tx_data  <= {src_addr[15:0],16'h0016};
        			ff_tx_mod   <= 2'b0;
        			ff_tx_dval  <= 1'b1;
        			ff_tx_sop   <= 1'b0;
        			ff_tx_eop   <= 1'b0;
				end
			4'b0010:
				begin
					ff_tx_data  <= 32'hfcc7a617;
        			ff_tx_mod   <= 2'b0;
        			ff_tx_dval  <= 1'b1;
        			ff_tx_sop   <= 1'b0;
        			ff_tx_eop   <= 1'b0;
				end
			4'b0011:
				begin
					ff_tx_data  <= {`FRAME_TYPE_DISC,`VERSION,`TYPE,`CODE_PADS};
        			ff_tx_mod   <= 2'b0;
        			ff_tx_dval  <= 1'b1;
        			ff_tx_sop   <= 1'b0;
        			ff_tx_eop   <= 1'b0;
				end
			4'b0100:
				begin
					ff_tx_data  <= {`SESSION_ID_AC,`LENGTH_PADS};
        			ff_tx_mod   <= 2'b0;
        			ff_tx_dval  <= 1'b1;
        			ff_tx_sop   <= 1'b0;
        			ff_tx_eop   <= 1'b0;
				end
			4'b0101:
				begin
					ff_tx_data  <= 32'h0101_0000;
        			ff_tx_mod   <= 2'b0;
        			ff_tx_dval  <= 1'b1;
        			ff_tx_sop   <= 1'b0;
        			ff_tx_eop   <= 1'b0;
				end
			4'b0110:
				begin
					ff_tx_data  <= 32'h0103_000c;
        			ff_tx_mod   <= 2'b0;
        			ff_tx_dval  <= 1'b1;
        			ff_tx_sop   <= 1'b0;
        			ff_tx_eop   <= 1'b0;
				end
			4'b0111:
				begin
					ff_tx_data  <= host_uniq[95:64];
        			ff_tx_mod   <= 2'b0;
        			ff_tx_dval  <= 1'b1;
        			ff_tx_sop   <= 1'b0;
        			ff_tx_eop   <= 1'b0;
				end
			4'b1000:
				begin
					ff_tx_data  <= host_uniq[63:32];
        			ff_tx_mod   <= 2'b0;
        			ff_tx_dval  <= 1'b1;
        			ff_tx_sop   <= 1'b0;
        			ff_tx_eop   <= 1'b0;
				end
			4'b1001:
				begin
					ff_tx_data  <= host_uniq[31:0];
        			ff_tx_mod   <= 2'b0;
        			ff_tx_dval  <= 1'b1;
        			ff_tx_sop   <= 1'b0;
        			ff_tx_eop   <= 1'b0;
				end
			4'b1010,4'b1011,4'b1100,4'b1101:
				begin
					ff_tx_data  <= 32'b0;
        			ff_tx_mod   <= 2'b0;
        			ff_tx_dval  <= 1'b1;
        			ff_tx_sop   <= 1'b0;
        			ff_tx_eop   <= 1'b0;
				end
			4'b1110:
				begin
					ff_tx_data  <= 32'b0;
        			ff_tx_mod   <= 2'b0;
        			ff_tx_dval  <= 1'b1;
        			ff_tx_sop   <= 1'b0;
        			ff_tx_eop   <= 1'b1;
				end
			default:
				begin
					ff_tx_data  <= 32'b0;
        			ff_tx_mod   <= 2'b0;
        			ff_tx_dval  <= 1'b0;
        			ff_tx_sop   <= 1'b0;
        			ff_tx_eop   <= 1'b0;
				end
		endcase
	end
	else if( frame_en_reg[4] == 1'b1 && ff_tx_rdy == 1'b1 )  begin//发送pado帧，frame_pado <= {src_addr,`SERVER_ADDR,`FRAME_TYPE_DISC,`VERSION,`TYPE,`CODE_PADO,`SESSION_ID_PADO,`LENGTH_PADO,`TAG_PADO,host_uniq,`TAG_AC,`AC_NAME,24'b0};
	case( send_cnt )
			4'b0000:
				begin
					ff_tx_data  <= src_addr[47:16];
        			ff_tx_mod   <= 2'b0;
        			ff_tx_dval  <= 1'b1;
        			ff_tx_sop   <= 1'b1;
        			ff_tx_eop   <= 1'b0;
				end
			4'b0001:
				begin
					ff_tx_data  <= {src_addr[15:0],16'h0016};
        			ff_tx_mod   <= 2'b0;
        			ff_tx_dval  <= 1'b1;
        			ff_tx_sop   <= 1'b0;
        			ff_tx_eop   <= 1'b0;
				end
			4'b0010:
				begin
					ff_tx_data  <= 32'hfcc7a617;
        			ff_tx_mod   <= 2'b0;
        			ff_tx_dval  <= 1'b1;
        			ff_tx_sop   <= 1'b0;
        			ff_tx_eop   <= 1'b0;
				end
			4'b0011:
				begin
					ff_tx_data  <= {`FRAME_TYPE_DISC,`VERSION,`TYPE,`CODE_PADO};
        			ff_tx_mod   <= 2'b0;
        			ff_tx_dval  <= 1'b1;
        			ff_tx_sop   <= 1'b0;
        			ff_tx_eop   <= 1'b0;
				end
			4'b0100:
				begin
					ff_tx_data  <= {`SESSION_ID_PADO,`LENGTH_PADO};
        			ff_tx_mod   <= 2'b0;
        			ff_tx_dval  <= 1'b1;
        			ff_tx_sop   <= 1'b0;
        			ff_tx_eop   <= 1'b0;
				end
			4'b0101:
				begin
					ff_tx_data  <= 32'h0101_0000;
        			ff_tx_mod   <= 2'b0;
        			ff_tx_dval  <= 1'b1;
        			ff_tx_sop   <= 1'b0;
        			ff_tx_eop   <= 1'b0;
				end
			4'b0110:
				begin
					ff_tx_data  <= 32'h0103_000c;
        			ff_tx_mod   <= 2'b0;
        			ff_tx_dval  <= 1'b1;
        			ff_tx_sop   <= 1'b0;
        			ff_tx_eop   <= 1'b0;
				end
			4'b0111:
				begin
					ff_tx_data  <= host_uniq[95:64];
        			ff_tx_mod   <= 2'b0;
        			ff_tx_dval  <= 1'b1;
        			ff_tx_sop   <= 1'b0;
        			ff_tx_eop   <= 1'b0;
				end
			4'b1000:
				begin
					ff_tx_data  <= host_uniq[63:32];
        			ff_tx_mod   <= 2'b0;
        			ff_tx_dval  <= 1'b1;
        			ff_tx_sop   <= 1'b0;
        			ff_tx_eop   <= 1'b0;
				end
			4'b1001:
				begin
					ff_tx_data  <= host_uniq[31:0];
        			ff_tx_mod   <= 2'b0;
        			ff_tx_dval  <= 1'b1;
        			ff_tx_sop   <= 1'b0;
        			ff_tx_eop   <= 1'b0;
				end
			4'b1010:
				begin
					ff_tx_data  <= `TAG_AC;
        			ff_tx_mod   <= 2'b0;
        			ff_tx_dval  <= 1'b1;
        			ff_tx_sop   <= 1'b0;
        			ff_tx_eop   <= 1'b0;
				end
			4'b1011:
				begin
					ff_tx_data  <= 32'h5844_4258;
        			ff_tx_mod   <= 2'b0;
        			ff_tx_dval  <= 1'b1;
        			ff_tx_sop   <= 1'b0;
        			ff_tx_eop   <= 1'b0;
				end
			4'b1100:
				begin
					ff_tx_data  <= 32'h515f_4d45;
        			ff_tx_mod   <= 2'b0;
        			ff_tx_dval  <= 1'b1;
        			ff_tx_sop   <= 1'b0;
        			ff_tx_eop   <= 1'b0;
				end
			4'b1101:
				begin
					ff_tx_data  <= 32'h3630_2d58;
        			ff_tx_mod   <= 2'b0;
        			ff_tx_dval  <= 1'b1;
        			ff_tx_sop   <= 1'b0;
        			ff_tx_eop   <= 1'b0;
				end
			4'b1110:
				begin
					ff_tx_data  <= {8'h38,24'b0};
        			ff_tx_mod   <= 2'b0;
        			ff_tx_dval  <= 1'b1;
        			ff_tx_sop   <= 1'b0;
        			ff_tx_eop   <= 1'b1;
				end
			default:
				begin
					ff_tx_data  <= 32'b0;
        			ff_tx_mod   <= 2'b0;
        			ff_tx_dval  <= 1'b0;
        			ff_tx_sop   <= 1'b0;
        			ff_tx_eop   <= 1'b0;
				end
		endcase
	end
	else if( frame_en_reg[5] == 1'b1 && ff_tx_rdy == 1'b1 )  begin//发送ack帧，frame_ack <= {src_addr,`SERVER_ADDR,`FRAME_TYPE_SESS,`VERSION,`TYPE,`CODE_SESSION,`SESSION_ID_AC,`LENGTH_LCP ,`PPP_LCP,8'h02,inner_id,16'h0015,/*req,id,len*/8'h01,8'h04,maxuni,/*type,len,maxuni*/8'h05,8'h06,magic,56'h070208020d0306,136'b0};
	case( send_cnt )
			4'b0000:
				begin
					ff_tx_data  <= src_addr[47:16];
        			ff_tx_mod   <= 2'b0;
        			ff_tx_dval  <= 1'b1;
        			ff_tx_sop   <= 1'b1;
        			ff_tx_eop   <= 1'b0;
				end
			4'b0001:
				begin
					ff_tx_data  <= {src_addr[15:0],16'h0016};
        			ff_tx_mod   <= 2'b0;
        			ff_tx_dval  <= 1'b1;
        			ff_tx_sop   <= 1'b0;
        			ff_tx_eop   <= 1'b0;
				end
			4'b0010:
				begin
					ff_tx_data  <= 32'hfcc7a617;
        			ff_tx_mod   <= 2'b0;
        			ff_tx_dval  <= 1'b1;
        			ff_tx_sop   <= 1'b0;
        			ff_tx_eop   <= 1'b0;
				end
			4'b0011:
				begin
					ff_tx_data  <= {`FRAME_TYPE_SESS,`VERSION,`TYPE,`CODE_SESSION};
        			ff_tx_mod   <= 2'b0;
        			ff_tx_dval  <= 1'b1;
        			ff_tx_sop   <= 1'b0;
        			ff_tx_eop   <= 1'b0;
				end
			4'b0100:
				begin
					ff_tx_data  <= {`SESSION_ID_AC,`LENGTH_LCP};
        			ff_tx_mod   <= 2'b0;
        			ff_tx_dval  <= 1'b1;
        			ff_tx_sop   <= 1'b0;
        			ff_tx_eop   <= 1'b0;
				end
			4'b0101:
				begin
					ff_tx_data  <= {`PPP_LCP,8'h02,inner_id};
        			ff_tx_mod   <= 2'b0;
        			ff_tx_dval  <= 1'b1;
        			ff_tx_sop   <= 1'b0;
        			ff_tx_eop   <= 1'b0;
				end
			4'b0110:
				begin
					ff_tx_data  <= {16'h0015,/*req,id,len*/8'h01,8'h04};
        			ff_tx_mod   <= 2'b0;
        			ff_tx_dval  <= 1'b1;
        			ff_tx_sop   <= 1'b0;
        			ff_tx_eop   <= 1'b0;
				end
			4'b0111:
				begin
					ff_tx_data  <= {maxuni,/*type,len,maxuni*/8'h05,8'h06};
        			ff_tx_mod   <= 2'b0;
        			ff_tx_dval  <= 1'b1;
        			ff_tx_sop   <= 1'b0;
        			ff_tx_eop   <= 1'b0;
				end
			4'b1000:
				begin
					ff_tx_data  <= magic;
        			ff_tx_mod   <= 2'b0;
        			ff_tx_dval  <= 1'b1;
        			ff_tx_sop   <= 1'b0;
        			ff_tx_eop   <= 1'b0;
				end
			4'b1001:
				begin
					ff_tx_data  <= 32'h0702_0802;
        			ff_tx_mod   <= 2'b0;
        			ff_tx_dval  <= 1'b1;
        			ff_tx_sop   <= 1'b0;
        			ff_tx_eop   <= 1'b0;
				end
			4'b1010:
				begin
					ff_tx_data  <= {24'h0d0306,8'h00};
        			ff_tx_mod   <= 2'b0;
        			ff_tx_dval  <= 1'b1;
        			ff_tx_sop   <= 1'b0;
        			ff_tx_eop   <= 1'b0;
				end
			4'b1011,4'b1100,4'b1101:
				begin
					ff_tx_data  <= 32'b0;
        			ff_tx_mod   <= 2'b0;
        			ff_tx_dval  <= 1'b1;
        			ff_tx_sop   <= 1'b0;
        			ff_tx_eop   <= 1'b0;
				end
			4'b1110:
				begin
					ff_tx_data  <= 32'b0;
        			ff_tx_mod   <= 2'b0;
        			ff_tx_dval  <= 1'b1;
        			ff_tx_sop   <= 1'b0;
        			ff_tx_eop   <= 1'b1;
				end
			default:
				begin
					ff_tx_data  <= 32'b0;
        			ff_tx_mod   <= 2'b0;
        			ff_tx_dval  <= 1'b0;
        			ff_tx_sop   <= 1'b0;
        			ff_tx_eop   <= 1'b0;
				end
		endcase
	end
end







//******************************************//
//read frame_acquiry
reg [7:0] id_len;
reg [7:0] pw_len;
reg [5:0] cnt_id;
//id cnt
wire [5:0] id_cnt;
wire [1:0] id_mod;
assign id_cnt = (id_len[1:0] == 2'b0)?id_len[7:2]:id_len[7:2] + 6'b1;
assign id_mod = id_len[1:0];
//wire [7:0] len;
// assign len = (pw_len == 8'b0)?8'b100:id_len + pw_len + 20;//(2:20_20+2:id_len,pw_len+16:d,saddr,type)
always @(posedge clk or posedge rst) begin
	if (rst) begin
		len <= 8'b100;
	end
	else if (pw_len != 8'b0) begin
		len <= id_len + pw_len + 20;
	end
	else begin
		len <= len;
	end
end
always @(posedge clk or posedge rst) begin
	if (rst) begin
		frame_acquiry 	<= 32'b0;
		ff_acq_en 		<= 1'b0;
		id_len 			<= 8'b0;
		pw_len 			<= 8'b0;
		cnt_id 			<= 6'b0;
	end
	else if (ppp_protocol == 16'hc023 && switch == 1'b1 ) begin
		if (cnt_parse == 'd6) begin
			frame_acquiry <= {24'h2020_25,ff_rx_data_f[7:0]}; 
			ff_acq_en <= 1'b1;
			id_len <= ff_rx_data_f[15:8];
			pw_len <= 8'b0;
			cnt_id <= cnt_id + 6'b1;			
		end
		else if (cnt_parse <= 'd14) begin
			if ((id_cnt == cnt_id)&(cnt_id != 6'b0)&(id_cnt != 6'b0)) begin
				 case(id_mod)
				 	2'b01: begin
				 		frame_acquiry <= {8'h25,ff_rx_data_f[23:0]}; 
						ff_acq_en <= 1'b0;
						id_len <= id_len;
						pw_len <= ff_rx_data_f[31:24];
						cnt_id <= 6'b0;	
				 	end
				 	2'b10: begin
				 		frame_acquiry <= {ff_rx_data_f[31:24],8'h25,ff_rx_data_f[15:0]}; 
						ff_acq_en <= 1'b0;
						id_len <= id_len;
						pw_len <= ff_rx_data_f[23:16];
						cnt_id <= 6'b0;				 			
				 	end
				 	2'b11: begin
				 		frame_acquiry <= {ff_rx_data_f[31:16],8'h25,ff_rx_data_f[7:0]}; 
						ff_acq_en <= 1'b0;
						id_len <= id_len;
						pw_len <= ff_rx_data_f[15:8];
						cnt_id <= 6'b0;	 			
				 	end
				 	default:begin
				 		frame_acquiry <= {ff_rx_data_f[31:8],8'h25}; 
						ff_acq_en <= 1'b0;
						id_len <= id_len;
						pw_len <= ff_rx_data_f[7:0];
						cnt_id <= 6'b0;			 			
				 	end
				 endcase
			end 
			else if ((cnt_id < id_cnt)&(cnt_id != 6'b0)) begin
			 	frame_acquiry <= ff_rx_data_f[31:0]; 
				ff_acq_en <= 1'b0;
				id_len <= id_len;
				pw_len <= pw_len;
				cnt_id <= cnt_id + 6'b1;				  		
			  end
			 else begin
			 	frame_acquiry <= ff_rx_data_f[31:0]; 
				ff_acq_en <= 1'b0;
				id_len <= id_len;
				pw_len <= pw_len;
				cnt_id <= 6'b0;			 					
			 end				
		end
		else begin
			frame_acquiry 	<= 32'b0;
			ff_acq_en 		<= 1'b0;
			id_len 			<= 8'b0;
			pw_len 			<= 8'b0;
			cnt_id 			<= 6'b0;	
		end
	end
end


endmodule
