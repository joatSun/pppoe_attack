// **************************************************************        
// COPYRIGHT(c)2021, Xidian University
// All rights reserved.
//
// IP LIB INDEX  : 
// IP Name       : 
//                
// File name     :  async_reset.v
// Module name   :  ASYNC_RESET
// Full name     : 
//
// Author        :  ck
// Email         :  1255279151@qq.com
// Data          :   
// Version       :  V 1.0  
// 
// Abstract      :	异步复位，同步释放 
// Called by     :  Father Module
// 
// This File is  Created on 2021/1/22
// Modification history
// ---------------------------------------------------------------------------------
`timescale 1ns/1ps

module ASYNC_RESET(
	input 				clk 			        ,
	input 				reset_via_bufg 			,
    output 	    		rst
);


reg reset_via_bufg_ff1;
reg reset_via_bufg_ff2;

always @(posedge clk or posedge reset_via_bufg) begin
	if (reset_via_bufg) begin
		reset_via_bufg_ff1 <= 1'b1;
		reset_via_bufg_ff2 <= 1'b1;
	end
	else begin
		reset_via_bufg_ff1 <= 1'b0;
		reset_via_bufg_ff2 <= reset_via_bufg_ff1;
	end
end


assign rst = reset_via_bufg_ff2 ;



endmodule
