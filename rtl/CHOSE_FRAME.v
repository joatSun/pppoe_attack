// **************************************************************        
// COPYRIGHT(c)2021, Xidian University
// All rights reserved.
//
// IP LIB INDEX  : 
// IP Name       : 
//                
// File name     :  
// Module name   :  
// Full name     : 
//
// Author        :  
// Email         :  
// Data          :   
// Version       :  V 1.0 
// 
// Abstract      :	
// Called by     :  
// 
// This File is  Created on 2021/1/22此模块用于模仿阻止服务器向用户侧发送pppoe帧的回应，只让用户侧接收我们产生的pppoe回应帧。
// Modification history
// ---------------------------------------------------------------------------------
`timescale 1ns/1ps

module CHOSE_FRAME(
//FPGA_GEN
input clk  ,         
input rst  , 
input frame_en,  
input ff_tx_rdy,//connect to mac         
//output ff_tx_rdy_0 , //connect to FPGA   
input[31:0]    ff_tx_data_0 ,   
input[1:0]     ff_tx_mod_0  ,   
input          ff_tx_dval_0 ,   
input          ff_tx_sop_0  ,   
input          ff_tx_eop_0  , 
//MAC_GEN               
input[31:0]    ff_tx_data_1 ,   
input[1:0]     ff_tx_mod_1  ,   
input          ff_tx_dval_1 ,   
input          ff_tx_sop_1  ,   
input          ff_tx_eop_1  ,
//chose to output 
output reg ff_tx_rdy_0 ,
output reg ff_tx_rdy_1,
output reg [31:0] ff_tx_data ,
output reg [1:0] ff_tx_mod  ,
output reg ff_tx_dval ,
output reg ff_tx_sop  ,
output reg ff_tx_eop 
);
reg     [  31:   0]   ff_tx_data_1_f           ; 
reg     [   1:   0]   ff_tx_mod_1_f            ; 
reg                   ff_tx_dval_1_f           ; 
reg                   ff_tx_sop_1_f            ; 
reg                   ff_tx_eop_1_f            ; 
reg     [  31:   0]   ff_tx_data_1_ff          ; 
reg     [   1:   0]   ff_tx_mod_1_ff           ; 
reg                   ff_tx_dval_1_ff          ; 
reg                   ff_tx_sop_1_ff           ; 
reg                   ff_tx_eop_1_ff           ; 
reg     [  31:   0]   ff_tx_data_1_fff         ; 
reg     [   1:   0]   ff_tx_mod_1_fff          ; 
reg                   ff_tx_dval_1_fff         ; 
reg                   ff_tx_sop_1_fff          ; 
reg                   ff_tx_eop_1_fff          ; 
reg     [  31:   0]   ff_tx_data_1_f4          ; 
reg     [   1:   0]   ff_tx_mod_1_f4           ; 
reg                   ff_tx_dval_1_f4          ; 
reg                   ff_tx_sop_1_f4           ; 
reg                   ff_tx_eop_1_f4           ; 
reg     [  31:   0]   ff_tx_data_1_ff5         ; 
reg     [   1:   0]   ff_tx_mod_1_ff5          ; 
reg                   ff_tx_dval_1_ff5         ; 
reg                   ff_tx_sop_1_ff5          ; 
reg                   ff_tx_eop_1_ff5          ; 
reg     [  31:   0]   ff_tx_data_1_fff6        ; 
reg     [   1:   0]   ff_tx_mod_1_fff6         ; 
reg                   ff_tx_dval_1_fff6        ; 
reg                   ff_tx_sop_1_fff6         ; 
reg                   ff_tx_eop_1_fff6         ; 
reg                   frame_en_f               ; 
//检测到PPPOE帧就阻断发送；
reg                   obstruct_flag            ; 
//reg                   obstruct_en              ; 
reg     [   4:   0]   cnt                      ; 
reg     [  15:   0]   pppoe_f                  ; 
always @(posedge clk or posedge rst) begin
    if (rst==1'b1) begin
        ff_tx_data_1_f   <=  32'b0;  
        ff_tx_mod_1_f    <=   2'b0; 
        ff_tx_dval_1_f   <=   1'b0; 
        ff_tx_sop_1_f    <=   1'b0; 
        ff_tx_eop_1_f    <=   1'b0; 
        ff_tx_data_1_ff  <=  32'b0;  
        ff_tx_mod_1_ff   <=   2'b0;   
        ff_tx_dval_1_ff  <=   1'b0;  
        ff_tx_sop_1_ff   <=   1'b0;   
        ff_tx_eop_1_ff   <=   1'b0;  
        ff_tx_data_1_fff <=  32'b0;  
        ff_tx_mod_1_fff  <=   2'b0; 
        ff_tx_dval_1_fff <=   1'b0; 
        ff_tx_sop_1_fff  <=   1'b0;  
        ff_tx_eop_1_fff  <=   1'b0;
        ff_tx_data_1_f4   <=  32'b0;
		ff_tx_mod_1_f4    <=   2'b0;
		ff_tx_dval_1_f4   <=   1'b0;
		ff_tx_sop_1_f4    <=   1'b0;
		ff_tx_eop_1_f4    <=   1'b0;
		ff_tx_data_1_ff5  <=  32'b0;
		ff_tx_mod_1_ff5   <=   2'b0;
		ff_tx_dval_1_ff5  <=   1'b0;
		ff_tx_sop_1_ff5   <=   1'b0;
		ff_tx_eop_1_ff5   <=   1'b0;
		ff_tx_data_1_fff6 <=  32'b0;
		ff_tx_mod_1_fff6  <=   2'b0;
		ff_tx_dval_1_fff6 <=   1'b0;
		ff_tx_sop_1_fff6  <=   1'b0;
		ff_tx_eop_1_fff6  <=   1'b0;  
    end
    else  begin
        ff_tx_data_1_f   <= ff_tx_data_1   ;
		ff_tx_mod_1_f    <= ff_tx_mod_1    ;  
		ff_tx_dval_1_f   <= ff_tx_dval_1   ;  
		ff_tx_sop_1_f    <= ff_tx_sop_1    ;  
		ff_tx_eop_1_f    <= ff_tx_eop_1    ;  
		ff_tx_data_1_ff  <= ff_tx_data_1_f ;
		ff_tx_mod_1_ff   <= ff_tx_mod_1_f  ;
		ff_tx_dval_1_ff  <= ff_tx_dval_1_f ;
		ff_tx_sop_1_ff   <= ff_tx_sop_1_f  ;
		ff_tx_eop_1_ff   <= ff_tx_eop_1_f  ;
		ff_tx_data_1_fff <= ff_tx_data_1_ff;
		ff_tx_mod_1_fff  <= ff_tx_mod_1_ff ;
		ff_tx_dval_1_fff <= ff_tx_dval_1_ff;
		ff_tx_sop_1_fff  <= ff_tx_sop_1_ff ;
		ff_tx_eop_1_fff  <= ff_tx_eop_1_ff ;
		ff_tx_data_1_f4   <= ff_tx_data_1_fff;
		ff_tx_mod_1_f4    <= ff_tx_mod_1_fff ;  
		ff_tx_dval_1_f4   <= ff_tx_dval_1_fff;  
		ff_tx_sop_1_f4    <= ff_tx_sop_1_fff ;  
		ff_tx_eop_1_f4    <= ff_tx_eop_1_fff ;  
		ff_tx_data_1_ff5  <= ff_tx_data_1_f4 ;
		ff_tx_mod_1_ff5   <= ff_tx_mod_1_f4  ;
		ff_tx_dval_1_ff5  <= ff_tx_dval_1_f4 ;
		ff_tx_sop_1_ff5   <= ff_tx_sop_1_f4  ;
		ff_tx_eop_1_ff5   <= ff_tx_eop_1_f4  ;
		ff_tx_data_1_fff6 <= ff_tx_data_1_ff5;
		ff_tx_mod_1_fff6  <= ff_tx_mod_1_ff5 ;
		ff_tx_dval_1_fff6 <= ff_tx_dval_1_ff5;
		ff_tx_sop_1_fff6  <= ff_tx_sop_1_ff5 ;
		ff_tx_eop_1_fff6  <= ff_tx_eop_1_ff5 ;
    end
end
always @(posedge clk or posedge rst) begin 
	if(rst == 1'b1) begin
		cnt <= 5'b0;
	end else if(ff_tx_dval_1 == 1'b1) begin
		cnt <= cnt +1'b1;
	end else if(ff_tx_dval_1_fff6 == 1'b0)begin 
		cnt <= 5'b0;
	end else begin 
		cnt <= 5'b0;
	end
end
always @(posedge clk or posedge rst) begin 
	if(rst == 1'b1) begin
		pppoe_f <= 0;
	end else if(cnt == 4'd3) begin
		pppoe_f <=ff_tx_data_1[31:16] ;
	end else begin 
		pppoe_f <=16'b0;
	end
end
always @(posedge clk or posedge rst) begin 
	if(rst == 1'b1) begin
		obstruct_flag <= 1'b0;
	end else if(pppoe_f == 16'h8863||pppoe_f == 16'h8864) begin
		obstruct_flag <= 1'b1;
	end else if(cnt == 1'b0)begin 
		obstruct_flag <= 1'b0;
	end else obstruct_flag <= obstruct_flag;
end

/*always @(posedge clk or posedge rst) begin 
	if(rst == 1'b1) begin
		obstruct_en <= 1'b0;
	end else if(obstruct_flag == 1'b1) begin
		obstruct_en <= 1'b1;
	end else if(cnt == 1'b0) begin 
		obstruct_en <= 1'b0;
	end else obstruct_en <= obstruct_en;
end*/
always @(posedge clk or posedge rst) begin
    if (rst==1'b1) begin
        frame_en_f <= 0;
    end
    else begin
        frame_en_f<=frame_en ;
    end
end
//chose output 
always @(posedge clk or posedge rst) begin
    if (rst==1'b1) begin
        ff_tx_rdy_0  <= 1'b0;
        ff_tx_rdy_1  <= 1'b0;  
        ff_tx_data   <=32'b0;
        ff_tx_mod    <=2'b0;
        ff_tx_dval   <=1'b0;
        ff_tx_sop    <=1'b0;
        ff_tx_eop    <= 1'b0;
    end
    else if (frame_en_f==1'b1) begin
    	ff_tx_rdy_0 <=ff_tx_rdy;
        ff_tx_data  <=ff_tx_data_0;
        ff_tx_mod   <=ff_tx_mod_0;
        ff_tx_dval  <=ff_tx_dval_0;
        ff_tx_sop   <=ff_tx_sop_0;
        ff_tx_eop   <=ff_tx_eop_0 ;
    end
    else if(obstruct_flag == 1'b1) begin 
    	ff_tx_rdy_0  <=  1'b0;
		ff_tx_rdy_1  <=  1'b0;
		ff_tx_data   <= 32'b0;
		ff_tx_mod    <=  2'b0;
		ff_tx_dval   <=  1'b0;
		ff_tx_sop    <=  1'b0;
		ff_tx_eop    <=  1'b0;
    end else begin
        ff_tx_rdy_1 <=    ff_tx_rdy;
        ff_tx_data  <=  ff_tx_data_1_fff6;
        ff_tx_mod   <=   ff_tx_mod_1_fff6;
        ff_tx_dval  <=  ff_tx_dval_1_fff6;
        ff_tx_sop   <=   ff_tx_sop_1_fff6;
        ff_tx_eop   <=   ff_tx_eop_1_fff6;
    end
end

endmodule
