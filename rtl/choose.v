// **************************************************************        
// COPYRIGHT(c)2021, Xidian University
// All rights reserved.
//
// IP LIB INDEX  : 
// IP Name       : 
//                
// File name     :  choose.v
// Module name   :  CHOOSE
// Full name     : 
//
// Author        :  ck
// Email         :  1255279151@qq.com
// Data          :   
// Version       :  V 1.0 
// 
// Abstract      :  determine whitch attack approach to use
// Called by     :  Father Module
// 
// This File is  Created on 2021/1/28
// Modification history
// ---------------------------------------------------------------------------------
`timescale 1ns/1ps
module CHOOSE(
    input               clk             ,
    input               rst             ,

    /*connect with the first attack approach*/
    input   [31:0]      tx_data1        ,
    input   [1:0]       tx_mod1         ,
    input               tx_dval1        ,
    input               tx_sop1         ,
    input               tx_eop1         ,

    /*connect with the second attack approach*/
    input   [31:0]      tx_data2        ,
    input   [1:0]       tx_mod2         ,
    input               tx_dval2        ,
    input               tx_sop2         ,
    input               tx_eop2         ,

    /*connect with the port*/
    input               switch          ,

    output    reg  [31:0]     tx_ff_data_mac_00 ,
    output    reg  [1:0]      tx_ff_mod_mac_00  ,
    output    reg             tx_ff_dval_mac_00 ,
    output    reg             tx_ff_sop_mac_00  ,
    output    reg             tx_ff_eop_mac_00 

);


always @(posedge clk or posedge rst) begin
    if (rst) begin
        tx_ff_data_mac_00 <= 'b0;
        tx_ff_mod_mac_00  <= 'b0;
        tx_ff_dval_mac_00 <= 'b0;
        tx_ff_sop_mac_00  <= 'b0;
        tx_ff_eop_mac_00  <= 'b0;
    end
    else if (switch) begin
        tx_ff_data_mac_00 <= tx_data1  ;
        tx_ff_mod_mac_00  <= tx_mod1   ;
        tx_ff_dval_mac_00 <= tx_dval1  ;
        tx_ff_sop_mac_00  <= tx_sop1   ;
        tx_ff_eop_mac_00  <= tx_eop1   ;
    end
    else begin
        tx_ff_data_mac_00 <= tx_data2  ;
        tx_ff_mod_mac_00  <= tx_mod2   ;
        tx_ff_dval_mac_00 <= tx_dval2  ;
        tx_ff_sop_mac_00  <= tx_sop2   ;
        tx_ff_eop_mac_00  <= tx_eop2   ;
    end
end



endmodule