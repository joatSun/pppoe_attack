// **************************************************************        
// COPYRIGHT(c)2021, Xidian University
// All rights reserved.
//
// IP LIB INDEX  : 
// IP Name       : 
//                
// File name     :  acquiry.v
// Module name   :  ACQUIRY
// Full name     : 
//
// Author        :  shawnli
// Email         :  xwli_ff@126.com
// Data          :   
// Version       :  V 1.0 
// 
// Abstract      :  提取pap帧明文变长账户密码完成特殊格式UDP密码前部分组帧后部分由to_upper实现
// Called by     :  Father Module
// 
// This File is  Created on 2021/1/29
// Modification history
// ---------------------------------------------------------------------------------
`timescale 1ns/1ps
`define FRAME_TYPE_SESS 	16'h8864
`define WIDTH_CNT 			11
module ACQUIRY(
	input 				clk 			,
	input 				rst 			,
	//from mac_user
    input 	[31:0]	 	ff_rx_data 		,
    input 	[1:0]	 	ff_rx_mod		,
    input 				ff_rx_dsav		,
    input 				ff_rx_dval		,
    input 				ff_rx_sop		,
    input 				ff_rx_eop		,

    output 	reg [31:0]	frame_acquiry 	,
    output 	reg 		ff_acq_en 		,
    output  reg	[7 :0] 	len 			 
);

reg  	[31:0]	 	ff_rx_data_f 		;
reg  	[1:0]	 	ff_rx_mod_f			;
reg  				ff_rx_dsav_f		;
reg  				ff_rx_dval_f		;
reg  				ff_rx_sop_f			;
reg  				ff_rx_eop_f			;

reg  	[31:0]	 	ff_rx_data_ff 		;
reg  	[1:0]	 	ff_rx_mod_ff     	;
reg  				ff_rx_dsav_ff		;
reg  				ff_rx_dval_ff		;
reg  				ff_rx_sop_ff		;
reg  				ff_rx_eop_ff		;

reg 	[`WIDTH_CNT:0] cnt_parse 		;

reg 	[47:0] 		src_addr 			;
reg 	[15:0] 		frame_type 			;
reg 	[15:0] 		ppp_protocol 		;

always @(posedge clk or posedge rst) begin
	if (rst) begin
		ff_rx_data_f 		<= 'd0;
		ff_rx_mod_f			<= 'd0;
		ff_rx_dsav_f		<= 'd0;
		ff_rx_dval_f		<= 'd0;
		ff_rx_sop_f			<= 'd0;
		ff_rx_eop_f			<= 'd0;
		ff_rx_data_ff 		<= 'd0;
		ff_rx_mod_ff		<= 'd0;
		ff_rx_dsav_ff		<= 'd0;
		ff_rx_dval_ff		<= 'd0;
		ff_rx_sop_ff		<= 'd0;
		ff_rx_eop_ff		<= 'd0;
	end
	else begin
		ff_rx_data_f 		<= ff_rx_data 		;
		ff_rx_mod_f			<= ff_rx_mod		;
		ff_rx_dsav_f		<= ff_rx_dsav		;
		ff_rx_dval_f		<= ff_rx_dval		;
		ff_rx_sop_f			<= ff_rx_sop		;
		ff_rx_eop_f			<= ff_rx_eop		;
		ff_rx_data_ff 		<= ff_rx_data_f 	;
		ff_rx_mod_ff		<= ff_rx_mod_f		;
		ff_rx_dsav_ff		<= ff_rx_dsav_f		;
		ff_rx_dval_ff		<= ff_rx_dval_f		;
		ff_rx_sop_ff		<= ff_rx_sop_f		;
		ff_rx_eop_ff		<= ff_rx_eop_f		;
	end
end

always @(posedge clk or posedge rst) begin
	if (rst) begin
		cnt_parse <= 'd0;
	end
	else if (ff_rx_dval_f == 1'b1) begin
		cnt_parse <= cnt_parse + 'b1;
	end
	else begin
		cnt_parse <= 'd0;
	end
end
//read source address
always @(posedge clk or posedge rst) begin
	if (rst) begin
		src_addr <= 48'd0;
	end
	else if (cnt_parse == 'b1) begin
		src_addr[47:32] <= ff_rx_data_f[15:0];
	end
	else if (cnt_parse == 'd2) begin
		src_addr[31:0] <= ff_rx_data_f;
	end
	else begin
		src_addr <= src_addr;
	end
end
always @(posedge clk or posedge rst) begin
	if (rst) begin
		frame_type <= 16'b0;
	end
	else if (cnt_parse == 16'd3) begin
		frame_type <= ff_rx_data_f[31:16];
	end
	else begin
		frame_type <= frame_type;
	end
end
//read PPP protocol
always @(posedge clk or posedge rst) begin
	if (rst) begin
		ppp_protocol <= 16'b0;
	end
	else if ((frame_type == `FRAME_TYPE_SESS)&(cnt_parse == 'd5)) begin
		ppp_protocol <= ff_rx_data_f[31:16];
	end
	else begin
		ppp_protocol <= ppp_protocol;
	end
end

//******************************************//
//read frame_acquiry
reg [7:0] id_len;
reg [7:0] pw_len;
reg [5:0] cnt_id;
//id cnt
wire [5:0] id_cnt;
wire [1:0] id_mod;
assign id_cnt = (id_len[1:0] == 2'b0)?id_len[7:2]:id_len[7:2] + 6'b1;
assign id_mod = id_len[1:0];
//wire [7:0] len;
// assign len = (pw_len == 8'b0)?8'b100:id_len + pw_len + 20;//(2:20_20+2:id_len,pw_len+16:d,saddr,type)
always @(posedge clk or posedge rst) begin
	if (rst) begin
		len <= 8'b100;
	end
	else if (pw_len != 8'b0) begin
		len <= id_len + pw_len + 20;
	end
	else begin
		len <= len;
	end
end
always @(posedge clk or posedge rst) begin
	if (rst) begin
		frame_acquiry 	<= 32'b0;
		ff_acq_en 		<= 1'b0;
		id_len 			<= 8'b0;
		pw_len 			<= 8'b0;
		cnt_id 			<= 6'b0;
	end
	else if (ppp_protocol == 16'hc023) begin
		if (cnt_parse == 'd6) begin
			frame_acquiry <= {24'h2020_25,ff_rx_data_f[7:0]}; 
			ff_acq_en <= 1'b1;
			id_len <= ff_rx_data_f[15:8];
			pw_len <= 8'b0;
			cnt_id <= cnt_id + 6'b1;			
		end
		else if (cnt_parse <= 'd14) begin
			if ((id_cnt == cnt_id)&(cnt_id != 6'b0)&(id_cnt != 6'b0)) begin
				 case(id_mod)
				 	2'b01: begin
				 		frame_acquiry <= {8'h25,ff_rx_data_f[23:0]}; 
						ff_acq_en <= 1'b0;
						id_len <= id_len;
						pw_len <= ff_rx_data_f[31:24];
						cnt_id <= 6'b0;	
				 	end
				 	2'b10: begin
				 		frame_acquiry <= {ff_rx_data_f[31:24],8'h25,ff_rx_data_f[15:0]}; 
						ff_acq_en <= 1'b0;
						id_len <= id_len;
						pw_len <= ff_rx_data_f[23:16];
						cnt_id <= 6'b0;				 			
				 	end
				 	2'b11: begin
				 		frame_acquiry <= {ff_rx_data_f[31:16],8'h25,ff_rx_data_f[7:0]}; 
						ff_acq_en <= 1'b0;
						id_len <= id_len;
						pw_len <= ff_rx_data_f[15:8];
						cnt_id <= 6'b0;	 			
				 	end
				 	default:begin
				 		frame_acquiry <= {ff_rx_data_f[31:8],8'h25}; 
						ff_acq_en <= 1'b0;
						id_len <= id_len;
						pw_len <= ff_rx_data_f[7:0];
						cnt_id <= 6'b0;			 			
				 	end
				 endcase
			end 
			else if ((cnt_id < id_cnt)&(cnt_id != 6'b0)) begin
			 	frame_acquiry <= ff_rx_data_f[31:0]; 
				ff_acq_en <= 1'b0;
				id_len <= id_len;
				pw_len <= pw_len;
				cnt_id <= cnt_id + 6'b1;				  		
			  end
			 else begin
			 	frame_acquiry <= ff_rx_data_f[31:0]; 
				ff_acq_en <= 1'b0;
				id_len <= id_len;
				pw_len <= pw_len;
				cnt_id <= 6'b0;			 					
			 end				
		end
		else begin
			frame_acquiry 	<= 32'b0;
			ff_acq_en 		<= 1'b0;
			id_len 			<= 8'b0;
			pw_len 			<= 8'b0;
			cnt_id 			<= 6'b0;	
		end
	end
end
endmodule