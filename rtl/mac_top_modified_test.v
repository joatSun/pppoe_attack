// **************************************************************
// COPYRIGHT(c)2015, Xidian University
// All rights reserved.
//
// IP LIB INDEX : 
// IP Name      : 
//                
// File name    : 
// Module name  : 
// Full name    :   
//
// Author       :   lxw
// Email        :   
// Date         :   
// Version      :   V 1.0 
// 
// Abstract     :
// Called by    :  
// 
// Modification history
// ------------------------------------------------------------------------------------------------------
// //对第三种DDOS攻击进行了上班测试，bit流文件可以生成，但再接到计算机后，winkshark抓向计算机广播发送PADI帧无结果，还得继续修改。
//  Log
//  V1.0  
//
// *********************************************************************

// *******************
// TIMESCALE
// ******************* 
`timescale 1ns/1ps 

//*******************
//DEFINE(s)
//*******************


//*******************
//DEFINE MODULE PORT
//*******************
module mac_top_modified_test(
    input   wire                  Clk_in                                                                   ,
    input   wire                  Reset                                                                    ,
    output  wire                  phyrst_n                                                                 ,
    input   wire                  rgmii_rx_clk_00,//之前PHY用的是RGMII，现在改为SGMII，但是MAC采用GMII，故需要进行SGMII-->GMII转化
    input   wire    [   3:   0]   rgmii_rxd_00                                                             ,
    input   wire                  rgmii_rx_ctl_00                                                          ,
    output  wire                  rgmii_tx_clk_00                                                          ,
    output  wire    [   3:   0]   rgmii_txd_00                                                             ,
    output  wire                  rgmii_tx_ctl_00                                                          ,
    input   wire                  pppoe_padi                                                               ,
    input   wire                  pppoe_ddos                                                               
    /*input               rgmii_rx_clk_01,*/
    /*input       [3:0]   rgmii_rxd_01   ,*/
    /*input               rgmii_rx_ctl_01,*/
    /*output              rgmii_tx_clk_01,*/
    /*output      [3:0]   rgmii_txd_01   ,*/
    /*output              rgmii_tx_ctl_01,*/
    /*output              led_out_flag*/
);
 wire        rx_ff_rdy_mac_00 ;
 wire [31:0] rx_ff_data_mac_00;
 wire [1:0]  rx_ff_mod_mac_00 ;
 wire        rx_ff_dsav_mac_00;
 wire        rx_ff_dval_mac_00;
 wire        rx_ff_sop_mac_00 ;
 wire        rx_ff_eop_mac_00 ;
 wire [5:0]  rx_ff_err_mac_00 ;
 wire [10:0] frame_length_00  ;
 (*mark_debug = "true"*) wire [31:0] tx_ff_data_mac_00;
 wire [1:0]  tx_ff_mod_mac_00 ;
 (*mark_debug = "true"*) wire        tx_ff_dval_mac_00;
 wire        tx_ff_sop_mac_00 ;
 wire        tx_ff_eop_mac_00 ;
 (*mark_debug = "true"*) wire        ff_tx_rdy_mac_00 ;
 (*mark_debug = "true"*) wire [5:0]  Fifo_data_count_00;
//mac2
/* wire        rx_ff_rdy_mac_01 ;*/
/* wire [31:0] rx_ff_data_mac_01;*/
/* wire [1:0]  rx_ff_mod_mac_01 ;*/
/* wire        rx_ff_dsav_mac_01;*/
/* wire        rx_ff_dval_mac_01;*/
/* wire        rx_ff_sop_mac_01 ;*/
/* wire        rx_ff_eop_mac_01 ;*/
/* wire [5:0]  rx_ff_err_mac_01 ;*/
/* wire [10:0] frame_length_01  ;*/
/* wire [31:0] tx_ff_data_mac_01;*/
/* wire [1:0]  tx_ff_mod_mac_01 ;*/
/* reg        tx_ff_dval_mac_01;*/
/* reg        tx_ff_sop_mac_01 ;*/
/* reg        tx_ff_eop_mac_01 ;*/
/* wire        ff_tx_rdy_mac_01 ;*/
/* wire [5:0]  Fifo_data_count_01;*/

/* wire        rx_ff_rdy_mac_10 ;*/
/* wire [31:0] rx_ff_data_mac_10;*/
/* wire [1:0]  rx_ff_mod_mac_10 ;*/
/* wire        rx_ff_dsav_mac_10;*/
/* wire        rx_ff_dval_mac_10;*/
/* wire        rx_ff_sop_mac_10 ;*/
/* wire        rx_ff_eop_mac_10 ;*/
/* wire [5:0]  rx_ff_err_mac_10 ;*/
/* wire [10:0] frame_length_10;*/
/* wire [31:0] tx_ff_data_mac_10;*/
/* wire [1:0]  tx_ff_mod_mac_10 ;*/
/* wire        tx_ff_dval_mac_10;*/
/* wire        tx_ff_sop_mac_10 ;*/
/* wire        tx_ff_eop_mac_10 ;*/
/* wire        ff_tx_rdy_mac_10 ;*/
/* wire [5:0]  Fifo_data_count_10;*/


wire        Clk_125M  ;
wire        Clk90     ;
wire        dcm_locked;
wire        Clk_user  ;
wire        Clk_200M  ;

/*(*mark_debug = "true"*)reg [4:0] frame_length_cnt;*/
/*(*mark_debug = "true"*)reg [4:0] read_frame_length_cnt;*/
/*reg         led_out_flag;*/
//-----------------------------------------------
wire reset_via_bufg ;
BUFG reset_bufg(
    .I(Reset),
    .O(reset_via_bufg)

    );

IDELAYCTRL dlyctrl(
    .RDY   (        ),
    .REFCLK(Clk_200M),
    .RST   (reset_via_bufg   )
);

mac_rgmii_clk clk_gen(
    .CLK_IN1_P      (Clk_in     ),
    .CLK_OUT1       (Clk_125M   ),
    .CLK_OUT2       (Clk_user   ),
    .CLK_OUT3       (Clk_200M   ),
    .CLK_OUT4       (Clk90      ),   
    .RESET          (reset_via_bufg      ),
    .LOCKED         (dcm_locked )
);

MAC_RGMII U_MAC_upper(
    .Reset       (reset_via_bufg            ),
    .Clk_user    (Clk_user         ),
    .Clk_125M    (Clk_125M         ),
    .Clk90       (Clk90            ),
    .dcm_locked  (dcm_locked       ),
    .rgmii_rx_clk(rgmii_rx_clk_00  ),
    .rgmii_rxd   (rgmii_rxd_00     ),
    .rgmii_rx_ctl(rgmii_rx_ctl_00  ),
    .phyrst_n    (phyrst_n         ),
    .rgmii_tx_clk(rgmii_tx_clk_00  ),
    .rgmii_txd   (rgmii_txd_00     ),
    .rgmii_tx_ctl(rgmii_tx_ctl_00  ),
    .ff_rx_rdy   (1'b1 ),
    .ff_rx_data  (rx_ff_data_mac_00),
    .ff_rx_mod   (rx_ff_mod_mac_00 ),
    .ff_rx_dsav  (rx_ff_dsav_mac_00),
    .ff_rx_dval  (rx_ff_dval_mac_00),
    .ff_rx_sop   (rx_ff_sop_mac_00 ),
    .ff_rx_eop   (rx_ff_eop_mac_00 ),
    .rx_err      (rx_ff_err_mac_00 ),
    .frame_length(frame_length_00  ),
    .ff_tx_data  (tx_ff_data_mac_00),
    .ff_tx_mod   (tx_ff_mod_mac_00 ),
    .ff_tx_wren  (tx_ff_dval_mac_00),
    .ff_tx_err   (0                ),
    .ff_tx_sop   (tx_ff_sop_mac_00 ),
    .ff_tx_eop   (tx_ff_eop_mac_00 ),
    .ff_tx_rdy   (ff_tx_rdy_mac_00 ),
    .ff_tx_septy ( ),
    .Fifo_data_count(Fifo_data_count_00)
    );
DDOS_TOP U_DOSS_TOP(
    .clk                  (Clk_user               ),
    .rst                  (reset_via_bufg         ),
    .rx_data              (rx_ff_data_mac_00      ),
    .rx_mod               (rx_ff_mod_mac_00       ),
    .rx_dsav              (rx_ff_dsav_mac_00      ),
    .rx_dval              (rx_ff_dval_mac_00      ),
    .rx_sop               (rx_ff_sop_mac_00       ),
    .rx_eop               (rx_ff_eop_mac_00       ),
    .pppoe_padi           (pppoe_padi             ),
    .pppoe_ddos           (pppoe_ddos             ),
    .tx_rdy               (ff_tx_rdy_mac_00       ),
    .tx_data              (tx_ff_data_mac_00      ),
    .tx_mod               (tx_ff_mod_mac_00       ),
    .tx_dval              (tx_ff_dval_mac_00      ),
    .tx_sop               (tx_ff_sop_mac_00       ),
    .tx_eop               (tx_ff_eop_mac_00       )
);

/*MAC_RGMII U_MAC_lower(
    .Reset       (reset_via_bufg   ),
    .Clk_user    (Clk_user         ),
    .Clk_125M    (Clk_125M         ),
    .Clk90       (Clk90            ),
    .dcm_locked  (dcm_locked       ),
    .rgmii_rx_clk(rgmii_rx_clk_01  ),
    .rgmii_rxd   (rgmii_rxd_01     ),
    .rgmii_rx_ctl(rgmii_rx_ctl_01  ),
    .phyrst_n    (                 ),
    .rgmii_tx_clk(rgmii_tx_clk_01  ),
    .rgmii_txd   (rgmii_txd_01     ),
    .rgmii_tx_ctl(rgmii_tx_ctl_01  ),
    .ff_rx_rdy   (1'b1 ),
    .ff_rx_data  (rx_ff_data_mac_01),
    .ff_rx_mod   (rx_ff_mod_mac_01 ),
    .ff_rx_dsav  (rx_ff_dsav_mac_01),
    .ff_rx_dval  (rx_ff_dval_mac_01),
    .ff_rx_sop   (rx_ff_sop_mac_01 ),
    .ff_rx_eop   (rx_ff_eop_mac_01 ),
    .rx_err      (rx_ff_err_mac_01 ),
    .frame_length(frame_length_01  ),
    .ff_tx_data  (tx_ff_data_mac_01),
    .ff_tx_mod   (tx_ff_mod_mac_01 ),
    .ff_tx_wren  (tx_ff_dval_mac_01),
    .ff_tx_err   (0                ),
    .ff_tx_sop   (tx_ff_sop_mac_01 ),
    .ff_tx_eop   (tx_ff_eop_mac_01 ),
    .ff_tx_rdy   (ff_tx_rdy_mac_01 ),
    .ff_tx_septy ( ),
    .Fifo_data_count(Fifo_data_count_01)
    );

assign tx_ff_data_mac_01 = 32'h12345678;
assign tx_ff_mod_mac_01 = 2'd0;

always @(posedge Clk_user or posedge reset_via_bufg)
begin 
    if(reset_via_bufg == 1'b1)
        frame_length_cnt <= 5'd0;
    else if(frame_length_cnt == 5'd20)
        frame_length_cnt <= 5'd0;
    else 
        frame_length_cnt <= frame_length_cnt + 1'b1;
end

always @(posedge Clk_user or posedge reset_via_bufg)
begin 
    if(reset_via_bufg == 1'b1)
        tx_ff_dval_mac_01 <= 1'd0;
    else if(frame_length_cnt == 5'd1)
        tx_ff_dval_mac_01 <= 1'd1;
    else if(frame_length_cnt == 5'd6)
        tx_ff_dval_mac_01 <= 1'b0;
end

always @(posedge Clk_user or posedge reset_via_bufg)
begin 
    if(reset_via_bufg == 1'b1)
        tx_ff_sop_mac_01 <= 1'd0;
    else if(frame_length_cnt == 5'd1)
        tx_ff_sop_mac_01 <= 1'd1;
    else
        tx_ff_sop_mac_01 <= 1'b0;
end


always @(posedge Clk_user or posedge reset_via_bufg)
begin 
    if(reset_via_bufg == 1'b1)
        tx_ff_eop_mac_01 <= 1'd0;
    else if(frame_length_cnt == 5'd5)
        tx_ff_eop_mac_01 <= 1'd1;
    else
        tx_ff_eop_mac_01 <= 1'b0;
end


always @(posedge Clk_user or posedge reset_via_bufg)
begin 
    if(reset_via_bufg == 1'b1)
        read_frame_length_cnt <= 5'd0;
    else if(rx_ff_eop_mac_00 == 1'b1)
        read_frame_length_cnt <= 5'd0;
    else if(rx_ff_dval_mac_00 == 1'b1)
        read_frame_length_cnt <= read_frame_length_cnt + 1'b1;
end


always @(posedge Clk_user or posedge reset_via_bufg)
begin 
    if(reset_via_bufg == 1'b1)
        led_out_flag <= 1'd0;
    else if(read_frame_length_cnt < 5'd14)
        led_out_flag <= 1'b1;
    else
        led_out_flag <= 1'd0;
end*/
endmodule
